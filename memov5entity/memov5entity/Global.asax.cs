﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using memov5entity.App_Start;
using System.Collections;

namespace memov5entity
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add( new Microsoft.Web.Mvc.FixedRazorViewEngine());
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            DeviceConfig.EvaluateDisplayMode(); //Evaluate incoming request and update Display Mode table
            AuthConfig.RegisterAuth();
        }
        void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            try
            {
                HttpContext context = HttpContext.Current;

                string acceptEncoding = context.Request.Headers["Accept-Encoding"].ToUpperInvariant();

                HttpResponse response = context.Response;

                string contentType = response.ContentType;

                if (contentType != "text/html" && contentType != "text/css" && contentType != "application/javascript")
                    return;

                if (acceptEncoding.Contains("GZIP"))
                {
                    response.Filter = new GZipStream(response.Filter, CompressionMode.Compress);
                    response.AppendHeader("Content-Encoding", "gzip");
                }
                else if (acceptEncoding.Contains("DEFLATE"))
                {
                    response.Filter = new DeflateStream(response.Filter, CompressionMode.Compress);
                    response.AppendHeader("Content-Encoding", "deflate");
                }
            }
            catch { }

        }

        public override string GetVaryByCustomString(HttpContext context, string custom)
        {
            if (string.Equals(custom, "ismobile", StringComparison.OrdinalIgnoreCase))
                return context.Request.Browser.IsMobileDevice.ToString();

            return base.GetVaryByCustomString(context,custom);
        }
        void Application_Error(object sender, EventArgs e)
        {

            //get the client ip
            HttpRequest request = base.Request;

            // Get UserHostAddress property.
            string address = request.UserHostAddress;

            // Write to response.
            //  base.Response.Write(address);

            // Done.
            base.CompleteRequest();


            // end client ip



            // At this point we have information about the error
            HttpContext ctx = HttpContext.Current;

            Exception exception = ctx.Server.GetLastError();

            string errorInfo =
               "<br>Offending URL: " + ctx.Request.Url.ToString() +
               "<br>Source: " + exception.Source +
               "<br>Message: " + exception.Message +
               "<br>Stack trace: " + exception.StackTrace;

            //   ctx.Response.Write(errorInfo);

            // --------------------------------------------------
            // To let the page finish running we clear the error
            // --------------------------------------------------
            //  ctx.Server.ClearError();

            //  base.OnError(e);
            ArrayList arrparam = new ArrayList();
            arrparam.Add("@errormessage");
            arrparam.Add("@ip");
            arrparam.Add("@details");
            arrparam.Add("@url");
            ArrayList arrvalues = new ArrayList();
            arrvalues.Add(exception.Message);
            arrvalues.Add(address);
            arrvalues.Add(exception.Source + " ****** " + exception.StackTrace);
            arrvalues.Add(ctx.Request.Url.ToString());
            //    connection.runproc("save_error", arrparam, arrvalues);
            //save_error(@errormessage nvarchar(200),@ip nvarchar(50),@details ntext)
            //  ctx.Server.ClearError();
        }


    }
}