﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Text;
namespace memov5entity
{
    public partial class shreet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
            rbl_mofakera.Items[0].Text = "خارج الشريط من خلال مربع ينطلق منه الشريط";
            rbl_mofakera.Items[1].Text = "داخل الشريط مع عرض الأخبار";
            ddl_background.Items[0].Text = "اسود";
            ddl_background.Items[1].Text = "أبيض";
            ddl_background.Items[2].Text = "أحمر";
            ddl_background.Items[3].Text = "برتقالي";
            ddl_background.Items[4].Text = "أصفر";
            ddl_background.Items[5].Text = "أخضر";
            ddl_background.Items[6].Text = "أزرق";
            ddl_background.Items[7].Text = "نيلي";
            ddl_background.Items[8].Text = "بنفسجي";
            ddl_background.Items[9].Text = "تخصيص";

            ddl_forecolor.Items[0].Text = "أبيض";
            ddl_forecolor.Items[1].Text = "أحمر";
            ddl_forecolor.Items[2].Text = "برتقالي";
            ddl_forecolor.Items[3].Text = "أصفر";
            ddl_forecolor.Items[4].Text = "أخضر";
            ddl_forecolor.Items[5].Text = "أزرق";
            ddl_forecolor.Items[6].Text = "نيلي";
            ddl_forecolor.Items[7].Text = "بنفسجي";
            ddl_forecolor.Items[8].Text = "اسود";
            ddl_forecolor.Items[9].Text = "تخصيص";

            ddlcat.Items[0].Text = "عام";
            ddlcat.Items[1].Text = "سياسة";
            ddlcat.Items[2].Text = "أخبار منوعة";
            ddlcat.Items[3].Text = "تقارير";
            ddlcat.Items[4].Text = "الأسرة";
            //ddl_forecolor.Items[].Text=Resources.memo.sh51;

            cbl_font.Items[0].Text = "مائل";
            cbl_font.Items[1].Text = "سميك";
            cbl_font.Items[2].Text = "تحته خط";

            rbl_mofakera.Items[0].Text = "خارج الشريط من خلال مربع ينطلق منه الشريط";
            rbl_mofakera.Items[1].Text = "داخل الشريط مع عرض الأخبار";

            rbl_dir.Items[0].Text = "من اليسار إلى اليمين";
            rbl_dir.Items[1].Text = "من أسفل لأعلى";
            rbl_dir.Items[2].Text = "من اعلى لأسفل";


            if (!Page.IsPostBack)
            {

                getdata();
            }

            ddl_background.Attributes.Add("onchange", "en_back();");
            ddl_forecolor.Attributes.Add("onchange", "en_fore();");

            register_script();

            //   myiframe.Attributes.Add("src", "http://islammemo.cc/bar.aspx?id=" + Session["shreet_uname"] + "&p=2868&se=%2B&ci=164" );
            //  TextBox3.Text = "<iframe src='http://islammemo.cc/bar.aspx?id=" + Session["shreet_uname"] + "&p=2868&se=%2B&ci=164'"  + "></iframe>";

            myiframe.Attributes.Add("src", "http://www.saeem.com/bar.aspx?id=" + Session["shreet_uname"] + "&p=2868&se=%2B&ci=164");
            TextBox3.Text = "<iframe src='http://www.saeem.com/bar.aspx?id=" + Session["shreet_uname"] + "&p=2868&se=%2B&ci=164'" + "></iframe>";


        }

        void register_script()
        {


            StringBuilder sb = new StringBuilder();
            sb.Append("<script>");
            sb.Append("function en_back()");
            sb.Append("{");
            sb.Append("if(document.getElementById('" + ddl_background.ClientID + "').options[document.getElementById('" + ddl_background.ClientID + "').selectedIndex].value=='0')");
            sb.Append("{");
            sb.Append("document.getElementById('" + TextBox1.ClientID + "').disabled='';");
            sb.Append("}else{");
            sb.Append("document.getElementById('" + TextBox1.ClientID + "').disabled='disabled';");
            sb.Append("}}");
            sb.Append("function en_fore()");
            sb.Append("{");
            sb.Append("if(document.getElementById('" + ddl_forecolor.ClientID + "').options[document.getElementById('" + ddl_forecolor.ClientID + "').selectedIndex].value=='0')");
            sb.Append("{");
            sb.Append("document.getElementById('" + TextBox2.ClientID + "').disabled='';");
            sb.Append("}else{");
            sb.Append("document.getElementById('" + TextBox2.ClientID + "').disabled='disabled';");
            sb.Append("}}");
            sb.Append("");
            sb.Append("</script>");



            Literal1.Text = sb.ToString();

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["test4ConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("UPDATE bar SET mofkra =@mofkra, direction =@direction, bold =@bold, italic =@italic, underline =@underline, fontfamily =@fontfamily, fontsize =@fontsize, scrollamount =@scrollamount, scrolldelay =@scrolldelay, backcolor =@backcolor, color =@color,catid=@catid WHERE (id = @id)", conn);
            cmd.Parameters.AddWithValue("@mofkra", rbl_mofakera.SelectedValue);
            cmd.Parameters.AddWithValue("@direction", rbl_dir.SelectedValue);
            cmd.Parameters.AddWithValue("@bold", cbl_font.Items[0].Selected);
            cmd.Parameters.AddWithValue("@italic", cbl_font.Items[1].Selected);
            cmd.Parameters.AddWithValue("@underline", cbl_font.Items[2].Selected);
            cmd.Parameters.AddWithValue("@fontfamily", ddl_fontfamily.SelectedValue);
            cmd.Parameters.AddWithValue("@fontsize", ddl_fontsize.SelectedValue);
            cmd.Parameters.AddWithValue("@scrollamount", ddl_marq_speed.SelectedValue);
            cmd.Parameters.AddWithValue("@scrolldelay", 40);
            cmd.Parameters.AddWithValue("@catid", ddlcat.SelectedValue);
            if (ddl_background.SelectedValue == "0")
            {
                cmd.Parameters.AddWithValue("@backcolor", TextBox1.Text);
            }
            else
            {
                cmd.Parameters.AddWithValue("@backcolor", ddl_background.SelectedValue);
            }
            if (ddl_forecolor.SelectedValue == "0")
            {
                cmd.Parameters.AddWithValue("@color", TextBox2.Text);
            }
            else
            {
                cmd.Parameters.AddWithValue("@color", ddl_forecolor.SelectedValue);
            }
            cmd.Parameters.AddWithValue("@id", Session["shreet_uname"]);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        void getdata()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["test4ConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("select id, uname, email, site, mofkra, direction, bold, italic, underline, fontfamily, fontsize, scrollamount, scrolldelay, color, backcolor,catid from  bar where id=@id", conn);
            cmd.Parameters.AddWithValue("@id", Session["shreet_uname"]);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                if (dr.GetBoolean(4))
                {
                    rbl_mofakera.SelectedIndex = 0;
                }
                else
                {
                    rbl_mofakera.SelectedIndex = 1;
                }
                if (!finditem(ddlcat, dr["catid"].ToString()))
                {
                    ddlcat.SelectedIndex = 0;
                }

                try
                {

                    rbl_dir.SelectedValue = dr["direction"].ToString();
                }
                catch { }


                cbl_font.Items[0].Selected = dr.GetBoolean(7);
                cbl_font.Items[1].Selected = dr.GetBoolean(6);
                cbl_font.Items[2].Selected = dr.GetBoolean(8);

                try
                {
                    ddl_fontsize.SelectedValue = dr["fontsize"].ToString();
                    ddl_marq_speed.SelectedValue = dr["scrollamount"].ToString();
                    finditem(ddl_fontfamily, dr["fontfamily"].ToString().Trim());
                }
                catch { }


                if (!finditem(ddl_forecolor, dr["color"].ToString()))
                {
                    TextBox2.Enabled = true;
                    TextBox2.Text = dr["color"].ToString();
                    ddl_forecolor.SelectedValue = "0";
                }


                if (!finditem(ddl_background, dr["backcolor"].ToString()))
                {
                    TextBox1.Enabled = true;
                    TextBox1.Text = dr["backcolor"].ToString();
                    ddl_background.SelectedValue = "0";
                }


            }
            conn.Close();
        }

        bool finditem(DropDownList ddl, string myval)
        {
            foreach (ListItem item in ddl.Items)
            {
                if (item.Value == myval)
                {
                    item.Selected = true;
                    return true;
                }
            }
            return false;
        }


    }
}