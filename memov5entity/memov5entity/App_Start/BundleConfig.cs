﻿using System.Web;
using System.Web.Optimization;

namespace memov5entity
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
            //            "~/Scripts/jquery-ui-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.unobtrusive*",
            //            "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/bundle/css").Include("~/css/style.css"));
            bundles.Add(new ScriptBundle("~/bundlejs/js").Include(
                "~/widget/lib/jquery-1.7.1.js",
                "~/js/placeholder.js",
                "~/js/ddtabmenu.js",
                "~/js/jquery.featureList-1.0.0.js",
                "~/js/gallery_home.js",
                "~/js/jquery.jscrollpane.min.js",
                "~/js/jquery.newsTicker-2.2.js",
                "~/js/litetabs.jquery.js"));
        bundles.Add(new ScriptBundle("~/bundle/lazylooding").Include("~/js/lazylooding.js"));
              
            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
            bundles.Add(new ScriptBundle("~/bundle/mobiljs").Include("~/mobilscript/jquery.mobile-1.3.0.js", "~/mobilscript/jquery.mobile-1.3.0.min.js"));
            bundles.Add(new StyleBundle("~/bundle/mobilcss").Include("~/css_mobil/style.css",
                "~/css_mobil/bootstrap.min.css", "~/css_mobil/bootstrap.css", "~/css_mobil/bootstrap-responsive.min.css",
                "~/css_mobil/bootstrap-responsive.css"
                
                ));
            
           
        }
    }
}