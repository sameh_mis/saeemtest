﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.WebPages;

namespace memov5entity.App_Start
{
    public static class DeviceConfig
    {
        public static void EvaluateDisplayMode()
        {
            DisplayModeProvider.Instance.Modes.Insert(0,
                new DefaultDisplayMode("Phone")
                {  //...modify file (view that is served)
                    //Query condition
                    ContextCondition = (ctx => (
                        //look at user agent
                        (ctx.GetOverriddenUserAgent() != null) &&
                        (//...either iPhone or iPad  
                        
                            (ctx.GetOverriddenBrowser().IsMobileDevice) 
                          //  (ctx.GetOverriddenUserAgent().IndexOf("iPod", StringComparison.OrdinalIgnoreCase) >= 0)
                        )
                ))
                });
            
        }
    }
}