﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace memov5entity
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.IgnoreRoute("images/{*pathInfo}");
            /*AHMed*/
            routes.MapRoute(
             "Root",
             "",
             new { controller = "Home", action = "Index", id = "" }
           );

            routes.MapRoute("testart", "akhbar/testart", new { controller = "akhbar", action = "testart" });
            routes.MapRoute("comments", "khabar/comments/{id}", new { controller = "akhbar", action = "comments" });
            routes.MapRoute("cat", "akhbar/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("mokh", "mokhtasr-el-dros/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("mokh2", "mokhtasr-el-dros/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });


            routes.MapRoute("arkam", "arkam/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("arkam2", "arkam/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });

            routes.MapRoute("kalam", "Aklam-el-koraa/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("kalam2", "Aklam-el-koraa/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });

            routes.MapRoute("vedio_images", "vedio-images/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("vedio_images2", "vedio-images/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });



            routes.MapRoute("takreer", "Tkarer/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("takreer2", "Tkarer/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("takreer3", "Tkarer/{cat}/{cat2}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });

            routes.MapRoute("malaf", "malaf-sahafi/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("malaf2", "malaf-sahafi/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });

            //  routes.MapRoute("gharb", "nahn-we-el-gharb", new { controller = "nahn_wa_elgharb", action = "Index" });
            routes.MapRoute("gharb1", "nahn-we-el-gharb/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("gharb2", "nahn-we-el-gharb/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });


            routes.MapRoute("almesbar", "Al-Mesbar/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("almesbar2", "Al-Mesbar/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });

            routes.MapRoute("tahkeek", "Tahkikat/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("tahkeek2", "Tahkikat/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });

            routes.MapRoute("estsharat2", "Estsharat/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("estsharat22", "Estsharat/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            ///moatamrat/2006/12/26/25754.html



            routes.MapRoute("motamrat", "moatamrat/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("motamrat2", "moatamrat/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });


            routes.MapRoute("monasamat", "monathamat-we-Haieat/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("monasamat2", "monathamat-we-Haieat/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });

            routes.MapRoute("economy", "culture-and-economy/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("economy2", "culture-and-economy/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("economy3", "culture-and-economy/{cat}/{cat2}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });

            //Mohet
            routes.MapRoute("mohet", "Mohet/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("mohet2", "Mohet/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });


            routes.MapRoute("monawaat2", "monawaat/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("monawaat3", "monawaat/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });

            routes.MapRoute("cat2", "akhbar/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("cat3", "akhbar/{cat}/{cat2}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });

            /*اشتغلت*/
            routes.MapRoute("hadath", "hadath-el-saa", new { controller = "hadath", action = "Index" });


            routes.MapRoute("archive", "hadath-el-saa/archive/{page}", new { controller = "hadath", action = "archive", page = 0 });
            routes.MapRoute("archive2", "hadath-el-saa/archive/page/{page}", new { controller = "hadath", action = "archive", page = 0 });


            routes.MapRoute("hadath_cat", "hadath-el-saa/{cat}/{page}", new { controller = "hadath_cats_", action = "Index", cat = "", page = 0 });
            routes.MapRoute("hadath_cat2", "hadath-el-saa/{cat}/page/{page}", new { controller = "hadath_cats_", action = "Index", cat = "", page = 0 });
            routes.MapRoute("shreet", "shreetlogin.aspx", new { controller = "shreetlogin", action = "Index" });
            routes.MapRoute("general_marquee ", "general_marquees.aspx", new { controller = "shreetlogin", action = "GeneralMarquees" });
            routes.MapRoute("mail", "mailingllist.aspx", new { controller = "mailinglist", action = "Index" });
            routes.MapRoute("shreet2", "shreet.aspx", new { controller = "shreetlogin", action = "shreet" });


            routes.MapRoute("guest", "guest.aspx", new { controller = "guest", action = "Index", page = 0 });
            routes.MapRoute("guest2", "guest.aspx/page/{page}", new { controller = "guest", action = "Index", page = 0 });
            routes.MapRoute("sharek", "sharek-braek", new { controller = "category", action = "Index", page = 0 });
            routes.MapRoute("sharek2", "sharek-braek/page/{page}", new { controller = "category", action = "Index", page = 0 });

            routes.MapRoute("polls", "polls", new { controller = "polls", action = "Index", page = 0 });
            routes.MapRoute("polls2", "polls/page/{page}", new { controller = "polls", action = "Index", page = 0 });
            //..............................................................................................
            routes.MapRoute("advancedsearch", "advancedsearch.aspx", new { controller = "search", action = "advancedsearch" });
            routes.MapRoute("search1", "search.aspx/{aaa}", new { controller = "search", action = "search", aaa = "", page = 0 });
            routes.MapRoute("search2", "search.aspx/{aaa}/page/{page}", new { controller = "search", action = "search", aaa = "", page = 0 });
            //......................................................................................................
            routes.MapRoute("constraints", "using.aspx", new { controller = "serveses", action = "constraints" });
            routes.MapRoute("donate", "donate.aspx", new { controller = "serveses", action = "donate" });
            routes.MapRoute("urlerror", "GeneralError.aspx", new { controller = "serveses", action = "GeneralError" });
            routes.MapRoute("jawal", "jawal.aspx", new { controller = "serveses", action = "jawal" });
            routes.MapRoute("barq", "barq.aspx", new { controller = "serveses", action = "barq" });
            routes.MapRoute("mobil", "mobil.aspx", new { controller = "serveses", action = "mobil" });
            routes.MapRoute("rssfeed", "rssfeed.aspx", new { controller = "serveses", action = "rssfeed" });
            routes.MapRoute("sitemape", "sitemape.aspx", new { controller = "serveses", action = "sitemape" });
            routes.MapRoute("contact", "contact.aspx/{s}", new { controller = "contact", action = "Index", s = "" });
            //routes.MapRoute("shreetlogin", "shreetlogin.aspx", new { controller = "shreetlogin", action = "Index" });
         
             routes.MapRoute("comment_vote", "comment_vote/{type}/{id}", new { controller = "akhbar", action = "article_comment_voting", id = 0 });

            // routes.MapRoute("gharb", "nahn-we-el-gharb", new { controller = "nahn_wa_elgharb", action = "Index" });
            // routes.MapRoute("gharb1", "nahn-we-el-gharb/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            // routes.MapRoute("gharb2", "nahn-we-el-gharb/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });

            routes.MapRoute("zakera", "zakera", new { controller = "category", action = "Index" });
            routes.MapRoute("zakera1", "zakera/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("zakera2", "zakera/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });

            routes.MapRoute("edara", "fan-el-edara", new { controller = "category", action = "Index" });
            routes.MapRoute("edara1", "fan-el-edara/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("edara2", "fan-el-edara/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });

            routes.MapRoute("mostshar", "mostashar", new { controller = "category", action = "Index" });
            routes.MapRoute("mostshar1", "mostashar/{cat}/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });
            routes.MapRoute("mostshar2", "mostashar/{cat}/page/{page}", new { controller = "category", action = "Index", cat = "", page = 0 });


            routes.MapRoute("monawaat", "akhbar-monawaa", new { controller = "category", action = "Index", cat = "monawaat", page = 0 });
            // routes.MapRoute("monawaatp", "akhbar-monawaa/page/{page}", new { controller = "category", action = "Index", cat = "monawaat", page = 0 });
            routes.MapRoute("sakhena", "manatek-sakhena", new { controller = "category", action = "Index" });
            routes.MapRoute("nawafez", "nawafez-siasia", new { controller = "category", action = "Index" });


            /*اشتغلت*/
            routes.MapRoute(
                "Default",                                              // Route name
                "{controller}/{action}/{id}",                           // URL with parameters
                new { controller = "Home", action = "Index", id = "" }  // Parameter defaults
            );//culture-and-economy


            //Estsharat
            // routes.MapRoute("Estsharat", "Estsharat/{year}/{month}/{day}/{id}" + "." + "html", new { controller = "akhbar", action = "article", cat1 = "Estsharat", cat2 = "" });
            //category/index/633
            routes.MapRoute("category", "category/index/{cat_id}", new { controller = "category", action = "index" });


            routes.MapRoute("hadath_elsaa", "{year}/{month}/{day}/{id}" + "." + "html", new { controller = "akhbar", action = "article", cat1 = "", cat2 = "" });
            //routes.MapRoute("Estsharat", "Estsharat/{year}/{month}/{day}/{id}" + "." + "html", new { controller = "akhbar", action = "article", cat1 = "Estsharat", cat2 = "" });
            //routes.MapRoute("aklaam", "Aklam-el-koraa/{year}/{month}/{day}/{id}" + "." + "html", new { controller = "akhbar", action = "article", cat1 = "Aklam-el-koraa", cat2 = "" }, new { year = @"\d{4}" });
            //routes.MapRoute("ektsaad", "culture-and-economy/{year}/{month}/{day}/{id}" + "." + "html", new { controller = "akhbar", action = "article", cat1 = "culture-and-economy", cat2 = "" }, new { year = @"\d{4}" });
            //routes.MapRoute("mesbar", "Al-Mesbar/{year}/{month}/{day}/{id}" + "." + "html", new { controller = "akhbar", action = "article", cat1 = "Al-Mesbar", cat2 = "" });
            //routes.MapRoute("tahkikat", "Tahkikat/{year}/{month}/{day}/{id}" + "." + "html", new { controller = "akhbar", action = "article", cat1 = "Tahkikat", cat2 = "" });
            routes.MapRoute("imagescat", "vedio-images/images/{year}/{month}/{day}/{id}" + "." + "html", new { controller = "image", action = "imgcattest" });
            routes.MapRoute("vedio", "vedio-images/vedio/{year}/{month}/{day}/{id}" + "." + "html", new { controller = "vedio", action = "index" });
            routes.MapRoute("sub_vedio", "vedio-images/{cat}/{year}/{month}/{day}/{id}" + "." + "html", new { controller = "vedio", action = "index" });

            routes.MapRoute("khabar", "{cat1}/{cat2}/{year}/{month}/{day}/{id}" + "." + "html", new { controller = "akhbar", action = "article" });
            routes.MapRoute("khabar2", "{cat1}/{year}/{month}/{day}/{id}" + "." + "html", new { controller = "akhbar", action = "article" });

            routes.MapRoute("khabar3", "{cat1}/{cat2}/{cat3}/{year}/{month}/{day}/{id}" + "." + "html", new { controller = "akhbar", action = "article" });

            routes.MapRoute("khabar_print", "{cat1}/{cat2}/{year}/{month}/{day}/{id}" + "." + "html/print", new { controller = "akhbar", action = "print" });
            routes.MapRoute("khabar2_print", "{cat1}/{year}/{month}/{day}/{id}" + "." + "html/print", new { controller = "akhbar", action = "print" });

            routes.MapRoute("khabar3_print", "{cat1}/{cat2}/{cat3}/{year}/{month}/{day}/{id}" + "." + "html/print", new { controller = "akhbar", action = "print" });

            //temp_barq
            routes.MapRoute("addcomment", "akhbar/article/akhbar/addcomments", new { controller = "akhbar", action = "addcomments" });
          // routes.MapRoute("old_marquee", "bar/islammemo.asp", new { controller = "temp_barq", action = "index" });

        }
    }
}