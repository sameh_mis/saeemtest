﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using memov5entity.Models;
namespace memov5entity
{
    public partial class rss : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("http://feeds.feedburner.com/islammemo/WMNM");

            Response.Clear();
            //Response.ClearHeaders();
            Response.AddHeader("Content-Type", "text/xml; charset=utf-8");

            int myid;
            int.TryParse(Request["id"], out myid);

            string temp = MyDb.get_rss(1, Page);
            switch (myid)
            {

                //منوعات---->طب وصحة ، عجائب وغرائب ، كوارث وحوداث ،نافذة اقتصادية ، علوم وتكنولوجيا ، نافذة ثقافية ، الحدقة ، المرأة ، الطفل
                case 1:
                    Response.Write(MyDb.get_rss(1, Page));
                    break;
                //تقارير ومقالات ----> تقارير عامة ، مترجمة ، ميدانية ، مقالات ، حوارات ، تحقيقات ، تأملات
                case 2:
                    Response.Write(MyDb.get_rss(2, Page));
                    break;
                //الأسرة ---->البيت السعيد ، اطفالنا ، زهرة ، إستشارات
                case 3:
                    Response.Write(MyDb.get_rss(3, Page));
                    break;
                //سياسة ---->عام الأخبار
                default:
                    Response.Write(MyDb.get_rss(0, Page));
                    break;
            }


            //Response.Filter = new rssfilter(Response.Filter);
        }

        protected override void InitializeCulture()
        {
            try
            {
                UICulture = Request["lang"];
                Culture = Request["lang"];
            }
            catch
            {
                UICulture = "ar-eg";
                Culture = "ar-eg";
            }
        }
    }
}