﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Numerics;
using System.Data.SqlClient;
using System.Data;

namespace memov5entity
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int f = 0;
            DataTable dt2 = new DataTable();
            BigInteger w = new BigInteger();
            BigInteger q = new BigInteger();
            BigInteger[] minnum = new BigInteger[12];
            SqlConnection con = new SqlConnection();
            con.ConnectionString = "Data Source=.;Initial Catalog=ip;Integrated Security=true";
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "get_max_ipsa";
            cmd.Connection = con;
            SqlDataAdapter dad = new SqlDataAdapter();
            dad.SelectCommand = cmd;
            DataTable dt = new DataTable();
            dad.Fill(dt);
            Response.Write(" BigInteger[] minnum ={");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string v = dt.Rows[i]["maxip"].ToString();
                string[] ss = v.Split('"');
                string value = ss[1];
                byte[] addrBytes = System.Net.IPAddress.Parse(value).GetAddressBytes();
                if (System.BitConverter.IsLittleEndian)
                {
                    //little-endian machines store multi-byte integers with the
                    //least significant byte first. this is a problem, as integer
                    //values are sent over the network in big-endian mode. reversing
                    //the order of the bytes is a quick way to get the BitConverter
                    //methods to convert the byte arrays in big-endian mode.
                    System.Collections.Generic.List<byte> byteList = new System.Collections.Generic.List<byte>(addrBytes);
                    byteList.Reverse();
                    addrBytes = byteList.ToArray();
                }
                ulong[] addrWords = new ulong[2];
                if (addrBytes.Length > 8)
                {
                    addrWords[0] = System.BitConverter.ToUInt64(addrBytes, 0);
                    w = addrWords[0];
                    addrWords[1] = System.BitConverter.ToUInt64(addrBytes, 8);
                    q = addrWords[1];
                    string c = Convert.ToString(w + " " + q);
                    c = c.Replace(" ", "");
                    BigInteger d = BigInteger.Parse(c);
                   // minnum[i] = d;

                    Response.Write(d + ",");

                  
                   
                }

            }
            Response.Write("};");
        }
    }
}