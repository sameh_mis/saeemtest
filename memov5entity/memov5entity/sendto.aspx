﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="sendto.aspx.cs" Inherits="memov5entity.sendto" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%="إرسال مقال إلى صديق" %> | <%="مفكرة الاسلام"%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="PRAGMA" content="NO-CACHE">
    <meta http-equiv="Expires" content="-1">
    <?xml version="1.0" encoding="windows-1256" ?>
</head>
<body style="text-align: center; font-size: 12pt;margin:0px;">
    <form id="form1" runat="server">
        <table width="400" style="text-align:right;" dir="rtl">
            <tr>
                <td>
                    <span id="article_title" style="font-size: 16pt; color: #ff6600">
                    
                    </span>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                <%="اسم الصديق الذي تريد الإرسال إليه"%>: 
                </td>
            </tr>
            <tr>
                <td>
                    
                    <asp:TextBox ID="txt_name_to" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="txt_name_to" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                <%="البريد الإلكترونى لصديقك"%>:    
                </td>
            </tr>
            <tr>
                <td>
                    
                    <asp:TextBox ID="txt_mail_to" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                        ErrorMessage="البريد الألكترونى غير صحيح" 
                        ControlToValidate="txt_mail_to" Display="Dynamic" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ControlToValidate="txt_mail_to" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                <%="اسم المرسل"%>: 
                </td>
            </tr>
            <tr>
                <td>
                    
                    <asp:TextBox ID="txt_name_from" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ControlToValidate="txt_name_from" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                <%="البريد الالكترونى"%>:    
                </td>
            </tr>
            <tr>
                <td>
                    
                    <asp:TextBox ID="txt_mail_from" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                        ErrorMessage="البريد الألكترونى غير صحيح" 
                        ControlToValidate="txt_mail_from" Display="Dynamic" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" ControlToValidate="txt_mail_from" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="Button1" runat="server" Text="ارسال" 
                        OnClick="Button1_Click" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
        </table>
        
        <script type="text/javascript" language="javascript">

            window.document.getElementById('article_title').innerHTML = window.opener.document.getElementById('hidden_article_inf').innerHTML;        
        </script>
    </form>
</body>
</html>
