﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace memov5entity
{
    public partial class mailing_list_remove_email : System.Web.UI.Page
    {
        string constr;
        SqlConnection cn;
        SqlCommand cmd;
        protected void Page_Load(object sender, EventArgs e)
        {
            constr = ConfigurationManager.ConnectionStrings["mailinglist"].ConnectionString;
            cn = new SqlConnection(constr);
            cmd = cn.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "remove_email";

            SqlParameter p1 = new SqlParameter("@guid", Request["id"]);
            cmd.Parameters.Add(p1);
            cn.Open();
            if (Request["id"].ToString().Trim().Length > 0)
                cmd.ExecuteNonQuery();
            cn.Close();
            //webdev2009@islammemo.cc
            Response.Write("تم الغاء الاشتراك بنجاح");
        }
    }
}