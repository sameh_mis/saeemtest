﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="general_marquees.aspx.cs" Inherits="memov5entity.general_marquees" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body style="margin-top:0px;">
    
    <%if(Request["type"]==null){ %>
     <marquee  direction="right" behavior="scroll" loop="0" scrolldelay="6" scrollamount="2" onmouseover="this.stop();" onmouseout="this.start();"><%=explore_marquee_important_news() %> </marquee>
   
    <%}else if(Request["type"]=="1"){ %>
    <marquee  direction="right" behavior="scroll" loop="0" scrolldelay="6" scrollamount="2" onmouseover="this.stop();" onmouseout="this.start();"><%=explore_marquee_important_news() %> </marquee>
   
    <%} else if(Request["type"]=="2"){ %>
    <marquee style="text-align:center;" direction="down" behavior="scroll" loop="0" scrolldelay="6" scrollamount="2" onmouseover="this.stop();" onmouseout="this.start();"><%=explore_marquee_important_news_up() %> </marquee>
   
    <%} else if(Request["type"]=="3"){ %>
    <marquee style="text-align:center;" direction="up" behavior="scroll" loop="0" scrolldelay="6" scrollamount="2" onmouseover="this.stop();" onmouseout="this.start();"><%=explore_marquee_important_news_up() %> </marquee>
   
    <%}else{ %>
    <marquee direction="right" behavior="scroll" loop="0" scrolldelay="6" scrollamount="2" onmouseover="this.stop();" onmouseout="this.start();"><%=explore_marquee_important_news() %> </marquee>
   
    <%} %>
   
</body>
</html>
