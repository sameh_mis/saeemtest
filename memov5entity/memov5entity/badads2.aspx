﻿<%@ Page Language="C#"  %>
<%@ Import Namespace="memov5entity.Models" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
  
    protected void Button1_Click(object sender, EventArgs e)
    {
        bool updated = true;
        try
        {
            int count = 0;
            string tempurl = txt_url.Text;
            string temp2 = "";
            if (tempurl.Contains("http") == false)
            {
                tempurl = "http://" + tempurl;
                temp2 = "http://";

            }
            Uri r = new Uri(tempurl);
            string sss = tempurl.Split('?')[0].Trim();
            if (r.AbsolutePath.Length > 1) sss = sss.Remove(sss.IndexOf(r.AbsolutePath));
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["test4ConnectionString"].ConnectionString);
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("select count(*)  from ads_complains where ad_url like @url ", conn);
            conn.Open();
            if (temp2 == "http://")
            {
                sss = sss.Substring(7);

            }
            cmd.Parameters.AddWithValue("@url", "%" + sss.Trim() + "%");
             count =Convert.ToInt32( cmd.ExecuteScalar());
            conn.Close();
            if (count == 0)
            {
                insert();
            }
            else
            {
                update(sss);
            }
        }
        catch
        {
         updated=   insert();
        }
        if(updated)
        form1.InnerHtml = "<center><font style='color:Red; right:300px;font-weight:bold; font-size:xx-large;'> نشكرك على تعاونك معنا وسوف تقوم الادارة بمراجعة الشكوى لحذف الاعلان</font></center>";
            
       
    }
    public bool insert()
    {
        if (txt_url.Text.Trim() == "" || txt_reason.Text.Trim()=="")
        {
            lbl_message.Text = "رجاء كتابة البيانات كاملة ";
            return false;
        }
        else
        {
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["test4ConnectionString"].ConnectionString);
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("INSERT INTO ads_complains (ad_url, reason,date,ip) VALUES (@url, @reason,@date,@ip)", conn);
            cmd.Parameters.AddWithValue("@url", txt_url.Text.Trim().ToLower());
            cmd.Parameters.AddWithValue("@reason", txt_reason.Text.Trim().ToLower());
            cmd.Parameters.AddWithValue("@date", DateTime.Now);
            cmd.Parameters.AddWithValue("@ip", Request.UserHostAddress);

            conn.Open();

            cmd.ExecuteNonQuery();
            conn.Close();
         
        }
        return true;
    }
    public void update(string sss)
    {
        System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["test4ConnectionString"].ConnectionString);
        System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("update ads_complains set complains_no=complains_no+1 where ad_url like @url ", conn);
        cmd.Parameters.AddWithValue("@url","%"+sss.Trim()+"%");
        conn.Open();
        cmd.ExecuteNonQuery();
        conn.Close();

    } 
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>شكاوى اعلانات جوجل</title>
    <link href="style_sheets/layout.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</head>
<body>
     <form id="form1" runat="server">
    
    
       
        
        <div runat="server" id="logindiv" align="center">
<table width="300" dir="rtl" cellpadding=0 cellspacing=0 style=" border-left:solid 2px #27a4dd;border-right:solid 2px #27a4dd;border-bottom:solid 2px #27a4dd;margin:10px;">
    <tr>
        <td colspan="2" align="center" style="background-color:#27a4dd;height:30px;font-size:large;">
        إبلغ عن اعلان مخالف
        </td>
    </tr>
    
        <tr>
            <td style="height:35px; font-size:large;" >
            رابط الاعلان
            </td>
            <td align="left">
                <asp:TextBox ID="txt_url" runat="server" ValidationGroup="login"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*" ControlToValidate="txt_url" ValidationGroup="login"></asp:RequiredFieldValidator></td>
        </tr>
         <tr>
            <td style="height:35px;font-size:large;" >
           سبب طلب الحذف
            </td>
            <td align="left">
                <asp:TextBox ID="txt_reason" runat="server" 
                    ValidationGroup="login"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*" ControlToValidate="txt_reason" ValidationGroup="login"></asp:RequiredFieldValidator></td>
        </tr>
       
         <tr>
            <td colspan="2" align="center" style="height:40px;" >
            <asp:Button ID="Button2" runat="server" Text="ارسال" ValidationGroup="login"
                        OnClick="Button1_Click" />
                <br />
                <asp:Label ID="lbl_message" runat="server" Text=""></asp:Label>
            </td>
        </tr>
    </table>
        </div>
        
    
   
    </form>
</body>
</html>
