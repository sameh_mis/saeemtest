﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="shreet2.aspx.cs" Inherits="memov5entity.shreet" %>
<div class="head_slice"></div>
<div class="jawal_border">

    <form id="Form1" runat="server">
 <table cellpadding="3" cellspacing="3" style="border-width: 0; border-collapse: collapse;text-align:right; margin-top:18px;"
            width="100%" dir="rtl">
            <tr>
                <td class="padding shreet_align" style="color: #226EA0;">
                    مكان كلمة مفكرة الإسلام :
                </td>
                <td>
               
                    <asp:RadioButtonList ID="rbl_mofakera" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1" Selected="true">خارج الشريط من خلال مربع ينطلق منه الشريط</asp:ListItem>
                        <asp:ListItem Value="0">داخل الشريط مع عرض الأخبار</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="padding shreet_align" style="color: #226EA0;">
                    اختر اتجاه شريط الأخبار:
                </td>
                <td>
                    <asp:RadioButtonList ID="rbl_dir" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="right" Selected="true">من اليسار إلى اليمين</asp:ListItem>
                        <asp:ListItem Value="up">من أسفل لأعلى</asp:ListItem>
                        <asp:ListItem Value="down">من اعلى لأسفل</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="padding shreet_align" style="color: #226EA0;">
                    قم بتحديد تنسيق خط كتابة الأخبار:
                </td>
                <td>
                    <asp:CheckBoxList ID="cbl_font" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="0">مائل</asp:ListItem>
                        <asp:ListItem Value="1">سميك</asp:ListItem>
                        <asp:ListItem Value="2">تحته خط</asp:ListItem>
                    </asp:CheckBoxList>
                </td>
            </tr>
            <tr>
                <td class="padding shreet_align" style="color: #226EA0;">
                اختر نوع خط كتابة الأخبار:
                </td>
                <td>
                    <asp:DropDownList ID="ddl_fontfamily" runat="server">
                    <asp:ListItem value="Akhbar MT">Akhbar MT</asp:ListItem>   
  <asp:ListItem value="Andalus">Andalus</asp:ListItem>    
  <asp:ListItem value="Arabic Transparent">Arabic Transparent</asp:ListItem> 
  <asp:ListItem value="Arial">Arial</asp:ListItem>   
  <asp:ListItem value="Courier">Courier</asp:ListItem>

  <asp:ListItem value="Monotype Koufi">Monotype Koufi</asp:ListItem>   
  <asp:ListItem value="MS Sans Serif">MS Sans Serif</asp:ListItem>
  <asp:ListItem value="Mudir MT">Mudir MT</asp:ListItem>   
  <asp:ListItem value="PT Simple Bold Ruled">PT Simple Bold Ruled</asp:ListItem>   
  <asp:ListItem value="Simplified Arabic">Simplified Arabic</asp:ListItem> 
  <asp:ListItem value="Tahoma">Tahoma</asp:ListItem>
  <asp:ListItem value="Times New Roman">Times New Roman</asp:ListItem>   
  <asp:ListItem value="Traditional Arabic">Traditional Arabic</asp:ListItem>
  
                    
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="padding shreet_align" style="color: #226EA0;">
                    اختر حجم خط كتابة الأخبار:
                </td>
                <td>
                    <asp:DropDownList ID="ddl_fontsize" runat="server">
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="padding shreet_align" style="color: #226EA0;">
                سرعة تحرك شريط الأخبار:
                </td>
                <td>
                    <asp:DropDownList ID="ddl_marq_speed" runat="server">
                    <asp:ListItem Value="1">1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                    <asp:ListItem Value="3">3</asp:ListItem>
                    <asp:ListItem Value="4">4</asp:ListItem>
                    <asp:ListItem Value="5">5</asp:ListItem>
                    <asp:ListItem Value="6">6</asp:ListItem>
                    <asp:ListItem Value="7">7</asp:ListItem>
                    <asp:ListItem Value="8">8</asp:ListItem>
                    <asp:ListItem Value="9">9</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="padding shreet_align" style="color: #226EA0;">
                اختر لون خلفية شريط الأخبار:
                </td>
                <td>
                    <asp:DropDownList ID="ddl_background" runat="server">
                    <asp:ListItem value="000000" dir="rtl">أسود</asp:ListItem>   
  <asp:ListItem value="#ffffff" dir="rtl">أبيض</asp:ListItem>   
  <asp:ListItem value="#ff0000" dir="rtl">أحمر</asp:ListItem>    
  <asp:ListItem value="#FF9900" dir="rtl">برتقالي</asp:ListItem>  
  <asp:ListItem value="#ffff00" dir="rtl">أصفر</asp:ListItem>   
  <asp:ListItem value="#008000" dir="rtl">أخضر</asp:ListItem>   
  <asp:ListItem value="#0000FF" dir="rtl">أزرق</asp:ListItem>   
  <asp:ListItem value="#33CCFF" dir="rtl">نيلي</asp:ListItem>   
  <asp:ListItem value="#CC66FF" dir="rtl">بنفسجي</asp:ListItem>
  <asp:ListItem value="0">تخصيص</asp:ListItem>  
                    </asp:DropDownList>
                    أو ضع القيمة التي تناسب موقعك
                    <asp:TextBox ID="TextBox1" runat="server" Width="52px" Enabled="false"></asp:TextBox>
                    مثلاً : 
                    <span dir="ltr">#ffffff</span>
                </td>
            </tr>
            <tr>
                <td class="padding shreet_align" style="color: #226EA0;">
                اختر لون كتابة الأخبار:
                </td>
                <td>
                    <asp:DropDownList ID="ddl_forecolor" runat="server">
                    <asp:ListItem value="ffffff">أبيض</asp:ListItem>   
  <asp:ListItem value="#ff0000">أحمر</asp:ListItem>    
  <asp:ListItem value="#FF9900">برتقالي</asp:ListItem>  
  <asp:ListItem value="#ffff00">أصفر</asp:ListItem>   
  <asp:ListItem value="#008000">أخضر</asp:ListItem>   
  <asp:ListItem value="#0000FF">أزرق</asp:ListItem>   
  <asp:ListItem value="#33CCFF">نيلي</asp:ListItem>   
  <asp:ListItem value="#CC66FF">بنفسجي</asp:ListItem> 
  <asp:ListItem value="#000000">أسود</asp:ListItem>
  <asp:ListItem value="0">تخصيص</asp:ListItem>
                    </asp:DropDownList>
                    أو ضع القيمة التي تناسب موقعك
                    <asp:TextBox ID="TextBox2" runat="server" Width="53px" Enabled="false"></asp:TextBox>
                    مثلاً :
                    <span dir="ltr">#000000</span>
                </td>
            </tr>
            <tr>
                <td class="padding shreet_align" style="color: #226EA0;">
                نوع شريط الاخبار:
                </td>
                <td>
                    <asp:DropDownList ID="ddlcat" runat="server">
                    <asp:ListItem Value="0">عام</asp:ListItem>
                    <asp:ListItem Value="4">سياسة</asp:ListItem>
                    <asp:ListItem Value="1">منوعات</asp:ListItem>
                    <asp:ListItem Value="2">تقارير ومقالات</asp:ListItem>
                    <asp:ListItem Value="3">الاسرة</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
            <td colspan=2 align="center">
            <hr>
            
            <iframe id="myiframe" runat="server" width="450" height="100" frameborder="0" scrolling="no"></iframe>
			
			
	        <hr>
            </td>
            </tr>
            
            <tr>
            <td colspan="2" align="center">
                <asp:Button ID="Button1" runat="server" Text="حفظ" 
                    OnClick="Button1_Click" />
            </td>
            </tr>
        </table>
        
        
        <div class="padding shreet_align" style="text-align:right;">
        الكود 
        <br />
            <asp:TextBox Width="80%" Rows="3" ID="TextBox3" runat="server" ReadOnly="true" TextMode="multiLine"></asp:TextBox>
        </div>
    
    
    
    
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        
        <div class="padding shreet_align"><br>
         </div> 
    </form>
 </div>





