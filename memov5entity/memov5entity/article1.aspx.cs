﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using memov5entity.Models;

namespace memov5entity
{
    public partial class article1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string art_url = Request.Url.AbsoluteUri;

            if (art_url.Contains("*"))
            {

                art_url = art_url.Split('*')[0];

            }

            if (String.IsNullOrEmpty(Request["id"]))
            {
                Response.Redirect("/");
            }
            else
            {
                //Response.Write(Request["id"]);
                test4Entities dt = new test4Entities();
                var my_id = Convert.ToInt32(Request["id"].Split('*')[0]);
                //  string Art_url = dt.Articles.Where(p => p.ID == Convert.ToInt32(Request["id"].Split('*')[0])).FirstOrDefault().url;
                string Art_url = dt.Articles.Where(p => p.ID == my_id).FirstOrDefault().url;

                Response.Redirect(@"~/" + Art_url);

            }


        }
    }
}