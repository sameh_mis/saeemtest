﻿using System;
using System.IO;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using memov5entity.Models;

namespace memov5entity
{
    public partial class grss : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Response.Clear();
            //Response.ClearHeaders();
            Response.AddHeader("Content-Type", "text/xml; charset=utf-8");

            int myid;
            int.TryParse(Request["id"], out myid);

            //string temp = MyDb.get_rss(1, Page);
            switch (myid)
            {

                //ãäæÚÇÊ---->ØÈ æÕÍÉ ¡ ÚÌÇÆÈ æÛÑÇÆÈ ¡ ßæÇÑË æÍæÏÇË ¡äÇÝÐÉ ÇÞÊÕÇÏíÉ ¡ Úáæã æÊßäæáæÌíÇ ¡ äÇÝÐÉ ËÞÇÝíÉ ¡ ÇáÍÏÞÉ ¡ ÇáãÑÃÉ ¡ ÇáØÝá
                case 1:
                    Response.Write(MyDb.get_rss(1, Page));
                    break;
                //ÊÞÇÑíÑ æãÞÇáÇÊ ----> ÊÞÇÑíÑ ÚÇãÉ ¡ ãÊÑÌãÉ ¡ ãíÏÇäíÉ ¡ ãÞÇáÇÊ ¡ ÍæÇÑÇÊ ¡ ÊÍÞíÞÇÊ ¡ ÊÃãáÇÊ
                case 2:
                    Response.Write(MyDb.get_rss(2, Page));
                    break;
                //ÇáÃÓÑÉ ---->ÇáÈíÊ ÇáÓÚíÏ ¡ ÇØÝÇáäÇ ¡ ÒåÑÉ ¡ ÅÓÊÔÇÑÇÊ
                case 3:
                    Response.Write(MyDb.get_rss(3, Page));
                    break;
                //ÓíÇÓÉ ---->ÚÇã ÇáÃÎÈÇÑ
                default:
                    Response.Write(MyDb.get_rss(0, Page));
                    break;
            }


            //Response.Filter = new rssfilter(Response.Filter);
        }

        protected override void InitializeCulture()
        {
            try
            {
                UICulture = Request["lang"];
                Culture = Request["lang"];
            }
            catch
            {
                UICulture = "ar-eg";
                Culture = "ar-eg";
            }
        }
    }
}