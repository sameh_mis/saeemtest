﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using memov5entity.Models;
namespace memov5entity.Controllers
{
    public class shreetloginController : Controller
    {
        //
        // GET: /shreet/

        public ActionResult Index()
        {
            @ViewData["errorMessage"] = "";
            @Session["shreet_uname"] = "";
            @ViewData["general_marquee"] = "";
            @ViewData["Request_type"] = @Request["type"];
          
            return View();
        }
        [HttpPost]
        public ActionResult Index(string logname, string logpass, string name2, string pass2, string email2, string domain)
        {
            shreetlogin log = new shreetlogin();
            if (logname != null && logpass != null)
            {
                string check_name = log.shreet_log(logname, logpass);
                if (check_name.Length > 0)
                {
                    Session["shreet_uname"] = check_name;
                    Response.Redirect("/shreet.aspx");
                }
                else
                {
                    ViewData["errorMessage"] = "برجاء التأكد من إدخال البيانات بصورة صحيحة";
                }
            }
            else if (name2 != null && pass2 != null && email2 != null && domain != null)
            {
                string n = log.shreet_register(name2, pass2, email2, domain);
                if (n.Length > 0)
                {
                    Session["shreet_uname"] = n;
                    Response.Redirect("/shreet.aspx");
                }
                else
                {
                    ViewData["errorMessage2"] = "برجاء التأكد من إدخال البيانات بصورة صحيحة";
                }


            }
            return View();
        }

        public ActionResult shreet2()
        {
            @ViewData["errorMessage"] = "";
            return View();
        }
        public ActionResult shreet()
        {
            string s = "";

            if (Session["shreet_uname"] == null || Session["shreet_uname"].ToString().Equals(s))
            {
                Response.Redirect("shreetlogin.aspx");

            }
      

            return View();

        }

        public ActionResult GeneralMarquees()
        {
            shreetlogin sh = new shreetlogin();
            string s = "";
            if (Request["type"] == null)
            {
                s = sh.explore_marquee_important_news();
                ViewData["Request_type"] = Request["type"];

            }
            else if (Request["type"] == "1")
            {
                s = sh.explore_marquee_important_news();
                ViewData["Request_type"] = Request["type"];
            }
            else if (Request["type"] == "2")
            {
                s = sh.explore_marquee_important_news_up();
                ViewData["Request_type"] = Request["type"];

            }
            else if (Request["type"] == "3")
            {
                s = sh.explore_marquee_important_news_up();
                ViewData["Request_type"] = Request["type"];

            }
            else
            {
                s = sh.explore_marquee_important_news();
                ViewData["Request_type"] = @Request["type"];

            }
            ViewData["general_marquee"] = s;
            return View();
        }



    }
}
