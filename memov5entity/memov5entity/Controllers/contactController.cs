﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;
using memov5entity.Models;
using imgver.NumberGenerator;
using memov5entity.Models;

namespace memov5entity.Controllers
{
    public class contactController : Controller
    {
        //
        // GET: /contact/

         [OutputCache(Duration = 300)]
        public ActionResult Index(string s)
        {

            count = 0;
            CImgVerify cI = new CImgVerify();
            string sTxt = cI.getRandomAlphaNumeric();
            ViewData["captcha"] = sTxt;
            ViewData["thanks"] = "";
            try
            {
                Bitmap bmp = cI.generateImage(sTxt);

                bmp.Save(Request.PhysicalApplicationPath + "confirm_img.jpg");

                bmp.Dispose();
            }
            catch (Exception ex) { Response.Write(ex); }


            contactviewmodel c = new contactviewmodel(s);
            return View(c);
        }
        static int count = 0;
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Index(string s, string CatID, string SName, string SEmail, string Country, string Title, string Body)
        {
            contactviewmodel c = new contactviewmodel(s);
            count++;
            //, STitle, , , IP, , RealCountry, Telephon, , Notes, , Date, Opened, Repliead) VALUES (@CatID, @STitle, @SName, @SEmail, @IP, @Country, @RealCountry, @Telephon, @Title, @Notes, @Body
            CImgVerify cI = new CImgVerify();
            string sTxt = cI.getRandomAlphaNumeric();
            ViewData["captcha"] = sTxt;
            ViewData["thanks"] = "";
            Bitmap bmp = cI.generateImage(sTxt);

            bmp.Save(Request.PhysicalApplicationPath + "confirm_img.jpg");

            bmp.Dispose();


            //// Validation logic
            //if (SName.Trim().Length == 0)
            //    ModelState.AddModelError("SName", "من فضلك ادخل الاسم");
            //if (SEmail.Trim().Length == 0)
            //    ModelState.AddModelError("SEmail", "من فضلك ادخل البريد الالكترونى");
            //if (SEmail.Trim().Length > 0 && Regex.IsMatch(SEmail, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*") == false)
            //    ModelState.AddModelError("SEmail", " من فضلك ادخل البريد الاكترونى بصورة صحيحة");
            //if (Country.Trim().Length == 0)
            //    ModelState.AddModelError("Country", "من فضلك ادخل البلد");
            //if (Title.Trim().Length == 0)
            //    ModelState.AddModelError("Title", "من فضلك ادخل عنوان الرسالة");
            //if (Body.Trim().Length == 0)
            //    ModelState.AddModelError("Body", "من فضلك ادخل الرسالة");
            //if (!ModelState.IsValid)
            //    return View(c);

            if (count < 2)
                c.insert_contact(CatID, SName, SEmail, Country, Title, Body, Request.UserHostAddress);
            ViewData["thanks"] = "نشكرك على المشاركة";
            return View(c);
        }

    }
}


