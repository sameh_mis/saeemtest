﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using memov5entity.Models;



namespace memov5entity.Controllers
{
    public class pollsController : Controller
    {
        //
        // GET: /polls/
        [OutputCache(Duration = 300, VaryByCustom = "ismobile")]
        public ActionResult Index(int? page)
        {

            pollsviewmodel p = new pollsviewmodel(Convert.ToInt32(page));

            ViewData["cdate"] = p.getlongdate();

            return View(p);
        }
           [OutputCache(Duration = 300, VaryByCustom = "ismobile")]
        public ActionResult ajaxpoll(string bab)
        {
            // Response.Redirect("/polls/ajaxpollresult?bab=" + bab);
            ajaxpollviewmodel p = new ajaxpollviewmodel(bab);
            string pid = p.get_poll_id().ToString();
            if (Request.Browser.Cookies == true && Request.Cookies["c" + pid] != null)
            {
                string s = Request.Cookies["c" + pid].Value;

                Response.Redirect("/polls/ajaxpollresult?bab=" + bab);

            }
            return View(p);
        }
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult ajaxpoll(string bab,string choice)
        //{
        //    ajaxpollviewmodel p = new ajaxpollviewmodel(bab);
        //    p.add_voting(choice);

        //    return View(p);
        //}
           [OutputCache(Duration = 300, VaryByCustom = "ismobile")]
        public ActionResult ajaxpollresult(string bab, string choice)
        {
            ajaxpollviewmodel p = new ajaxpollviewmodel(bab);
            int b = 0;
            if (choice != "result")
                b = p.add_voting(choice);
            //string pid = p.get_poll_id().ToString();
            if (b != 0)
            {
                HttpCookie coke = new HttpCookie("c" + b);
               coke.Value = b.ToString();
                coke.Expires = DateTime.Now.AddMonths(1);
             //  coke.Domain = ".islammemo.cc";
                Response.Cookies.Add(coke);

            }

            return View(p);
        }
        public ActionResult popup_poll(int? poll_id)
        {
            // pollsDataContext dt = new pollsDataContext();
            weekly_pollSQLEntities dt = new weekly_pollSQLEntities();
            tblPoll poll = dt.tblPolls.Where(p => p.id_no == poll_id).FirstOrDefault();
            //int p_id = Convert.ToInt32(poll_id);
            //pollsviewmodel p = new pollsviewmodel(1);
            //tblPoll p = pollsviewmodel.getpollbyid(p_id);
            ViewData["poll"] = poll;
            return View();

        }
        public ActionResult popup_poll_result(int? poll_id)
        {
            //  pollsDataContext dt = new pollsDataContext();
            weekly_pollSQLEntities dt = new weekly_pollSQLEntities();
            tblPoll poll = dt.tblPolls.Where(p => p.id_no == poll_id).FirstOrDefault();
            //int p_id = Convert.ToInt32(poll_id);
            //pollsviewmodel p = new pollsviewmodel(1);
            //tblPoll p = pollsviewmodel.getpollbyid(p_id);
            ViewData["poll"] = poll;
            return View();

        }


    }
}
