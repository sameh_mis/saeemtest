﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using memov5entity.Models;

namespace memov5entity.Controllers
{
    public class partialsController : Controller
    {
        //
        // GET: /partials/
        partialsviewmodel model = new partialsviewmodel();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult syria_tyhawra_tkareer()
        {
            return View(model);
        }
        public ActionResult reports_search_socialicons()
        {
            return View(model);
        }

        public ActionResult imp_news()
        {
            return View(model);
        }
        [OutputCache(Duration = 300)]
        public ActionResult top_articles()
        {

            // ViewData["r"] = h.topreadingarticle();
            // ViewData["s"] = h.topsentarticle();
            // ViewData["c"] = h.topcommentarticle();
            return View(model);
        }
        public ActionResult services()
        {
            return View();
        }

    }
}
