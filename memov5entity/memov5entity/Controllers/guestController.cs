﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using memov5entity.Models;
using System.Drawing;
using System.Text.RegularExpressions;
using memov5entity.Models;
using imgver.NumberGenerator;

namespace memov5entity.Controllers
{
    public class guestController : Controller
    {
        //
        // GET: /guest/
       [OutputCache(Duration = 300)]
        public ActionResult Index(int? page)
        {

            a = 0;
            int page_count = Convert.ToInt32(page);
            guestviewmodel g = new guestviewmodel(page_count);




            CImgVerify cI = new CImgVerify();
            string sTxt = cI.getRandomAlphaNumeric();
            ViewData["captcha"] = sTxt;
            ViewData["thanks"] = "";
            Bitmap bmp = cI.generateImage(sTxt);

            bmp.Save(Request.PhysicalApplicationPath + "confirm_img.jpg");

            bmp.Dispose();
            ViewData["cdate"] = g.getlongdate();
            return View(g);
        }

        static int a = 0;
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Index(int? page, string SName, string Title, string SEmail, string Body, string Country)
        {

            a++;

            CImgVerify cI = new CImgVerify();
            string sTxt = cI.getRandomAlphaNumeric();
            Random r = new Random();

            ViewData["captcha"] = sTxt;
            ViewData["thanks"] = "";
            Bitmap bmp = cI.generateImage(sTxt);

            bmp.Save(Request.PhysicalApplicationPath + "confirm_img.jpg");

            bmp.Dispose();
            int page_count = Convert.ToInt32(page);
            guestviewmodel g = new guestviewmodel(page_count);



            //// Validation logic
            //if (SName.Trim().Length == 0)
            //    ModelState.AddModelError("SName", "من فضلك ادخل الاسم");
            //if (SEmail.Trim().Length == 0)
            //    ModelState.AddModelError("SEmail", "من فضلك ادخل البريد الإليكتروني");
            ////\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*
            //if (SEmail.Trim().Length > 0 && Regex.IsMatch(SEmail, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*") == false)
            //    ModelState.AddModelError("SEmail", " من فضلك ادخل البريد الاكترونى بصورة صحيحة");
            //if (Title.Trim().Length == 0)
            //    ModelState.AddModelError("Title", "من فضلك ادخل عنوان التعليق");

            //if (Body.Trim().Length == 0)
            //    ModelState.AddModelError("Body", "من فضلك ادخل التعليق");

            //if (!ModelState.IsValid)
            //    return View(g);



            try
            {
                if (a < 2)
                {
                    string ip = Request.UserHostAddress;
                    g.add_comment(SName, SEmail, ip, Country, Title, Body);
                    ViewData["thanks"] = "نشكرك على المشاركة";
                }

                return View(g);
            }
            catch
            {
                return View(g);
            }
        }


    }
}
