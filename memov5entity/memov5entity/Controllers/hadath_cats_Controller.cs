﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using memov5entity.Models;

namespace memov5entity.Controllers
{
    public class hadath_cats_Controller : Controller
    {
        //
        // GET: /hadath_cats_/
        //   [OutputCache(Duration = int.MaxValue, VaryByParam = "*")]
        [OutputCache(Duration = 300)]
        public ActionResult Index(string cat, int? page)
        {
            int p = Convert.ToInt32(page);
            string category_url = HttpContext.Request.Url.LocalPath;

            hadath_cats c;

            c = new hadath_cats(category_url, p);
            //c = new hadath_cats();
            //categoryviewmodel cat_model = new categoryviewmodel(category_url, page);
            // ViewData["cdate"] = c.getlongdate();
            return View(c);
        }



    }
}
