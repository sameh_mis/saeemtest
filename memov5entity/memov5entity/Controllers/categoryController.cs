﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using memov5entity.Models;

namespace memov5entity.Controllers
{
    public class categoryController : Controller
    {
        //
        // GET: /category/
    [OutputCache(Duration = 300,VaryByParam="*",VaryByCustom = "ismobile")]
        public ActionResult Index(string cat, int? page)
        {
            string category_url = HttpContext.Request.Url.LocalPath;
            if (cat == "monawaat")
            {
                category_url = "/" + cat;
            }
            // string category_url = "/" + cat;
            categoryviewmodel cat_model = new categoryviewmodel(category_url, page);

            return View(cat_model);
        }
     


    }
}
