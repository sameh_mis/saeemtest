﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using memov5entity.Models;
using System.Drawing;
using System.Web.UI;
using System.Text.RegularExpressions;
using Recaptcha;
using memov5entity.Models;
namespace memov5entity.Controllers
{
    public class akhbarController : Controller
    {
        //
        // GET: /akhbar/

      //  islammemoDataContext dt = new islammemoDataContext();
        test4Entities dt = new test4Entities();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult print(string cat1, string cat2, int? year, int? month, int? day, int? id)
        {

            singlearticle s = new singlearticle(cat1, cat2, year, month, day, id);

            return View(s);
        }
        // [OutputCache(VaryByParam="*",Duration=int.MaxValue) ]
        public ActionResult testart()
        {
            Article a = (from p in dt.Articles
                         where p.stat == 8
                         orderby p.Date descending
                         select p).FirstOrDefault();
            ViewData["art"] = a;
            ViewData["vdarts"] = (from p in dt.Articles
                                  where p.cat_id == 846 && p.stat == 8 && p.Date < DateTime.Now
                                  orderby p.Date descending
                                  select p).Take(3).ToList();

            return View();
        }
        
        public ActionResult article(string cat1, string cat2, int? year, int? month, int? day, int? id)
        {

            try
            {
                int myid = Convert.ToInt32(id);
                dt.ExecuteStoreCommand("update dbo.Articles  set Readers=Readers+1 where ID={0}", myid);
            }
            catch { }

            return cacheart(cat1, cat2, year, month, day, id);

        }
        [OutputCache(Duration = 300, VaryByParam = "id", VaryByCustom = "ismobile")]
        public ActionResult cacheart(string cat1, string cat2, int? year, int? month, int? day, int? id)
        {
           
            singlearticle s = new singlearticle(cat1, cat2, year, month, day, id);

            return View("article" , s);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [RecaptchaControlMvc.CaptchaValidator]
        public ActionResult article(string cat1, string cat2, int? year, int? month, int? day, int? id, string title, string body, string username, int article_id, string url, bool captchaValid)
        {
          
            singlearticle s = new singlearticle(cat1, cat2, year, month, day, id);
           
            if (!captchaValid)
            {
                ModelState.AddModelError("", "فضلا اكتب الحروف التى بالصورة لتتمكن من التعليق");
            }
            else
            {
                ViewData["end"] = "0";

                // try { if (Request["lang"].Contains("ar") == false && string.IsNullOrEmpty(Request["lang"]) == false) s = new e_singlearticle(cat1, cat2, year, month, day, id); master = "Site_en"; }
                // catch { }
                // Validation logic
                if (title.Trim().Length == 0)
                    ModelState.AddModelError("title", "من فضلك ادخل عنوان التعليق");
                if (body.Trim().Length == 0)
                    ModelState.AddModelError("body", "من فضلك ادخل التعليق");
                if (username.Trim().Length == 0)
                    ModelState.AddModelError("username", "من فضلك ادخل الاسم");
                if (!ModelState.IsValid)
                    return View( s);

                if (Regex.IsMatch(url, @"http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?"))
                {
                    ViewData["end"] = "1";
                    return View( s);
                }
                try
                {
                    comment c2 = new comment();
                    c2.username = username;
                    c2.cdate = DateTime.Now;
                    c2.body = body.Replace("\n", "<br>");
                    c2.email = url;
                    c2.allow = false;
                    c2.article_id = article_id;
                    c2.title = title;
                    dt.AddTocomments(c2);
                   dt.SaveChanges();
                    //dt.comments.InsertOnSubmit(c2);
                   // dt.SubmitChanges();
                    ViewData["end"] = "1";
                    return View( s);
                }
                catch
                {
                    return View(s);
                }

            }
            return View(s);
        }

        public ActionResult addcomments(int? id)
        {
            int Id = Convert.ToInt32(id);
            ViewData["id"] = Id;
            return View();
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult addcomments(string title, string body, string username, int? article_id)
        {

            comment c2 = new comment();
            c2.username = username;
            c2.cdate = DateTime.Now;
            c2.body = body.Replace("\n", "<br>");
            c2.allow = false;
            c2.article_id = article_id;
            c2.title = title;
            dt.AddTocomments(c2);
            dt.SaveChanges();
           
            return View();
        }
        public ActionResult comments(int? id)
        {

            //article a = dt.Articles.Where(p => p.ID == Convert.ToInt32(id)).FirstOrDefault();
            // List<comment> comments = dt.comments.Where(p => p.article_id == id).ToList();
            List<comment> comments = (from p in dt.comments
                                      where p.article_id == id && p.allow == true
                                      orderby p.id descending
                                      select p).ToList();
            ViewData["comments"] = comments;

            return View();
        }
        //public  List<Article> getlastvdios()
        //  {
        //      return (from p in dt.Articles
        //              where p.cat_id == 846 && p.stat == 8 && p.Date < DateTime.Now
        //              orderby p.Date descending
        //              select p).Take(3).ToList();
        //  }



        public ActionResult article_comment_voting(bool type, int? id)
        {
            //ViewData["type"] = type.ToString();
            int c_id = Convert.ToInt32(id);
            comment c = dt.comments.Where(p => p.id == c_id).FirstOrDefault();
            if (c.negative_openion == null)
                c.negative_openion = 0;
            c.negative_openion = c.negative_openion + 1;
            dt.SaveChanges();
            return View();
        }

    }
}
