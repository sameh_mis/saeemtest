﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using memov5entity.Models;

namespace memov5entity.Controllers
{
    public class hadathController : Controller
    {
        //
        // GET: /hadath/
        test4Entities dt = new test4Entities();
        hadathviewmodel model = new hadathviewmodel();
       [OutputCache(Duration = 300)]
        public ActionResult Index()
        {
          ViewData["cdate"] = model.getlongdate();
            return View(model);
        }
      
         [OutputCache(Duration = 300)]
        public ActionResult archive(int? page)
        {

            ViewData["page"] = Convert.ToInt32(page);
            ViewData["cdate"] = model.getlongdate();
            return View(model);
        }
        [OutputCache(Duration = 12000)]
        public ActionResult previous_events()
        {

            return View(model);
        }

    }
}
