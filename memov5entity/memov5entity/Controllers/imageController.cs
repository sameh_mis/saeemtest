﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using memov5entity.Models;
using imgver.NumberGenerator;
using System.Drawing;
using Recaptcha;
namespace memov5entity.Controllers
{
    public class imageController : Controller
    {
        //
        // GET: /image/
        // islammemoDataContext dt = new islammemoDataContext();
        test4Entities dt = new test4Entities();
        [OutputCache(Duration = 300, VaryByCustom="ismobile")]
        public ActionResult imgcattest(int? year, int? month, int? day, int? id)
        {
            CImgVerify cI = new CImgVerify();
            string sTxt = cI.getRandomAlphaNumeric();
            ViewData["captcha"] = sTxt;
            ViewData["thanks"] = "";
            try
            {
                Bitmap bmp = cI.generateImage(sTxt);

                bmp.Save(Request.PhysicalApplicationPath + "confirm_img.jpg");

                bmp.Dispose();
            }
            catch (Exception ex) { Response.Write(ex); }
            imageviwmodel im = new imageviwmodel(year, month, day, id);

            //Homeviewmodel h = new Homeviewmodel();
            // ViewData["cdate"] = im.A.LongDate;
            try
            {
                dt.ExecuteStoreCommand("update dbo.Articles  set Readers=Readers+1 where ID={0}", id);
            }
            catch { }


            return View(im);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [RecaptchaControlMvc.CaptchaValidator]
        public ActionResult imgcattest(int? year, int? month, int? day, int? id, string Title, string Body, string SName, int article_id, string SEmail, bool captchaValid)
        {
            CImgVerify cI = new CImgVerify();
            string sTxt = cI.getRandomAlphaNumeric();
            ViewData["captcha"] = sTxt;
            ViewData["thanks"] = "";
            try
            {
                Bitmap bmp = cI.generateImage(sTxt);

                bmp.Save(Request.PhysicalApplicationPath + "confirm_img.jpg");

                bmp.Dispose();
            }
            catch (Exception ex) { Response.Write(ex); }

            ViewData["end"] = "0";
            imageviwmodel image = new imageviwmodel(year, month, day, id);

            bool commentm = image.insertcomment(Title, Body, SName, article_id, SEmail);
            if (commentm == true)
            {
                ViewData["end"] = "1";
            }
            return View(image);
        }

        public ActionResult comments(int? id)
        {

            //article a = dt.Articles.Where(p => p.ID == Convert.ToInt32(id)).FirstOrDefault();
            // List<comment> comments = dt.comments.Where(p => p.article_id == id).ToList();
            List<comment> comments = (from p in dt.comments
                                      where p.article_id == id && p.allow == true
                                      orderby p.id descending
                                      select p).ToList();

            ViewData["comments"] = comments;

            return View();
        }


    }
}
