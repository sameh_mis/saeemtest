﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using memov5entity.Models;
using System.Drawing;
using Recaptcha;
using imgver.NumberGenerator;
using memov5entity.Models;

namespace memov5entity.Controllers
{
    public class vedioController : Controller
    {
        //
        // GET: /vedio/
        test4Entities dt = new test4Entities();
        [OutputCache(Duration = 300, VaryByCustom = "ismobile")]
        public ActionResult Index(int? year, int? month, int? day, int? id)
        {
            CImgVerify cI = new CImgVerify();
            string sTxt = cI.getRandomAlphaNumeric();
            ViewData["captcha"] = sTxt;
            ViewData["thanks"] = "";
            try
            {
                Bitmap bmp = cI.generateImage(sTxt);

                bmp.Save(Request.PhysicalApplicationPath + "confirm_img.jpg");

                bmp.Dispose();
            }
            catch (Exception ex) { Response.Write(ex); }

            try
            {
                dt.ExecuteStoreCommand("update dbo.Articles  set Readers=Readers+1 where ID={0}", id);
            }
            catch { }

            vedioviewmodel vdio = new vedioviewmodel(year, month, day, id); ViewData["cdate"] = vdio.A.LongDate;
            return View(vdio);



        }
        [AcceptVerbs(HttpVerbs.Post)]
        [RecaptchaControlMvc.CaptchaValidator]
        public ActionResult Index(int? year, int? month, int? day, int? id, string Title, string Body, string SName, int article_id, string SEmail, bool captchaValid)
        {

            CImgVerify cI = new CImgVerify();
            string sTxt = cI.getRandomAlphaNumeric();
            ViewData["captcha"] = sTxt;
            ViewData["thanks"] = "";
            try
            {
                Bitmap bmp = cI.generateImage(sTxt);

                bmp.Save(Request.PhysicalApplicationPath + "confirm_img.jpg");

                bmp.Dispose();
            }
            catch (Exception ex) { Response.Write(ex); }

            ViewData["end"] = "0";
            vedioviewmodel vdio = new vedioviewmodel(year, month, day, id);
            //if (!captchaValid)
            //{
            //    ModelState.AddModelError("", "فضلا اكتب الحروف التى بالصورة لتتمكن من التعليق");
            //}
            //// Validation logic
            //if (Title.Trim().Length == 0)
            //    ModelState.AddModelError("Title", "من فضلك ادخل عنوان التعليق");
            //if (Body.Trim().Length == 0)
            //    ModelState.AddModelError("Body", "من فضلك ادخل التعليق");
            //if (SName.Trim().Length == 0)
            //    ModelState.AddModelError("SName", "من فضلك ادخل الاسم");
            //if (!ModelState.IsValid)
            //    return View(vdio);

            bool commentm = vdio.insertcomment(Title, Body, SName, article_id, SEmail);
            if (commentm == true)
            {
                ViewData["end"] = "1";
            }
            return View(vdio);
        }

        public ActionResult comments(int? id)
        {

            //article a = dt.Articles.Where(p => p.ID == Convert.ToInt32(id)).FirstOrDefault();
            // List<comment> comments = dt.comments.Where(p => p.article_id == id).ToList();
            List<comment> comments = (from p in dt.comments
                                      where p.article_id == id && p.allow == true
                                      orderby p.id descending
                                      select p).ToList();
            ViewData["comments"] = comments;

            return View();
        }
    }
}
