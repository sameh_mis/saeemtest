﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using memov5entity.Models;

namespace memov5entity.Controllers
{
    public class mailinglistController : Controller
    {
        //
        // GET: /mailinglist/
        [OutputCache(Duration = 300)]
        public ActionResult Index()
        {
            ViewData["validMessage"] = "";
            return View();
        }
        [HttpPost]
        public ActionResult Index(string SEmail, string submitbutton)
        {
            mailing mail_list = new mailing();
            if (submitbutton == "اشتراك")
            {
                string ret = mail_list.add_email(SEmail);
                ViewData["validMessage"] = ret;
            }
            else if (submitbutton == "إلغاء اشتراك")
            {
                string remove_ret = mail_list.Remove_email_share(SEmail);
                ViewData["validMessage"] = remove_ret;

            }
            // string button2 = Request["sendMSG2"].ToString();

            return View();

        }

    }
}
