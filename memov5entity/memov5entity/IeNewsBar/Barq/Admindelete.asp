<% If Session("project4_status") <> "login" Then Response.Redirect "login.asp" %>
<% If Session("project4_status_UserID") = "" And Session("project4_status_UserLevel") <> -1 Then Response.Redirect "login.asp" %>
<%
Response.expires = 0
Response.expiresabsolute = Now() - 1
Response.addHeader "pragma", "no-cache"
Response.addHeader "cache-control", "private"
Response.CacheControl = "no-cache"
%>
<!--#include file="db.asp"-->
<!--#include file="aspmkrfn.asp"-->
<%
Response.Buffer = True

' Single delete record
key = Request.querystring("key")
If key = "" Or IsNull(key) Then
	key = Request.Form("key")
End If
If key = "" Or IsNull(key) Then Response.Redirect "Adminlist.asp"
sqlKey = sqlKey & "[ID]=" & "" & key & ""

' Get action
a = Request.Form("a")
If a = "" Or IsNull(a) Then
	a = "I"	' Display with input box
End If

' Open Connection to the database
Set conn = Server.CreateObject("ADODB.Connection")
conn.Open xDb_Conn_Str
Select Case a
	Case "I": ' Display
		strsql = "SELECT * FROM [Admin] WHERE " & sqlKey
    If Session("project4_status_UserLevel") <> -1 Then ' Non system admin
			strsql = strsql & " AND ([ID] = " & Session("project4_status_UserID") & ")"
    End If
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.Open strsql, conn
		If rs.Eof Then
			Response.Clear
			Response.Redirect "Adminlist.asp"
		Else
			rs.MoveFirst
		End If
	Case "D": ' Delete
		strsql = "SELECT * FROM [Admin] WHERE " & sqlKey
    If Session("project4_status_UserLevel") <> -1 Then ' Non system admin
			strsql = strsql & " AND ([ID] = " & Session("project4_status_UserID") & ")"
    End If
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.Open strsql, conn, 1, 2
		Do While Not rs.Eof
			rs.Delete
			rs.MoveNext
		Loop
		rs.Close
		Set rs = Nothing
		conn.Close
		Set conn = Nothing		
		Response.Clear
		Response.Redirect "Adminlist.asp"
End Select
%>
<!--#include file="header.asp"-->
<p><span class="aspmaker">Delete from TABLE: Admin<br><br><a href="Adminlist.asp">Back to List</a></span></p>
<form action="Admindelete.asp" method="post">
<p>
<input type="hidden" name="a" value="D">
<table border="0" cellspacing="1" cellpadding="4" bgcolor="#CCCCCC">
	<tr bgcolor="#0099CC">
		<td><span class="aspmaker" style="color: #FFFFFF;">ID</span>&nbsp;</td>
		<td><span class="aspmaker" style="color: #FFFFFF;">Login Name</span>&nbsp;</td>
		<td><span class="aspmaker" style="color: #FFFFFF;">Passwors</span>&nbsp;</td>
	</tr>
<%
recCount = 0
Do While Not rs.Eof
	recCount = recCount + 1

	' Set row color
	bgcolor = "#FFFFFF"
%>
<%	

	' Display alternate color for rows
	If recCount Mod 2 <> 0 Then
		bgcolor = "#F5F5F5"
	End If
%>
<%
	x_ID = rs("ID")
	x_LoginName = rs("LoginName")
	x_Passwors = rs("Passwors")
%>
	<tr bgcolor="<%= bgcolor %>">
	<input type="hidden" name="key" value="<%= key %>">
		<td class="aspmaker"><% Response.Write x_ID %>&nbsp;</td>
		<td class="aspmaker"><% Response.Write x_LoginName %>&nbsp;</td>
		<td class="aspmaker"><% Response.Write x_Passwors %>&nbsp;</td>
  </tr>
<%
	rs.MoveNext
Loop
rs.Close
Set rs = Nothing
conn.Close
Set conn = Nothing
%>
</table>
<p>
<input type="submit" name="Action" value="CONFIRM DELETE">
</form>
<!--#include file="footer.asp"-->
