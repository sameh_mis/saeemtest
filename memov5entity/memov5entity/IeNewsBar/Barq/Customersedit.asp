<% If Session("project4_status") <> "login" Then Response.Redirect "login.asp" %>
<%
Response.expires = 0
Response.expiresabsolute = Now() - 1
Response.addHeader "pragma", "no-cache"
Response.addHeader "cache-control", "private"
Response.CacheControl = "no-cache"
%>
<!--#include file="db.asp"-->
<!--#include file="aspmkrfn.asp"-->
<%
Response.Buffer = True
key = Request.Querystring("key")
If key = "" Or IsNull(key) Then key = Request.Form("key")
If key = "" Or IsNull(key) Then Response.Redirect "Customerslist.asp"

' Get action
a = Request.Form("a")
If a = "" Or IsNull(a) Then
	a = "I"	' Display with input box
End If

' Get fields from form
x_ID = Request.Form("x_ID")
x_Name = Request.Form("x_Name")
x_Email = Request.Form("x_Email")
x_Country = Request.Form("x_Country")
x_MailingList = Request.Form("x_MailingList")

' Open Connection to the database
Set conn = Server.CreateObject("ADODB.Connection")
conn.Open xDb_Conn_Str
Select Case a
	Case "I": ' Get a record to display
		tkey = "" & key & ""
		strsql = "SELECT * FROM [Customers] WHERE [ID]=" & tkey
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.Open strsql, conn
		If rs.Eof Then
			Response.Clear
			Response.Redirect "Customerslist.asp"
		Else
			rs.MoveFirst
		End If

		' Get the field contents
		x_ID = rs("ID")
		x_Name = rs("Name")
		x_Email = rs("Email")
		x_Country = rs("Country")
		x_MailingList = rs("MailingList")
		rs.Close
		Set rs = Nothing
	Case "U": ' Update

		' Open record
		tkey = "" & key & ""
		strsql = "SELECT * FROM [Customers] WHERE [ID]=" & tkey
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.Open strsql, conn, 1, 2
		If rs.Eof Then
			Response.Clear
			Response.Redirect "Customerslist.asp"
		End If
		tmpFld = Trim(x_Name)
		If Trim(tmpFld) & "x" = "x" Then tmpFld = Null
		rs("Name") = tmpFld
		tmpFld = Trim(x_Email)
		If Trim(tmpFld) & "x" = "x" Then tmpFld = Null
		rs("Email") = tmpFld
		tmpFld = Trim(x_Country)
		If Trim(tmpFld) & "x" = "x" Then tmpFld = Null
		rs("Country") = tmpFld
		tmpFld = x_MailingList
		If tmpFld = "Yes" Then
		    rs("MailingList") = True
		Else
		    rs("MailingList") = False
		End If
		rs.Update
		rs.Close
		Set rs = Nothing
		conn.Close
		Set conn = Nothing
		Response.Clear
		Response.Redirect "Customerslist.asp"
End Select
%>
<!--#include file="header.asp"-->
<p><span class="aspmaker">Edit TABLE: Customers<br><br><a href="Customerslist.asp">Back to List</a></span></p>
<script language="JavaScript" src="ew.js"></script>
<script language="JavaScript">
<!-- start Javascript
function  EW_checkMyForm(EW_this) {
return true;
}
// end JavaScript -->
</script>
<form onSubmit="return EW_checkMyForm(this);"  name="Customersedit" action="Customersedit.asp" method="post">
<p>
<input type="hidden" name="a" value="U">
<input type="hidden" name="key" value="<%= key %>">
<table border="0" cellspacing="1" cellpadding="4" bgcolor="#CCCCCC">
	<tr>
		<td bgcolor="#0099CC"><span class="aspmaker" style="color: #FFFFFF;">ID</span>&nbsp;</td>
		<td bgcolor="#F5F5F5"><span class="aspmaker"><% Response.Write x_ID %><input type="hidden" name="x_ID" value="<%= x_ID %>"></span>&nbsp;</td>
	</tr>
	<tr>
		<td bgcolor="#0099CC"><span class="aspmaker" style="color: #FFFFFF;">�����</span>&nbsp;</td>
		<td bgcolor="#F5F5F5"><span class="aspmaker"><input type="text" name="x_Name" size="30" maxlength="255" value="<%= Server.HTMLEncode(x_Name&"") %>"></span>&nbsp;</td>
	</tr>
	<tr>
		<td bgcolor="#0099CC"><span class="aspmaker" style="color: #FFFFFF;">������ �����������</span>&nbsp;</td>
		<td bgcolor="#F5F5F5"><span class="aspmaker"><input type="text" name="x_Email" size="30" maxlength="255" value="<%= Server.HTMLEncode(x_Email&"") %>"></span>&nbsp;</td>
	</tr>
	<tr>
		<td bgcolor="#0099CC"><span class="aspmaker" style="color: #FFFFFF;">������</span>&nbsp;</td>
		<td bgcolor="#F5F5F5"><span class="aspmaker"><input type="text" name="x_Country" size="30" maxlength="50" value="<%= Server.HTMLEncode(x_Country&"") %>"></span>&nbsp;</td>
	</tr>
	<tr>
		<td bgcolor="#0099CC"><span class="aspmaker" style="color: #FFFFFF;">������� ��������</span>&nbsp;</td>
		<td bgcolor="#F5F5F5"><span class="aspmaker"><input type="radio" name="x_MailingList"<% If x_MailingList = True Then %> checked<% End If %> value="Yes"><%= "Yes" %><input type="radio" name="x_MailingList"<% If x_MailingList = False Then %> checked<% End If %> value="No"><%= "No" %></span>&nbsp;</td>
	</tr>
</table>
<p>
<input type="submit" name="Action" value="EDIT">
</form>
<!--#include file="footer.asp"-->
