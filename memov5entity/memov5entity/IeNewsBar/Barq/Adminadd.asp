<% If Session("project4_status") <> "login" Then Response.Redirect "login.asp" %>
<% If Session("project4_status_UserID") = "" And Session("project4_status_UserLevel") <> -1 Then Response.Redirect "login.asp" %>
<%
Response.expires = 0
Response.expiresabsolute = Now() - 1
Response.addHeader "pragma", "no-cache"
Response.addHeader "cache-control", "private"
Response.CacheControl = "no-cache"
%>
<!--#include file="db.asp"-->
<!--#include file="aspmkrfn.asp"-->
<%
Response.Buffer = True

' Get action
a = Request.Form("a")
If (a = "" Or IsNull(a)) Then
	key = Request.Querystring("key")
	If key <> "" Then
		a = "C" ' Copy record
	Else
		a = "I" ' Display blank record
	End If
End If

' Open Connection to the database
Set conn = Server.CreateObject("ADODB.Connection")
conn.Open xDb_Conn_Str
Select Case a
	Case "C": ' Get a record to display
		tkey = "" & key & ""
		strsql = "SELECT * FROM [Admin] WHERE [ID]=" & tkey
    If Session("project4_status_UserLevel") <> -1 Then ' Non system admin
			strsql = strsql & " AND ([ID] = " & Session("project4_status_UserID") & ")"
    End If
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.Open strsql, conn
		If rs.Eof Then
			Response.Clear
			Response.Redirect "Adminlist.asp"
		Else
			rs.MoveFirst

			' Get the field contents
			x_LoginName = rs("LoginName")
			x_Passwors = rs("Passwors")
		End If
		rs.Close
		Set rs = Nothing
	Case "A": ' Add

		' Get fields from form
x_ID = Request.Form("x_ID")
x_LoginName = Request.Form("x_LoginName")
x_Passwors = Request.Form("x_Passwors")

		' Open record
		strsql = "SELECT * FROM [Admin] WHERE 0 = 1"
		Set rs = Server.CreateObject("ADODB.Recordset")
		rs.Open strsql, conn, 1, 2
		rs.AddNew
		tmpFld = Trim(x_LoginName)
		If Trim(tmpFld) & "x" = "x" Then tmpFld = Null
		rs("LoginName") = tmpFld
		tmpFld = Trim(x_Passwors)
		If Trim(tmpFld) & "x" = "x" Then tmpFld = Null
		rs("Passwors") = tmpFld
    If Session("project4_status_UserLevel") <> -1 Then ' Non system admin
			rs("ID") = Session("project4_status_UserID")
    End If
		rs.Update
		rs.Close
		Set rs = Nothing
		conn.Close
		Set conn = Nothing
		Response.Clear
		Response.Redirect "Adminlist.asp"
End Select
%>
<!--#include file="header.asp"-->
<p><span class="aspmaker">Add to TABLE: Admin<br><br><a href="Adminlist.asp">Back to List</a></span></p>
<script language="JavaScript" src="ew.js"></script>
<script language="JavaScript">
<!-- start Javascript
function  EW_checkMyForm(EW_this) {
return true;
}
// end JavaScript -->
</script>
<form onSubmit="return EW_checkMyForm(this);"  action="Adminadd.asp" method="post">
<p>
<input type="hidden" name="a" value="A">
<table border="0" cellspacing="1" cellpadding="4" bgcolor="#CCCCCC">
	<tr>
		<td bgcolor="#0099CC"><span class="aspmaker" style="color: #FFFFFF;">Login Name</span>&nbsp;</td>
		<td bgcolor="#F5F5F5"><span class="aspmaker"><input type="text" name="x_LoginName" size="30" maxlength="255" value="<%= Server.HTMLEncode(x_LoginName&"") %>"></span>&nbsp;</td>
	</tr>
	<tr>
		<td bgcolor="#0099CC"><span class="aspmaker" style="color: #FFFFFF;">Passwors</span>&nbsp;</td>
		<td bgcolor="#F5F5F5"><span class="aspmaker"><input type="text" name="x_Passwors" size="30" maxlength="50" value="<%= Server.HTMLEncode(x_Passwors&"") %>"></span>&nbsp;</td>
	</tr>
</table>
<p>
<input type="submit" name="Action" value="ADD">
</form>
<!--#include file="footer.asp"-->
