﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using memov5entity.Models;
namespace memov5
{
    public partial class general_marquees : System.Web.UI.Page
    {
        test4Entities dt = new test4Entities();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public string explore_marquee_important_news()
        {
            StringBuilder sb = new StringBuilder();



            var news = from p in dt.marque_article
                       join p2 in dt.Articles
                       on p.article_id equals p2.ID
                       where p.cat_id == 1
                       orderby p.rank
                       select new { p2.Title, p2.ID, p2.url };



            sb.Append("***");

            foreach (var art in news)
            {
                sb.Append(string.Format("<a href='{0}'style='font-size:12px; font-weight:bold; text-decoration:none;color:Black;' target='_blank'>{1}</a>", art.url, art.Title));

                sb.Append(" *** ");
            }

            return sb.ToString();


        }
        public string explore_marquee_important_news_up()
        {
            StringBuilder sb = new StringBuilder();



            var news = from p in dt.marque_article
                       join p2 in dt.Articles
                       on p.article_id equals p2.ID
                       where p.cat_id == 1
                       orderby p.rank
                       select new { p2.Title, p2.ID, p2.url };



           // sb.Append("***");

            foreach (var art in news)
            {
                sb.Append(string.Format("<a href='{0}' style='color:black;font-size:13px; font-weight:bold;text-decoration:none;' target='_blank'>{1}</a><br/><br/>", art.url, art.Title));

               // sb.Append(" *** ");
            }

            return sb.ToString();


        }
    }
}
