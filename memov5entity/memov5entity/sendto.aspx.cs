﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using memov5entity.Models;

using System.Web.Mail;
using System.Data.SqlClient;

namespace memov5entity
{
    public partial class sendto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
           
            int newsid;
           int.TryParse(Request["id"], out newsid);
            ArrayList prams = new ArrayList();
            prams.Add("@id");
             ArrayList value = new ArrayList();
            value.Add(newsid);

            object resule = connection.scaler_proc("get_state", prams, value);
            int state = Convert.ToInt32(resule);
            if (state != 8)
            {
                Response.Redirect("/");
            }
           // string newsid = Request["id"].ToString();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["test4ConnectionString"].ConnectionString);
            conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "geturl";
            cmd.Connection = conn;
            SqlParameter pram = new SqlParameter("@id", newsid);
            cmd.Parameters.Add(pram);
            string url = cmd.ExecuteScalar().ToString();
            conn.Close();
            string artlink = File.ReadAllText(Server.MapPath("sendto.tpl"));

            artlink = artlink.Replace("%name_to%", txt_name_to.Text);
            artlink = artlink.Replace("%name_from%", txt_name_from.Text);
           // artlink = artlink.Replace("%article_link%", string.Format("http://www.islammemo.cc/article1.aspx?id={0}", newsid, 1));
             artlink = artlink.Replace("%article_link%", string.Format("http://www.islammemo.cc"+url));


            MailMessage objEmail = new MailMessage();
            objEmail.To = txt_mail_to.Text;
            objEmail.From = "sendto@islammemo.cc";
            //objEmail.Cc       = txtCc.Text;
            objEmail.Subject = "رساله من " + txt_name_from.Text;
            objEmail.Body = artlink;
            objEmail.Priority = MailPriority.High;
            objEmail.BodyFormat = MailFormat.Html;
            SmtpMail.SmtpServer = "smtp.islammemo.cc";
            objEmail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
            objEmail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "sendto"); //set your username here
            objEmail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", ConfigurationManager.AppSettings["password_email"]);	//set your password here
           
            try
            {
                SmtpMail.Send(objEmail);
                test4Entities dt = new test4Entities();
                dt.ExecuteStoreCommand("update dbo.Articles  set sendtofriendno=sendtofriendno+1 where ID={0}", newsid);
                Response.Write("<script>window.close();</script>");

            }
            catch (Exception exc)
            {

                Response.Write("Send failure: ");
            }


        }
          
            
       

    }
}