﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Text;

namespace memov5entity
{
    public partial class bar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            StringBuilder sb = new StringBuilder();
            bool mofkera = false;
            string direction = "";
            string speed = "";
            int myid;
            int catid = 0;
            int.TryParse(Request["id"], out myid);
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["test4ConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("select id, uname, email, site, mofkra, direction, bold, italic, underline, fontfamily, fontsize, scrollamount, scrolldelay, color, backcolor,catid from  bar where id=@id", conn);
            cmd.Parameters.AddWithValue("@id", myid);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                //try
                //{

                //    if (!validurl(Request.UrlReferrer.ToString().Trim(), dr["site"].ToString()))
                //    {
                //        //return;
                //    }
                //}
                //catch
                //{
                //    //return;
                //}
                int.TryParse(dr["catid"].ToString(), out catid);
                mofkera = dr.GetBoolean(4);
                direction = dr["direction"].ToString();
                speed = dr["scrollamount"].ToString();
                sb.Append("<style>");
                sb.Append(".aaa{direction:rtl;color:" + dr["color"] + ";background-color:" + dr["backcolor"] + "; font-size:" + dr["fontsize"] + "px;font-family:" + dr["fontfamily"] + ";}");
                sb.Append(".aaa a:link,.aaa a:visited{text-decoration:");
                if (!dr.IsDBNull(8) && dr.GetBoolean(8))
                {
                    sb.Append("underline;");
                }
                else
                {
                    sb.Append("none;");
                }
                sb.Append("color:" + dr["color"] + ";}");
                sb.Append("");
                sb.Append("");
                sb.Append("</style>");



            }
            else
            {
                return;
            }

            dr.Close();
            conn.Close();
            if (mofkera)
            {
                sb.Append("");
            }

            sb.Append(string.Format("<marquee scrollDelay='40' scrollAmount='{0}' direction='{1}' onmouseover='this.stop();' onmouseout='this.start();' class='aaa'>", speed, direction));

            switch (catid)
            {
                case 1:
                    cmd = new SqlCommand("get_rss", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@catid", 1);
                    cmd.Parameters.AddWithValue("@top", 14);
                    break;
                case 2:
                    cmd = new SqlCommand("get_rss", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@catid", 2);
                    cmd.Parameters.AddWithValue("@top", 14);
                    break;
                case 3:
                    cmd = new SqlCommand("get_rss", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@catid", 3);
                    cmd.Parameters.AddWithValue("@top", 14);
                    break;
                case 4:
                    cmd = new SqlCommand("get_rss", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@catid", 0);
                    cmd.Parameters.AddWithValue("@top", 14);
                    break;
                default:
                    cmd = new SqlCommand("SELECT top 15 Articles.ID, Articles.Title FROM article_cats INNER JOIN Articles ON article_cats.ArtID = Articles.ID WHERE (Articles.stat = 8) AND (article_cats.CatID = @catid) and [date]<getdate() order by date desc", conn);
                    cmd.Parameters.AddWithValue("@catid", ConfigurationManager.AppSettings["general_news_cat_id"]);
                    break;
            }

            conn.Open();
            dr = cmd.ExecuteReader();
            bool vertical = false;
            if (direction.ToLower() == "up" || direction.ToLower() == "down")
            {
                vertical = true;
            }
            sb.Append("<center>");
            while (dr.Read())
            {

                if (!mofkera)
                {
                    if (vertical)
                    {
                        // sb.Append(string.Format("<a target='blank' href='http://www.islammemo.cc/article1.aspx?id={0}{2}'>{1}</a>", dr["id"], tr.artoen(dr["title"].ToString().Trim(), Resources.memo.lang4), lang2));
                        //  sb.Append("<br/>*" + Resources.memo.ismemo + "*<br/>");
                        //   sb.Append(string.Format("<a target='blank' href='http://www.islammemo.cc/article1.aspx?id={0}'>{1}</a>", dr["id"], dr["title"].ToString()));
                        sb.Append(string.Format("<a target='blank' href='http://www.saeem.com/article1.aspx?id={0}'>{1}</a>", dr["id"], dr["title"].ToString()));
                        sb.Append("<br/>*" + "مفكرة الاسلام" + "*<br/>");
                    }
                    else
                    {
                        //  sb.Append(string.Format("<a target='blank' href='http://www.islammemo.cc/article1.aspx?id={0}{2}'>{1}</a>&nbsp;*" + Resources.memo.ismemo + "*&nbsp;", dr["id"], tr.artoen(dr["title"].ToString().Trim(), Resources.memo.lang4), lang2));

                        //sb.Append(string.Format("<a target='blank' href='http://www.islammemo.cc/article1.aspx?id={0}{2}'>{1}</a>&nbsp;*مفكرة الإسلام*&nbsp;", dr["id"], dr["title"].ToString().Trim()));
                        // eman  sb.Append(string.Format("<a target='blank' href='http://www.islammemo.cc/article1.aspx?id={0}'>{1}</a>&nbsp;*" + "مفكرة الإسلام" + "*&nbsp;", dr["id"], dr["title"].ToString()));
                        sb.Append(string.Format("<a target='blank' href='http://www.saeem.com/article1.aspx?id={0}'>{1}</a>&nbsp;*" + "مفكرة الإسلام" + "*&nbsp;", dr["id"], dr["title"].ToString()));
                    }
                }
                else
                {
                    // sb.Append(string.Format("<a target='blank' href='http://www.islammemo.cc/article1.aspx?id={0}{2}'>{1}</a>&nbsp;**&nbsp;", dr["id"], tr.artoen(dr["title"].ToString().Trim(), Resources.memo.lang4), lang2));
                    // eman   sb.Append(string.Format("<a target='blank' href='http://www.islammemo.cc/article1.aspx?id={0}{2}'>{1}</a>&nbsp;**&nbsp;", dr["id"], dr["title"].ToString()));
                    sb.Append(string.Format("<a target='blank' href='http://www.saeem.com/article1.aspx?id={0}{2}'>{1}</a>&nbsp;**&nbsp;", dr["id"], dr["title"].ToString()));
                }

            }
            dr.Close();
            conn.Close();
            sb.Append("</center>");

            sb.Append("</marquee>");
            if (mofkera)
            {
                sb.Append("");
            }

            Literal1.Text = sb.ToString();


        } //page load end

        bool validurl(string requsturl, string savedurl)
        {

            string regexPattern = @"^(?<s1>(?<s0>[^:/\?#]+):)?(?<a1>"
               + @"//(?<a0>[^/\?#]*))?(?<p0>[^\?#]*)"
               + @"(?<q1>\?(?<q0>[^#]*))?"
               + @"(?<f1>#(?<f0>.*))?";

            Regex re = new Regex(regexPattern, RegexOptions.ExplicitCapture);
            Match m = re.Match(requsturl);
            Match m2 = re.Match(savedurl);

            requsturl = m.Groups["a0"].Value;
            savedurl = m2.Groups["a0"].Value;

            if (!savedurl.StartsWith("www."))
            {
                savedurl = "www." + savedurl;
            }
            if (!requsturl.StartsWith("www."))
            {
                requsturl = "www." + requsturl;
            }

            if (savedurl == requsturl)
            {
                return true;
            }
            else
            {
                return false;
            }


        }



    }
}