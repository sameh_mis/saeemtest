﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace memov5entity.Models
{
   //// public class temp_art
   // {
   //     public string title, apprev, url, img;
   //   public  DateTime? date;
   //   public int? readers;
   //     public temp_art()
   //     {

   //     }
   //     public temp_art(string title, string apprev, string url, string img)
   //     {
   //         this.title = title;
   //         this.apprev = apprev;
   //         this.url = url;
   //         this.img = img;
   //     }
   // }
    public class partialsviewmodel
    {
        test4Entities dt = new test4Entities();
        public Article[] get_cat_articles(int catid, int arts_no)
        {
            return         (from p in dt.Articles
                              join p2 in dt.article_cats
                              on p.ID equals p2.ArtID
                              where p.stat == 8 && p2.CatID == catid && p.Date < DateTime.Now
                              orderby p.Date descending
                              select p).Take(arts_no).ToArray();
        }

        public List<temp_art> topreadingarticle(int no_of_articles)
        {
            try
            {

                 List<temp_art> topread =(from p in dt.Articles
                        where p.Date.Value.Month == DateTime.Now.Month && p.Date.Value.Day == DateTime.Now.Day && p.Date.Value.Year == DateTime.Now.Year && p.stat == 8
                        orderby p.Readers descending
                                    select new temp_art() { url=p.url, title = p.Title }).Take(no_of_articles).ToList();
                 return topread;
            }
            catch
            {
                return null;
            }
            
        }
        public DataTable topcommentarticles(int artsno)
        {

          
            ArrayList arr1 = new ArrayList();
            ArrayList arr2 = new ArrayList();
            arr1.Add("@arts_no");
            arr2.Add(artsno);
            DataTable dtcomments = connection.reader_proc("get_top_comments", arr1, arr2);
            //string[] arrli = new string[dtcomments.Rows.Count];
            //for (int a = 0; a < dtcomments.Rows.Count; a++)
            //{
            //    arrli[a] = "<li><a href='" + dtcomments.Rows[a]["url"] + "'>" + formaltitle(dtcomments.Rows[a]["title"].ToString(), 35) + "</a>";

            //}
            return dtcomments;
        }
        string formaltitle(string title, int length)
        {
            return title.Length >= length ? title.Remove(length - 1) + "..." : title;
        }
        public List<temp_art> topsentarticle(int no_of_articles)
        {

            try
            {
                var baselineDate = DateTime.Now.AddHours(-2);
                List<temp_art> sent = (from p in dt.Articles
                                       where p.Date.Value > baselineDate && p.sendtofriendno > 0
                                       orderby p.sendtofriendno descending
                                       select new temp_art() { url = p.url, title = p.Title }).Take(no_of_articles).ToList();
                return sent;
            }
            catch
            {
                return null;
            }
        }

        public  List<temp_art> get_important_news()
        {
           
            //add the ads before the news
            List<temp_art> news =( from p in dt.marque_textads
                                  where p.active == true
                                  select new temp_art() { title =p.ad , url = p.ad_link}).ToList();


            news.AddRange( (from p in dt.marque_article
                       join p2 in dt.Articles
                       on p.article_id equals p2.ID
                       where p.cat_id == 1
                       orderby p.rank
                            select new temp_art() { title = p2.Title, url = p2.url}).ToList());



         

            return news;


        }
      
        public DateTime get_last_article_date()
        {
            return  Convert.ToDateTime((from p in dt.Articles
                    where p.stat == 8 && p.Date <= DateTime.Now
                    orderby p.Date descending
                    select p.Date).FirstOrDefault());
        }
        public List<temp_art> get_syria_akhbar(int arts_no)
        {
           return (from p in dt.Articles
                                   join p2 in dt.article_cats
                                   on p.ID equals p2.ArtID
                                   where p2.CatID == 924 && p.stat == 8
                                   orderby p.Date descending
                                   select new temp_art() { title = p.Title, apprev = p.Apprev, url = p.url, img = p.image_url }).Take(arts_no).ToList();
        }
        public List<temp_art> get_syria_tkarer(int arts_no)
        {
            List<temp_art> arts = (from p in dt.Articles
                                   join p2 in dt.article_cats
                                   on p.ID equals p2.ArtID
                                   orderby p.Date descending
                                   where p.cat_id == 924 && p.stat == 8
                                   select new temp_art() { title = p.Title, apprev = p.Apprev, url = p.url, img = p.image_url }).Except((from p in dt.Articles
                                                                                                                                         join p2 in dt.article_cats
                                                                                                                                         on p.ID equals p2.ArtID
                                                                                                                                         where p.cat_id == 924 && p2.CatID == 621 && p.stat == 8
                                                                                                                                         select new temp_art() { title = p.Title, apprev = p.Apprev, url = p.url, img = p.image_url })).Take(arts_no).ToList();
            /*
//          select title,Apprev,url,image_url,date from articles
//join article_cats 
//on articles.ID=article_cats.ArtID
//where articles.cat_id=924  and stat=8

//EXCEPT(
//select title,Apprev,url,image_url,date from articles
//join article_cats 
//on articles.ID=article_cats.ArtID
//where articles.cat_id=924 and article_cats.CatID=621 and stat=8
//)
//order by   articles.date desc  
//        */
            
           return arts;
           // return null;
        }
        public List<temp_art> get_important_tkarer(int arts_no)
        {
            List<temp_art> arts = (from p in dt.Articles
                                   join p2 in dt.article_cats
                                   on p.ID equals p2.ArtID
                                   orderby p.Date descending
                                   where p.cat_id == 924 && p.stat == 8 && p.choice == true
                                   select new temp_art() { title = p.Title, apprev = p.Apprev, url = p.url, img = p.image_url, date = p.Date, readers = p.Readers }).Except((from p in dt.Articles
                                                                                                                                                                             join p2 in dt.article_cats
                                                                                                                                                                             on p.ID equals p2.ArtID
                                                                                                                                                                             where p.cat_id == 924 && p2.CatID == 621 && p.stat == 8
                                                                                                                                                                             select new temp_art() { title = p.Title, apprev = p.Apprev, url = p.url, img = p.image_url, date = p.Date, readers = p.Readers })).Take(arts_no).ToList();


            return arts;
        }


        public string[] new6 { get; set; }
    }

    
}