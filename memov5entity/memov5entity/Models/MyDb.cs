﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using myobjects;
using memo.enums;
using System.Collections;
using System.Text;
using memov5entity.Models;

namespace memov5entity.Models
{
    public class MyDb
    {
        public static DataSet get_doors(HttpContext context)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[1].ConnectionString);
            SqlDataAdapter da = new SqlDataAdapter("select * from categories", conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        public static void get_doors(SqlConnection conn, out List<ListItem> mydoors)
        {
            mydoors = new List<ListItem>();
            SqlCommand cmd = new SqlCommand("SELECT id, name FROM categories ", conn);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                mydoors.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
            } dr.Close();
        }
        public static void get_doors(SqlConnection conn, out List<ListItem> mydoors, HttpContext context)
        {
            System.Collections.Generic.List<ListItem> myitems;
            myitems = (System.Collections.Generic.List<ListItem>)context.Cache.Get("mydb_doors_list");
            if (myitems == null)
            {
                myitems = new System.Collections.Generic.List<ListItem>();
                SqlCommand cmd = new SqlCommand("SELECT id, name FROM categories", conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    myitems.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
                } dr.Close();
                //test
                myitems.Add(new ListItem(DateTime.Now.ToShortTimeString(), "ddd"));
                context.Cache.Insert("mydb_doors_list", myitems, new System.Web.Caching.CacheDependency(@"c:\cache\categories.txt"));
            }

            mydoors = myitems;
        }
        public static void get_doors(SqlConnection conn, out List<CatListItem> mydoors, HttpContext context)//4
        {
            System.Collections.Generic.List<CatListItem> myitems;
            myitems = (System.Collections.Generic.List<CatListItem>)context.Cache.Get("mydb_doors_list4");
            if (myitems == null)
            {
                myitems = new System.Collections.Generic.List<CatListItem>();
                SqlCommand cmd = new SqlCommand("SELECT id, name,parentid FROM categories", conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    myitems.Add(new CatListItem(dr[0].ToString(), dr[1].ToString().Trim(), dr[2].ToString()));
                } dr.Close();
                //test
                myitems.Add(new CatListItem(DateTime.Now.ToLongTimeString(), "ddd"));
                context.Cache.Insert("mydb_doors_list4", myitems, new System.Web.Caching.CacheDependency(@"c:\cache\categories.txt"));
            }

            mydoors = myitems;
        }

        public static DataSet get_countries()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[1].ConnectionString);
            SqlDataAdapter da = new SqlDataAdapter("SELECT apprv, name FROM country order by apprv", conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        public static System.Collections.Generic.List<ListItem> get_countries(SqlConnection conn, HttpContext context)
        {
            System.Collections.Generic.List<ListItem> myitems;


            myitems = (System.Collections.Generic.List<ListItem>)context.Cache.Get("mydb_countries_list");

            if (myitems == null)
            {
                myitems = new System.Collections.Generic.List<ListItem>();
                SqlCommand cmd = new SqlCommand("SELECT apprv, name FROM country order by name ", conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    myitems.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
                } dr.Close();

                context.Cache.Insert("mydb_countries_list", myitems);
            }


            return myitems;
        }

        public static System.Collections.Generic.List<myobjects.CatListItem> get_categories(SqlConnection conn, HttpContext context)//1
        {

            List<CatListItem> mycats = (List<CatListItem>)context.Cache.Get("mydb_categories_1");

            if (mycats == null)
            {
                mycats = new List<CatListItem>();
                SqlCommand cmd = new SqlCommand("select id,name,parentid from categories", conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    mycats.Add(new myobjects.CatListItem(dr[0].ToString(), dr[1].ToString(), dr[2].ToString()));
                } dr.Close();



                context.Cache.Insert("mydb_categories_1", mycats, new System.Web.Caching.CacheDependency(@"c:\cache\categories.txt"));

            }



            return mycats;
        }

        public static System.Collections.Generic.List<ListItem> get_marque_types(SqlConnection conn)
        {
            System.Collections.Generic.List<ListItem> myitems = new System.Collections.Generic.List<ListItem>();
            SqlCommand cmd = new SqlCommand("SELECT id, name FROM marques", conn);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                myitems.Add(new ListItem(dr[1].ToString(), dr[0].ToString()));
            } dr.Close();

            return myitems;

        }

        internal static string get_layout_id(int RealCatId, HttpContext context)
        {
            int myval = 0;

            List<nmevalu> mylayouts = (List<nmevalu>)context.Cache.Get("mydb_layouts");

            if (mylayouts == null)
            {
                mylayouts = new List<nmevalu>();
                //retrive from database
                int tempname;
                int tempvalue;
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["newislammemoConnectionString"].ConnectionString);
                SqlCommand cmd = new SqlCommand("SELECT Categories.ID AS cid,layouts.id AS lid FROM Categories INNER JOIN layouts ON Categories.layout = layouts.id", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int.TryParse(dr[0].ToString(), out tempname);
                    int.TryParse(dr[1].ToString(), out tempvalue);
                    mylayouts.Add(new nmevalu(tempname, tempvalue));
                }
                dr.Close();
                conn.Close();
            }


            foreach (nmevalu itm in mylayouts)
            {
                if (itm.Name == RealCatId)
                {
                    myval = itm.Value;
                    break;
                }
            }

            return myval.ToString();
        }


        public class nmevalu
        {
            int name;
            int value;

            public nmevalu(int nme, int vlu)
            {
                name = nme;
                value = vlu;
            }

            public int Name
            {
                get
                {
                    return name;
                }
                set
                {
                    name = value;
                }
            }
            public int Value
            {
                get
                {
                    return this.value;
                }
                set
                {
                    this.value = value;
                }
            }
        }

        public static void filldoors(DropDownList ddl1, SqlConnection conn, HttpContext mycontext)
        {
            System.Collections.Generic.List<myobjects.CatListItem> dd = new List<CatListItem>();

            MyDb.get_doors(conn, out dd, mycontext);
            //ddl1.Items.Clear();
            foreach (myobjects.CatListItem itm in dd)
            {
                ddl1.Items.Add(new ListItem(itm.Name, itm.Id));
            }
        }

        public static string getmarqu(marquAds ads, HttpContext mycontext)
        {
            return "ddddd";
        }



        public static string get_rss(int catid, Page pg)
        {
            StringBuilder sb = new StringBuilder();
            SqlDataReader dr;
            string caturl = "";
            string arturl = "";

            sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\" ?> ");
            //sb.Append("<?xml-stylesheet title=\"XSL_formatting\" type=\"text/xsl\" href=\"rss.xsl\"?>");
            sb.Append("<rss xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" version=\"2.0\">");
            sb.Append("<channel>");

            sb.Append("<image>");
            sb.Append("<title>" + "مفكرة الاسلام" + "</title>");
            sb.Append("<url>images/logo_print.jpg</url>");
            sb.Append("<link>");
            sb.Append((new Uri(new Uri(pg.Request.Url.AbsoluteUri), string.Format("/"))).AbsoluteUri +"");
            sb.Append("</link>");
            sb.Append("</image>");

            switch (catid)
            {
                case 1:
                    sb.Append("<title>");
                    Uri ur = new Uri(new Uri(pg.Request.Url.AbsoluteUri), string.Format("/akhbar-monawaa") + "");
                    sb.Append("أخبار منوعة" + ":" + "مفكرة الاسلام");
                    sb.Append("</title>");
                    sb.Append("<link>");
                    sb.Append(ur.AbsoluteUri);
                    sb.Append("</link>");
                    sb.Append("<description>");
                    sb.Append("");
                    sb.Append("</description>");
                    break;
                case 2:
                    sb.Append("<title>");
                    sb.Append("تقارير" + ":" + "مفكرة الاسلام");
                    sb.Append("</title>");
                    sb.Append("<link>");
                    sb.Append((new Uri(new Uri(pg.Request.Url.AbsoluteUri), string.Format("/Tkarer/Tkareer"))).AbsoluteUri +"");
                    sb.Append("</link>");
                    sb.Append("<description>");
                    sb.Append("");
                    sb.Append("</description>");
                    break;
                case 3:
                    sb.Append("<title>");
                    sb.Append("الأسرة" + ":" + "مفكرة الاسلام");
                    sb.Append("</title>");
                    sb.Append("<link>");
                    sb.Append((new Uri(new Uri(pg.Request.Url.AbsoluteUri), string.Format("/mostashar/el-beet-said"))).AbsoluteUri +"");

                    sb.Append("</link>");
                    sb.Append("<description>");
                    sb.Append("");
                    sb.Append("</description>");
                    break;
                default:
                    sb.Append("<title>");
                    sb.Append("اخبار مفكرة الاسلام");
                    sb.Append("</title>");
                    sb.Append("<link>");
                    sb.Append((new Uri(new Uri(pg.Request.Url.AbsoluteUri), string.Format("/"))).AbsoluteUri +"");

                    sb.Append("</link>");
                    sb.Append("<description>");
                    sb.Append("");
                    sb.Append("</description>");
                    break;
            }

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["test4ConnectionString"].ConnectionString);
            SqlCommand cmd;
           
           cmd = new SqlCommand("get_rss", conn);
                    
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@catid", catid);
            cmd.Parameters.AddWithValue("@top", 20);
            conn.Open();
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                sb.Append("<pubDate>");
                sb.Append(dr[4].ToString());
                sb.Append("</pubDate>");
                sb.Append("<item>");
                sb.Append("<title>");
                sb.Append(dr[1].ToString());
                sb.Append("</title>");
                string url = images_server.updateimg( dr[5].ToString(), "");
                sb.Append("<enclosure url='" + url + "' type='image/jpeg' /> ");
                sb.Append("<description>");
                sb.Append(dr[2].ToString());
                sb.Append("</description>");

                sb.Append("<link>");
                if (string.IsNullOrEmpty(arturl))
                {
                    Uri ur = new Uri(new Uri(pg.Request.Url.AbsoluteUri), dr["url"].ToString());

                    sb.Append(ur.AbsoluteUri + "");//check condition of page and 
                }
                else
                {
                    Uri ur = new Uri(new Uri(pg.Request.Url.AbsoluteUri), string.Format("/{0}?id={1}", arturl, dr[0].ToString()));
                    sb.Append(ur.AbsoluteUri +"");//check condition of page and 
                }
                sb.Append("</link>");

                sb.Append("</item>");
            } dr.Close();

            conn.Close();



            sb.Append("</channel>");
            sb.Append("</rss>");

            //put in the chash

            return sb.ToString();

        }

        public static string get_rss1(int catid, Page pg)
        {
            StringBuilder sb = new StringBuilder();
            SqlDataReader dr;
            string caturl = "";
            string arturl = "";

            sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\" ?> ");
            //sb.Append("<?xml-stylesheet title=\"XSL_formatting\" type=\"text/xsl\" href=\"rss.xsl\"?>");
            sb.Append("<rss xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" version=\"2.0\">");
            sb.Append("<channel>");

            sb.Append("<image>");
            sb.Append("<title>" + "مفكرة الاسلام" + "</title>");
            sb.Append("<url>images/logo_print.jpg</url>");
            sb.Append("<link>");
            sb.Append((new Uri(new Uri(pg.Request.Url.AbsoluteUri), string.Format("/"))).AbsoluteUri + "");
            sb.Append("</link>");
            sb.Append("</image>");

            switch (catid)
            {
                case 1:
                    sb.Append("<title>");
                    Uri ur = new Uri(new Uri(pg.Request.Url.AbsoluteUri), string.Format("/akhbar-monawaa") + "");
                    sb.Append("أخبار منوعة" + ":" + "مفكرة الاسلام");
                    sb.Append("</title>");
                    sb.Append("<link>");
                    sb.Append(ur.AbsoluteUri);
                    sb.Append("</link>");
                    sb.Append("<description>");
                    sb.Append("");
                    sb.Append("</description>");
                   sb.Append("<pubDate>"+DateTime.Now+"</pubDate><generator>http://www.islammemo.cc</generator><language>ar</language>");
                   break;
                case 2:
                    sb.Append("<title>");
                    sb.Append("تقارير" + ":" + "مفكرة الاسلام");
                    sb.Append("</title>");
                    sb.Append("<link>");
                    sb.Append((new Uri(new Uri(pg.Request.Url.AbsoluteUri), string.Format("/Tkarer/Tkareer"))).AbsoluteUri +"");
                    sb.Append("</link>");
                    sb.Append("<description>");
                    sb.Append("");
                    sb.Append("</description>");
                    sb.Append("<pubDate>" + DateTime.Now + "</pubDate><generator>http://www.islammemo.cc</generator><language>ar</language>");
                    break;
                case 3:
                    sb.Append("<title>");
                    sb.Append("الأسرة" + ":" + "مفكرة الاسلام");
                    sb.Append("</title>");
                    sb.Append("<link>");
                    sb.Append((new Uri(new Uri(pg.Request.Url.AbsoluteUri), string.Format("/mostashar/el-beet-said"))).AbsoluteUri + "");

                    sb.Append("</link>");
                    sb.Append("<description>");
                    sb.Append("");
                    sb.Append("</description>");
                    sb.Append("<pubDate>" + DateTime.Now + "</pubDate><generator>http://www.islammemo.cc</generator><language>ar</language>");
                    break;
                default:
                    sb.Append("<title>");
                    sb.Append("اخبار مفكرة الاسلام");
                    sb.Append("</title>");
                    sb.Append("<link>");
                    sb.Append((new Uri(new Uri(pg.Request.Url.AbsoluteUri), string.Format("/"))).AbsoluteUri +"");

                    sb.Append("</link>");
                    sb.Append("<description>");
                    sb.Append("");
                    sb.Append("</description>");
                    sb.Append("<pubDate>" + DateTime.Now + "</pubDate><generator>http://www.islammemo.cc</generator><language>ar</language>");
                    break;
            }

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["test4ConnectionString"].ConnectionString);
            SqlCommand cmd;


          
         
            cmd = new SqlCommand("get_rss1", conn);
        
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@catid", catid);
            cmd.Parameters.AddWithValue("@top", 20);
            conn.Open();
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                
                sb.Append("<item>");
                sb.Append("<title>");
                sb.Append(dr[1].ToString());
                sb.Append("</title>");
               // sb.Append("<image>");
               // sb.Append("<title>");
               // sb.Append(tr.artoen(dr[1].ToString(), Resources.memo.lang4));
               // sb.Append("</title>");
              //  sb.Append("<url>");
              //  sb.Append(images_server.name + dr[5].ToString());
              //  sb.Append("</url>");
               // sb.Append("<link>");
               // sb.Append("http://islammemo.cc");
               // sb.Append("</link>");
               // sb.Append("</image>");
                sb.Append("<link>");
                string target_url = "http://mobile.islammemo.cc/Articlen.aspx?id=" + dr[0].ToString() + "&catid=" + dr[6].ToString() + "&lid=0";
                if (string.IsNullOrEmpty(arturl))
                {
                    Uri ur = new Uri("http://mobile.islammemo.cc/Articlen.aspx?id=" + dr[0].ToString() + "&amp;catid=" + dr[6].ToString() + "&amp;lid=0");

                    sb.Append(ur.AbsoluteUri +"");//check condition of page and 
                }
                else
                {
                    Uri ur = new Uri(new Uri(pg.Request.Url.AbsoluteUri), string.Format("/{0}?id={1}", arturl, dr[0].ToString()));
                    sb.Append(ur.AbsoluteUri +"");//check condition of page and 
                }
                sb.Append("</link>");
                sb.Append("<pubDate>");
                sb.Append(dr[4].ToString());
                sb.Append("</pubDate>");
               sb.Append("<description>");
               sb.Append("<![CDATA[<a href='" + target_url + "'><img src='" + images_server.name + dr[5].ToString() + "' width='60px' height='60px'/></a>");
                sb.Append(" "+dr[2].ToString());
                sb.Append("]]>");
                sb.Append("</description>");

               

                sb.Append("</item>");
            } dr.Close();

            conn.Close();



            sb.Append("</channel>");
            sb.Append("</rss>");

            //put in the chash

            return sb.ToString();

        }

        public static bool cat_is_door(int mycat, HttpContext contex)
        {
            List<CatListItem> mydoors;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["newislammemoConnectionString"].ConnectionString);
            conn.Open();
            get_doors(conn, out mydoors, contex);
            conn.Close();

            foreach (CatListItem var in mydoors)
            {
                if (var.Id == mycat.ToString())
                {
                    return true;
                }
            }
            return false;
        }

        public static List<myobjects.CatListItem> get_root_cats(HttpContext context, List<string> myroles)
        {
            List<CatListItem> mycats = (List<CatListItem>)context.Cache.Get("mydb_root_cats");

            if (mycats == null)
            {
                mycats = new List<CatListItem>();
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["newislammemoConnectionString"].ConnectionString);
                SqlCommand cmd = new SqlCommand("select id,name,parentid from categories where parentid=0", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    mycats.Add(new myobjects.CatListItem(dr[0].ToString(), dr[1].ToString().Trim(), dr[2].ToString()));
                } dr.Close();
                conn.Close();


                context.Cache.Insert("mydb_root_cats", mycats, new System.Web.Caching.CacheDependency(@"c:\cache\categories.txt"));
            }

            return mycats;
        }

        public static List<CatListItem> get_user_categories(HttpContext Context, List<string> myroles, string myuser)
        {
            List<CatListItem> mycats = new List<CatListItem>();
            SqlConnection conn;
            SqlCommand cmd;
            SqlDataReader dr;
            if (myroles.Contains("programmers") || myroles.Contains("admin"))
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["newislammemoConnectionString"].ConnectionString);
                cmd = new SqlCommand("SELECT Categories.ID, Categories.Name,parentid,door FROM   Categories ", conn);
                conn.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (!dr.IsDBNull(3))
                    {
                        mycats.Add(new myobjects.CatListItem(dr[0].ToString(), dr[1].ToString().Trim(), dr[2].ToString(), dr.GetBoolean(3)));
                    }
                    else
                    {
                        mycats.Add(new myobjects.CatListItem(dr[0].ToString(), dr[1].ToString().Trim(), dr[2].ToString(), false));
                    }
                }
                dr.Close();
                conn.Close();

                return mycats;
            }


            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["newislammemoConnectionString"].ConnectionString);
            cmd = new SqlCommand("SELECT Categories.ID, Categories.Name,parentid ,door FROM users_cats INNER JOIN Categories ON users_cats.cat_id = Categories.ID WHERE (Categories.ParentID = 0) AND (users_cats.username = @user)", conn);
            cmd.Parameters.AddWithValue("@user", myuser);
            conn.Open();
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                if (!dr.IsDBNull(3))
                {
                    mycats.Add(new myobjects.CatListItem(dr[0].ToString(), dr[1].ToString().Trim(), dr[2].ToString(), dr.GetBoolean(3)));
                }
                else
                {
                    mycats.Add(new myobjects.CatListItem(dr[0].ToString(), dr[1].ToString().Trim(), dr[2].ToString(), false));
                }
            }
            dr.Close();
            cmd = new SqlCommand("SELECT Categories.ID, Categories.Name,parentid ,door FROM   Categories  WHERE ParentID <> 0", conn);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                if (!dr.IsDBNull(3))
                {
                    mycats.Add(new myobjects.CatListItem(dr[0].ToString(), dr[1].ToString().Trim(), dr[2].ToString(), dr.GetBoolean(3)));
                }
                else
                {
                    mycats.Add(new myobjects.CatListItem(dr[0].ToString(), dr[1].ToString().Trim(), dr[2].ToString(), false));
                }
            }

            conn.Close();

            return mycats;
        }

        public static string get_cat_full_path(string cat_id)
        {
            //todo: cachhhhhhhhhhhhhh
            string tempstring = "";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["newislammemoConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("select url_full_path from categories where id=@id", conn);
            cmd.Parameters.AddWithValue("@id", cat_id);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                tempstring = dr[0].ToString();
            }
            else
            {
                // tempstring = "$$$error" + cat_id;
            }
            dr.Close();
            conn.Close();

            return tempstring;
        }

        public static string get_cat_id_from_name(string catname)
        {
            //string aaa = "0";
            //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[1].ConnectionString);
            //SqlCommand cmd = new SqlCommand("select id ,url_name from categories where url_name=@aa", conn);
            //cmd.Parameters.AddWithValue("@aa",catname);
            //conn.Open();
            //SqlDataReader dr = cmd.ExecuteReader();
            //if (dr.Read())
            //{
            //    aaa = dr[0].ToString();
            //}
            //dr.Close();
            //conn.Close();

            //return aaa;
            Hashtable myHash = (Hashtable)HttpContext.Current.Cache.Get("cats_names");

            if (myHash == null)
            {
                myHash = new Hashtable();

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["newislammemoConnectionString"].ConnectionString);
                SqlCommand cmd = new SqlCommand("select id ,url_name from categories where door=1", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    myHash.Add(dr["url_name"].ToString().ToLower(), dr["id"].ToString());
                }
                dr.Close();
                conn.Close();

                HttpContext.Current.Cache.Insert("cats_names", myHash, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromHours(2), System.Web.Caching.CacheItemPriority.High, null);

            }
            if (myHash.Contains(catname.ToLower()))
            {
                return myHash[catname.ToLower()].ToString();
            }
            else
            {
                myHash = new Hashtable();

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["newislammemoConnectionString"].ConnectionString);
                SqlCommand cmd = new SqlCommand("select id ,url_name from categories where door=1", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    myHash.Add(dr["url_name"].ToString().ToLower(), dr["id"].ToString());
                }
                dr.Close();
                conn.Close();

                HttpContext.Current.Cache.Insert("cats_names", myHash, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromHours(2), System.Web.Caching.CacheItemPriority.High, null);
                return myHash[catname.ToLower()].ToString();
            }


            return "0";
        }

    }
}
