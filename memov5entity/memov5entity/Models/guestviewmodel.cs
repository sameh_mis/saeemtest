﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using memov5entity.Models;
namespace memov5entity.Models
{
    public class guestviewmodel
    {
        //guestDataContext dt = new guestDataContext();
        GUESTBOOKEntities dt = new GUESTBOOKEntities();
        test4Entities dt2 = new test4Entities();
        // islammemoDataContext dt2 = new islammemoDataContext();
        //SELECT ID, STitle, SName, SEmail, Country, RealCountry, Title, Body, Date FROM Guest WHERE ([Case] = 1) order by id desc
        int count, page;
        public int getpageno
        {
            get { return page; }
            set { page = value; }
        }
        public guestviewmodel() { }
        public guestviewmodel(int page)
        {
            this.page = page;

            this.count = (from p in dt.Guest
                          where p.Case == true && (p.RealCountry == "" || p.RealCountry.ToLower() == null)
                          orderby p.ID descending
                          select p.ID).Count();



        }

        public int get_pages_count()
        {


            //  if (pages_count < 33) return 1;
            int pages_count = count / 6;
            try
            {
                if (pages_count % count > 0 && pages_count > 0)
                    pages_count++;
            }
            catch { }
            return pages_count;

        }
        public List<Guest> get7comments()
        {
            int skippingno = 0;
            int cat_count = get_pages_count();
            int true_page = cat_count + 1 - page;
            if (page <= cat_count && page > 0)
                skippingno = cat_count - true_page;
            List<Guest> comments_list;
            if (page == 1)
            {

                comments_list = (from p in dt.Guest
                                 where p.Case == true && (p.RealCountry == "" || p.RealCountry.ToLower() == null)
                                 orderby p.ID descending
                                 select p).Take(6).ToList();


                return comments_list;
            }
            else
            {

                comments_list = (from p in dt.Guest
                                 where p.Case == true && (p.RealCountry == "" || p.RealCountry.ToLower() == null)
                                 orderby p.ID descending
                                 select p).Skip(skippingno * 6).Take(6).ToList();



                return comments_list;
            }
        }
        public string getlongdate()
        {
            // SELECT TOP (1) Articles.LongDate FROM article_cats INNER JOIN Articles ON article_cats.ArtID = Articles.ID WHERE (article_cats.CatID = 621) and (stat=8) and (date<getdate()) order by date desc
            return (from p in dt2.Articles
                    join p2 in dt2.article_cats
                    on p.ID equals p2.ArtID
                    where p2.CatID == 621 &&
                    p.stat == 8 &&
                    p.Date < DateTime.Now
                    orderby p.Date descending
                    select p).FirstOrDefault().LongDate;

        }

        public string explore_important_news()
        {
            StringBuilder sb = new StringBuilder();



            var news = from p in dt2.marque_article
                       join p2 in dt2.Articles
                       on p.article_id equals p2.ID
                       where p.cat_id == 1
                       orderby p.rank
                       select new { p2.Title, p2.ID, p2.url };





            foreach (var art in news)
            {
                sb.Append(string.Format("<a target='_blank' href='{0}'>{1}</a>", art.url, art.Title));

                sb.Append("*");
            }

            return sb.ToString();


        }
        public void add_comment(string name, string email, string ip, string country, string title, string body)
        {
            Guest g = new Guest();

            g.CatID = 0;
            g.STitle = "";
            g.SName = name;
            g.SEmail = email;
            g.IP = ip;
            g.Country = country;
            g.RealCountry = "";
            g.Telephon = "";
            g.Title = title;
            g.Notes = "";
            g.Body = body;
            g.Date = DateTime.Now;
            g.Opened = false;
            g.Case = false;
            dt.AddToGuest(g);
            dt.SaveChanges();
            //dt.Guests.InsertOnSubmit(g);
            //dt.SubmitChanges();


        }

    }
}