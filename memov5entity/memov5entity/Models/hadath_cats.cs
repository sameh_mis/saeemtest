﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace memov5entity.Models
{
    public class hadath_cats
    {
        test4Entities dt = new test4Entities();
        int id, count, page;
        string c_url;

        public virtual string C_url
        {
            get { return c_url; }
            set { c_url = value; }
        }

        public virtual int Count
        {
            get { return count; }
            set { count = value; }
        }
        string cat_name;

        public virtual string Cat_name
        {
            get { return cat_name; }
            set { cat_name = value; }
        }
        string cat_image;

        public virtual string Cat_image
        {
            get { return cat_image; }
            set { cat_image = value; }
        }
        public hadath_cats()
        { }
        public hadath_cats(string c_url, int page)
        {

            string[] url_array = c_url.Split('/');
            if (url_array[1] == "hadath-el-saa")
            {
                string resolveentityproblem = url_array[2];
                Category c = dt.Categories.Where(p => p.url_name == resolveentityproblem).FirstOrDefault();
                id = c.ID;
                cat_image = c.image_url;
                if (string.IsNullOrEmpty(cat_image))
                {
                    cat_image = "media/noimage.jpg";
                }
                else if (cat_image.Contains("_new_"))
                {
                    cat_image = cat_image.Split('.')[0] + "_691_157_." + cat_image.Split('.')[1];
                }
                count = (from p in dt.Articles
                         where p.stat == 8 && p.cat_id == id
                         select p.ID).Count();
                this.page = page;
                cat_name = c.Name;
            }
            if (c_url.Contains("/page/"))
            {
                string[] ss = Regex.Split(c_url, "/page/");
                this.c_url = ss[0];
            }
            else
            {
                this.c_url = c_url;
            }


        }




        public virtual int get_pages_count()
        {


            //  if (pages_count < 33) return 1;
            int pages_count = count / 17;
            //resolve related_articles_for_eventhot=0
            if (pages_count != 0)
            {
                if (pages_count % count > 0 && pages_count > 0)
                    pages_count++;
            }
            return pages_count;

        }




        public virtual List<Article> get_top33_articles()
        {
            int skippingno = 0;
            int cat_count = get_pages_count();
            if (page < cat_count && page > 0)
                skippingno = cat_count - page;
            if (page == 1)
            {
                List<Article> arts = (from p in dt.Articles
                                      where p.stat == 8 && p.cat_id == id
                                      orderby p.ID ascending
                                      select p).Take(17).ToList();
                return arts;
            }
            else
            {
                List<Article> arts = (from p in dt.Articles
                                      where p.stat == 8 && p.cat_id == id
                                      orderby p.ID descending
                                      select p).Skip(skippingno * 17).Take(17).ToList();
                return arts;
            }
        }






        public string get_images_sever_name()
        {
            return images_server.name;
        }
        public string rewrite_url(string art_url)
        {
            //if (s.StartsWith("/20")) { s = "/hadath-el-saa/" + related[a].cat_id  + s; }
            //if (art_url.StartsWith("/20"))
            //{
            //    art_url = "/hadath-el-saa/" +id+ art_url;
            //}
            return art_url;
        }

        //public virtual string getlongdate()
        //{
        //     return (from p in dt.Articles
        //            where p.cat_id == id &&
        //            p.stat == 8 &&
        //            p.Date < DateTime.Now
        //            orderby p.Date descending
        //            select p).FirstOrDefault().LongDate;

        //}




        public virtual List<Article> get_tkarer_articles()
        {
            List<int> akhbar_articles_ids = (from p in dt.Articles
                                             join p2 in dt.article_cats
                                             on p.ID equals p2.ArtID
                                             where p.cat_id == id && p2.CatID == 621
                                             select p.ID).ToList();

            List<int> all_articles_ids = (from p in dt.Articles
                                          join p2 in dt.article_cats
                                          on p.ID equals p2.ArtID
                                          where p.cat_id == id
                                          select p.ID).Distinct().ToList();

            List<int> tkarer_articles_ids = new List<int>();
            List<Article> tkarer_articles = new List<Article>();



            foreach (int a in all_articles_ids)
            {
                bool c = false;
                foreach (int b in akhbar_articles_ids)
                {
                    if (a == b)
                    {
                        c = true;
                        break;
                    }
                }
                if (c == false)
                    tkarer_articles_ids.Add(a);

            }
            foreach (int a in tkarer_articles_ids)
            {
                //Response.Write(a + ".............");
                Article art = dt.Articles.Where(p => p.ID == a).FirstOrDefault();
                tkarer_articles.Add(art);
            }
            tkarer_articles = tkarer_articles.OrderBy(p => p.Date).Reverse().Take(7).ToList();
            return tkarer_articles;

        }

        public virtual List<Article> topreadingarticles()
        {
            List<Article> l = (from p in dt.Articles
                               where p.stat == 8 && p.cat_id == id
                               orderby p.Readers descending
                               select p).Take(5).ToList();
            return l;


        }
    }
}