﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Text;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// Summary description for mytext
/// </summary>
public class mytext
{

    public static string get_bool_txt(string aaa) 
    {
        if(aaa.ToLower()=="true")
        {
            return "نعم";
        }
        return "لا";
    }
    public static string get_bool_txt(bool aaa)
    {
        if (aaa)
        {
            return "نعم";
        }
        return "لا";
    }
    public static bool get_bool_from_txt(string aaa)
    {
        return (aaa.ToLower() == "true");
    }


    public static string PreparURLforQueryString(string txt)
    {
        UriBuilder u = new UriBuilder(txt);
        if (string.IsNullOrEmpty(u.Query))
        {
            return txt + "?";
        }
        else
        {
            return txt + "&";
        }
    }

    public static string UpdateQueryStringItem(string RawUrl, string queryStringKey, string newQueryStringValue,NameValueCollection querystring)
    {
        string NewURL = RawUrl;

        //QueryString CONTAINS the Key...
        if (querystring.Get(queryStringKey) != null)
        {
            string OrignalSet = String.Format("{0}={1}", queryStringKey, querystring.Get(queryStringKey));
            string NewSet = String.Format("{0}={1}", queryStringKey, newQueryStringValue);

            //REMOVE the key/value completely since the NewValue is blank
            if (newQueryStringValue.Trim() == "")
            {
                //Replace Key/Value down the line...
                NewURL = Regex.Replace(NewURL, "&" + OrignalSet, "", RegexOptions.IgnoreCase);

                //Replace Key/Value at the beginning When there is other
                //key/values after...
                NewURL = Regex.Replace(NewURL, "?" + OrignalSet + "&", "?", RegexOptions.IgnoreCase);

                //Replace Key/Value at the beginning when there 
                //are NO other Key/Values.
                NewURL = Regex.Replace(NewURL, "?" + OrignalSet, "", RegexOptions.IgnoreCase);
            }
            //REPLACE old value with new value.
            else
            {
                NewURL = Regex.Replace(NewURL, OrignalSet, NewSet, RegexOptions.IgnoreCase);
            }
        }
        //Only add the key/value IF the new value is not blank.
        else if (newQueryStringValue.Trim() != "")
        {
            //QueryString DOES NOT CONTAIN the Key... and DOES NOT HAVE other key/value pairs.
            if (querystring.AllKeys.Length == 0)
            {
                NewURL += string.Format("?{0}={1}", queryStringKey, newQueryStringValue);
            }
            //QueryString DOES NOT CONTAIN the Key... and HAS other key/value pairs.
            else
            {
                NewURL += string.Format("&{0}={1}", queryStringKey, newQueryStringValue);
            }
        }
        return NewURL;
    }

    internal static string QuoteJScriptString(string value, bool forUrl)
    {
        StringBuilder builder1 = null;
        if (string.IsNullOrEmpty(value))
        {
            return string.Empty;
        }
        int num1 = 0;
        int num2 = 0;
        for (int num3 = 0; num3 < value.Length; num3++)
        {
            char ch1 = value[num3];
            if (ch1 <= '"')
            {
                switch (ch1)
                {
                    case '\t':
                        {
                            goto Label_00B0;
                        }
                    case '\n':
                        {
                            goto Label_0185;
                        }
                    case '\v':
                    case '\f':
                        {
                            goto Label_01EE;
                        }
                    case '\r':
                        {
                            goto Label_007A;
                        }
                    case '"':
                        {
                            goto Label_00E6;
                        }
                }
                goto Label_01EE;
            }
            switch (ch1)
            {
                case '%':
                    {
                        if (!forUrl)
                        {
                            goto Label_01EE;
                        }
                        if (builder1 == null)
                        {
                            builder1 = new StringBuilder(value.Length + 6);
                        }
                        if (num2 > 0)
                        {
                            builder1.Append(value, num1, num2);
                        }
                        builder1.Append("%25");
                        num1 = num3 + 1;
                        num2 = 0;
                        goto Label_01F2;
                    }
                case '&':
                    {
                        goto Label_01EE;
                    }
                case '\'':
                    {
                        if (builder1 == null)
                        {
                            builder1 = new StringBuilder(value.Length + 5);
                        }
                        if (num2 > 0)
                        {
                            builder1.Append(value, num1, num2);
                        }
                        builder1.Append(@"\'");
                        num1 = num3 + 1;
                        num2 = 0;
                        goto Label_01F2;
                    }
                case '\\':
                    {
                        if (builder1 == null)
                        {
                            builder1 = new StringBuilder(value.Length + 5);
                        }
                        if (num2 > 0)
                        {
                            builder1.Append(value, num1, num2);
                        }
                        builder1.Append(@"\\");
                        num1 = num3 + 1;
                        num2 = 0;
                        goto Label_01F2;
                    }
                default:
                    {
                        goto Label_01EE;
                    }
            }
        Label_007A:
            if (builder1 == null)
            {
                builder1 = new StringBuilder(value.Length + 5);
            }
            if (num2 > 0)
            {
                builder1.Append(value, num1, num2);
            }
            builder1.Append(@"\r");
            num1 = num3 + 1;
            num2 = 0;
            goto Label_01F2;
        Label_00B0:
            if (builder1 == null)
            {
                builder1 = new StringBuilder(value.Length + 5);
            }
            if (num2 > 0)
            {
                builder1.Append(value, num1, num2);
            }
            builder1.Append(@"\t");
            num1 = num3 + 1;
            num2 = 0;
            goto Label_01F2;
        Label_00E6:
            if (builder1 == null)
            {
                builder1 = new StringBuilder(value.Length + 5);
            }
            if (num2 > 0)
            {
                builder1.Append(value, num1, num2);
            }
            builder1.Append("\\\"");
            num1 = num3 + 1;
            num2 = 0;
            goto Label_01F2;
        Label_0185:
            if (builder1 == null)
            {
                builder1 = new StringBuilder(value.Length + 5);
            }
            if (num2 > 0)
            {
                builder1.Append(value, num1, num2);
            }
            builder1.Append(@"\n");
            num1 = num3 + 1;
            num2 = 0;
            goto Label_01F2;
        Label_01EE:
            num2++;
        Label_01F2: ;
        }
        if (builder1 == null)
        {
            return value;
        }
        if (num2 > 0)
        {
            builder1.Append(value, num1, num2);
        }
        return builder1.ToString();
    }

    public static string serverurl(HttpRequest myrequest) 
    {
        string Port = myrequest.ServerVariables["SERVER_PORT"];

        if (Port == null || Port == "80" || Port == "443")

            Port = "";

        else

            Port = ":" + Port;



        string Protocol = myrequest.ServerVariables["SERVER_PORT_SECURE"];

        if (Protocol == null || Protocol == "0")

            Protocol = "http://";

        else

            Protocol = "https://";





        // *** Figure out the base Url which points at the application's root

        string myurl = Protocol + myrequest.ServerVariables["SERVER_NAME"] +

                                    Port + myrequest.ApplicationPath;

        if(! myurl.EndsWith("/"))
        {
            myurl += "/";
        }

        return myurl;
    }

    public static string getthumbname(string imagename) 
    {
        string name=imagename.Substring(0,imagename.LastIndexOf(".")<0?0:imagename.LastIndexOf("."));
        string ext = imagename.Substring(imagename.LastIndexOf(".")<0?0:imagename.LastIndexOf("."));

        name += "_th";
        return name + ext;
    }

    public static string getdatestring(string mydate)
    {

        try 
        {
           return DateTime.Parse(mydate).ToString();
        }catch
        {
            return "غير متاح";
        }
    }

    public static string trimtext(string text, int count)
    {
       if(text.Length<count)
       {
           return text;
       }
       text = text.Substring(0, count);
       if (text.LastIndexOf(' ')>-1)
       {
           return text.Substring(0, text.LastIndexOf(' ')) + "...";
       }
       else
       {
           return text;
       }
    }

    public static string getcasestring(string strcase)
    {
        int temp;
        int.TryParse(strcase, out temp);
        switch (temp)
        {
            case 1:
                strcase = "خبر مرشح";
                break;
            case 2:
                strcase = "خبر مرفوض بعد الترشيح";
                break;
            case 3:
                strcase = "خبر مفتوح للكتابه";
                break;
            case 4:
                strcase = "انتهى الكتابه ومنتظر التصحيح";
                break;
            case 5:
                strcase = "التصحيح اللغوى خاطى";
                break;
            case 6:
                strcase = "التصحيح اللغوى صحيح";
                break;
            case 7:
                strcase = "مرفوض السماح";
                break;
            case 8:
                strcase = "سمح له ";
                break;
            default:
                strcase = "0";
                break;
        }
        return strcase;
    }

    public static string getactionpane(string myartid, string mystat, string writername, List<string> myroles, string req_cat, string req_cas, string myuname)
    {
        int tempstat;
        int.TryParse(mystat, out tempstat);
        int tempartid;
        int.TryParse(myartid, out tempartid);

        StringBuilder actionpanestring = new StringBuilder();


        if (myroles.Contains("programmers") || myroles.Contains("admin"))
        {
            switch (tempstat)
            {
                case 1:
                    actionpanestring.Append("<a href=\"javascript:showallowsug(\'" + myartid + "&hh=1\');\"><img src='../icons/allowsug.gif' border=0 title='سماح' /></a>");

                    break;
                case 2:
                    actionpanestring.Append("<a href=\"javascript:showallowsug(\'" + myartid + "&hh=1\');\"><img src='../icons/allowsug.gif' border=0 title='سماح' /></a>");

                    break;
                case 3:
                    actionpanestring.Append(string.Format("<a href='edit.aspx?id={0}&cat={1}&cas={2}'><img src='../icons/write.gif' border=0 title='كتابه المقال' /></a>", myartid, req_cat, req_cas));
                    actionpanestring.Append("<a href=\"javascript:showallowsug(\'" + myartid + "&hh=1\');\"><img src='../icons/allowsug.gif' border=0 title='سماح' /></a>");

                    break;
                case 4:
                    actionpanestring.Append(string.Format("<a href='edit.aspx?id={0}&cat={1}&cas={2}'><img src='../icons/edit.gif' border=0 title='تعديل' /></a>", myartid, req_cat, req_cas));
                    actionpanestring.Append("<a href=\"javascript:popUp(\'checkart.aspx?id=" + myartid + "\');\"><img src='../icons/spell.gif' border=0 title='تصحيح لغوى' /></a>");
                    break;
                case 5:
                    actionpanestring.Append(string.Format("<a href='edit.aspx?id={0}&cat={1}&cas={2}'><img src='../icons/edit.gif' border=0 title='تعديل' /></a>", myartid, req_cat, req_cas));
                    actionpanestring.Append("<a href=\"javascript:popUp(\'checkart.aspx?id=" + myartid + "\');\"><img src='../icons/spell.gif' border=0 title='تصحيح لغوى' /></a>");
                    break;
                case 6:
                    actionpanestring.Append(string.Format("<a href='edit.aspx?id={0}&cat={1}&cas={2}'><img src='../icons/edit.gif' border=0 title='تعديل' /></a>", myartid, req_cat, req_cas));
                    actionpanestring.Append("<a href=\"javascript:popUp(\'checkart.aspx?id=" + myartid + "\');\"><img src='../icons/spell.gif' border=0 title='تصحيح لغوى'/></a>");
                    actionpanestring.Append("<a href=\"javascript:showallow(\'" + myartid + "&hh=1\');\"><img src='../icons/allow.gif' border=0 title='سماح' /></a>");
                    break;
                case 7:
                    actionpanestring.Append(string.Format("<a href='edit.aspx?id={0}&cat={1}&cas={2}'><img src='../icons/edit.gif' border=0 title='تعديل' /></a>", myartid, req_cat, req_cas));
                    actionpanestring.Append("<a href=\"javascript:showallow(\'" + myartid + "&hh=1\');\"><img src='../icons/allow.gif' border=0 title='سماح' /></a>");
                    break;
                case 8:
                    actionpanestring.Append(string.Format("<a href='edit.aspx?id={0}&cat={1}&cas={2}'><img src='../icons/edit.gif' border=0 title='تعديل' /></a>", myartid, req_cat, req_cas));
                    actionpanestring.Append("<a href=\"javascript:showallow(\'" + myartid + "&hh=1\');\"><img src='../icons/allow.gif' border=0 title='سماح' /></a>");
                    break;
                default:
                    break;
            }
            actionpanestring.Append("<a href='delete.aspx?id=" + myartid + "'><img src='../icons/delete.gif' border=0 title='حذف' /></a>");
            actionpanestring.Append("<a href=\"javascript:popUp('addnote.aspx?id=" + myartid + "')\"><img border='0' src='../icons/addnote.gif' title='اضف ملاحظه' /></a>");
        }
        else if (myroles.Contains("checker"))
        {
            //checker and writer
            switch (tempstat)
            {
                case 1:
                    //empty
                    break;
                case 2:
                    //empty
                    break;
                case 3:
                    //empty
                    break;
                case 4:
                    actionpanestring.Append("<a href=\"javascript:popUp(\'checkart.aspx?id=" + myartid + "\');\"><img src='../icons/spell.gif' border=0 title='تصحيح لغوى' /></a>");
                    break;
                case 5:
                    actionpanestring.Append("<a href=\"javascript:popUp(\'checkart.aspx?id=" + myartid + "\');\"><img src='../icons/spell.gif' border=0 title='تصحيح لغوى' /></a>");
                    break;
                case 6:
                    actionpanestring.Append("<a href=\"javascript:popUp(\'checkart.aspx?id=" + myartid + "\');\"><img src='../icons/spell.gif' border=0 title='تصحيح لغوى' /></a>");
                    break;
                case 7:
                    break;
                case 8:
                    //empty
                    break;
                default:
                    break;
            }
            if (myroles.Contains("writers"))
            {
                if (myuname == writername)
                {
                    switch (tempstat)
                    {
                        case 1:
                            actionpanestring.Append("(edite sugg)<br/>");
                            actionpanestring.Append("<img src='../icons/delete.gif' border=0 title='حذف' />");
                            break;
                        case 2:
                            //empty
                            break;
                        case 3:
                            actionpanestring.Append(string.Format("<a href='edit.aspx?id={0}&cat={1}&cas={2}'><img src='../icons/write.gif' border=0 title='كتابه المقال' /></a>", myartid, req_cat, req_cas));
                            break;
                        case 4:
                            actionpanestring.Append(string.Format("<a href='edit.aspx?id={0}&cat={1}&cas={2}'><img src='../icons/edit.gif' border=0 title='تعديل' /></a>", myartid, req_cat, req_cas));
                            break;
                        case 5:
                            actionpanestring.Append(string.Format("<a href='edit.aspx?id={0}&cat={1}&cas={2}'><img src='../icons/edit.gif' border=0 title='تعديل' /></a>", myartid, req_cat, req_cas));
                            break;
                        case 6:
                            actionpanestring.Append(string.Format("<a href='edit.aspx?id={0}&cat={1}&cas={2}'><img src='../icons/edit.gif' border=0 title='تعديل' /></a>", myartid, req_cat, req_cas));
                            //empty
                            break;
                        case 7:
                            actionpanestring.Append(string.Format("<a href='edit.aspx?id={0}&cat={1}&cas={2}'><img src='../icons/edit.gif' border=0 title='تعديل' /></a>", myartid, req_cat, req_cas));
                            break;
                        case 8:
                            //empty
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        else if (myroles.Contains("writers"))
        {

            if (myuname == writername.Trim())
            {
                switch (tempstat)
                {
                    case 1:
                        actionpanestring.Append("(edite sugg)<br/>");
                        actionpanestring.Append("<img src='../icons/delete.gif' border=0 title='حذف' />");
                        break;
                    case 2:
                        //empty
                        break;
                    case 3:
                        actionpanestring.Append(string.Format("<a href='edit.aspx?id={0}&cat={1}&cas={2}'><img src='../icons/write.gif' border=0 title='كتابه المقال' /></a>", myartid, req_cat, req_cas));
                        break;
                    case 4:
                        actionpanestring.Append(string.Format("<a href='edit.aspx?id={0}&cat={1}&cas={2}'><img src='../icons/edit.gif' border=0 title='تعديل' /></a>", myartid, req_cat, req_cas));
                        break;
                    case 5:
                        actionpanestring.Append(string.Format("<a href='edit.aspx?id={0}&cat={1}&cas={2}'><img src='../icons/edit.gif' border=0 title='تعديل' /></a>", myartid, req_cat, req_cas));
                        break;
                    case 6:
                        //empty
                        break;
                    case 7:
                        actionpanestring.Append(string.Format("<a href='edit.aspx?id={0}&cat={1}&cas={2}'><img src='../icons/edit.gif' border=0 title='تعديل' /></a>", myartid, req_cat, req_cas));
                        break;
                    case 8:
                        //empty
                        break;
                    default:
                        break;
                }
            }
        }

        return actionpanestring.ToString();
    }

    public static void logevent(string message) 
    {
        File.AppendAllText("D:\\waleef\\mylog.txt",message+Environment.NewLine);
    }

    public static String GetRequestedURL(String url)
    {
        // Determine the page the user is requesting.
        //String url = originalRequest;
        //HttpContext.Current.Response.Write(url);
        // Check for an error URL which IIS will throw when a non-existing 
        // directory is requested and custom errors are thrown back to this site

        if (url.StartsWith("/redirect.aspx?404;"))
        {
            url = url.Substring(url.IndexOf(".cc:80/")+6);
        }
       // HttpContext.Current.Response.Write("<br/>");
        //HttpContext.Current.Response.Write(url);
        return url;
        /*
        if (url.Contains(";") && url.Contains("404"))
        {
            // Split the URL on the semi-colon. Error requests look like:
            // errorpage.aspx?404;http://originalrequest OR
            // errorpage.aspx?404;http://originalrequest:80
            String[] splitUrl = url.Split(';');

            // Set the URL to the original request to allow processing of it.
            if (splitUrl.Length >= 1)
            {
                url = splitUrl[1];

                // Remove the first http://
                url = url.Replace("http://", "");

                // Get only the application path and beyond of the string. This 
                // may be / if it's in the root folder.
                int index = url.IndexOf(
                    System.Web.HttpContext.Current.Request.ApplicationPath);
                if (index >= 0)
                {
                    url = url.Substring(index);
                }
            }
        }
        //else { url = "ddd"; }
       


        return url;
         * 
         */
    }
}
