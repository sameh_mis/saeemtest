﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Data;
using System.Configuration;
namespace memov5entity.Models
{
    public class connection
    {

        static DbProviderFactory factory;
        static SqlConnection conn;
       

        static DbProviderFactory pv()
        {
            string p = ConfigurationManager.ConnectionStrings["test4ConnectionString"].ProviderName;
            factory = DbProviderFactories.GetFactory(p);
            return factory;
        }

        static SqlConnection getconnection()
        {
            string s = ConfigurationManager.ConnectionStrings["test4ConnectionString"].ConnectionString;
            conn = new SqlConnection();
            conn.ConnectionString = s;
            return conn;
        }
        public static DataTable getdatatable(string select, string from, string where, string orderby)
        {
            conn = getconnection();
            SqlDataAdapter dad = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = select + " " + from + " " + where + " " + orderby;
            dad.SelectCommand = cmd;
            DataTable dt = new DataTable();
            dad.Fill(dt);
            return dt;
        }

        public static DataTable getdatatable(string table_name, string column_name, string order_type)
        {
            conn = getconnection();
            SqlDataAdapter dad = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = "select * from " + table_name + " order by " + column_name + " " + order_type;
            dad.SelectCommand = cmd;
            DataTable dt = new DataTable();
            dad.Fill(dt);
            return dt;
        }


        public static bool runproc(string procname, ArrayList ar1, ArrayList ar2)
        {
            conn = getconnection();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = procname;
            cmd.Connection = conn;
            conn.Open();
            if (ar1 != null)
            {
                for (int i = 0; i < ar1.Count; i++)
                {
                    DbParameter p1 = cmd.CreateParameter();
                    p1.ParameterName = ar1[i].ToString();
                    p1.Value = ar2[i];
                    cmd.Parameters.Add(p1);
                }
            }
            try
            {

                cmd.ExecuteNonQuery();
                return true;


            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                conn.Close();
            }
        }
        public static bool runproc(string procname, string ar1, string ar2)
        {
            conn = getconnection();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = procname;
            cmd.Connection = conn;
            conn.Open();
            if (ar1 != "")
            {
                DbParameter p1 = cmd.CreateParameter();
                p1.ParameterName = ar1;
                p1.Value = ar2;
                cmd.Parameters.Add(p1);

            }
            try
            {

                cmd.ExecuteNonQuery();
                return true;


            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                conn.Close();
            }
        }
        public static object scaler_proc(string proc, ArrayList ar1, ArrayList ar2)
        {
            conn = getconnection();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = proc;
            cmd.Connection = conn;
            try
            {
                conn.Open();
                if (ar1 != null)
                {
                    for (int i = 0; i < ar1.Count; i++)
                    {
                        SqlParameter p1 = cmd.CreateParameter();
                        p1.ParameterName = ar1[i].ToString();
                        p1.Value = ar2[i];
                        cmd.Parameters.Add(p1);
                    }
                }
                return cmd.ExecuteScalar();
            }
            catch
            {
                return cmd.ExecuteScalar();
            }
            finally
            {
                conn.Close();
            }
        }
        public static DataTable reader_proc(string proc, ArrayList ar1, ArrayList ar2)
        {
            conn = getconnection();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = proc;
            cmd.Connection = conn;
            conn.Open();
            if (ar1 != null)
            {
                for (int i = 0; i < ar1.Count; i++)
                {
                    DbParameter p1 = cmd.CreateParameter();
                    p1.ParameterName = ar1[i].ToString();
                    p1.Value = ar2[i];
                    cmd.Parameters.Add(p1);
                }
            }
            //return cmd.ExecuteReader();
            DataTable d = new DataTable();
            SqlDataReader dr = cmd.ExecuteReader();
            d.Load(dr);
            try
            {
                return d;
            }
            catch
            {
                return d;
            }
            finally
            {
                conn.Close();
            }

        }


        public static bool update(string insert)
        {
            try
            {
                conn = getconnection();
                SqlCommand cmd = new SqlCommand(insert, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                return true;
            }
            catch { return false; }
            finally
            {
                conn.Close();
            }

        }

        public connection()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}