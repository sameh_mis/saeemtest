﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace memov5entity.Models
{
    public class temp_art
    {
        public string title, apprev, url, img;
        public DateTime? date;
        public int? readers;
        public temp_art()
        {

        }
        public temp_art(string title, string apprev, string url, string img)
        {
            this.title = title;
            this.apprev = apprev;
            this.url = url;
            this.img = img;
        }
    }
}