﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Collections;
using memov5entity.Models;
using memov5entity.Models;
namespace memov5entity.Models
{
    public class singlearticle
    {
       // islammemoDataContext dt = new islammemoDataContext();
        test4Entities dt = new test4Entities();
        //string article_url;
        int id;
        string art_url;

        public string Art_url
        {
            get { return art_url; }
            set { art_url = value; }
        }
        public virtual int Id
        {

            get { return id; }
            set { id = value; }
        }
        Article a;
        public singlearticle()
        {
        }
        public singlearticle(string cat1, string cat2, int? year, int? month, int? day, int? id)
        {

            this.id = Convert.ToInt32(id);
            a = (from p in dt.Articles
                 where p.ID == this.id
                 select p).FirstOrDefault();
            art_url = a.url;


        }
        public virtual Article getarticle()
        {

            return a;

        }
        public virtual List<Article> getarticleschain()
        {
            List<Article> chain = (from p in dt.Articles
                                   where p.stat == 8 && p.chain == a.chain
                                   select p).ToList();
            return chain;
        }
        public virtual string translatorname()
        {
            return "";
        }
        public  List<Article> getrelatedarticles()
        {
            List<article_related> related = a.getallrelatedarticles(5);

            List<Article> articles = new List<Article>();
            foreach (article_related r in related)
            {
                Article art = dt.Articles.Where(p => p.ID == r.related_article_id).FirstOrDefault();
                articles.Add(art);
            }
            return articles.ToList();
        }
        public virtual List<comment> getcomments()
        {
            List<comment> comments = (from p in dt.comments
                                      where p.article_id == a.ID && p.allow == true
                                      select p).ToList();
            //  dt.comments.Where(p => p.article_id == a.ID).ToList();
            return comments;
        }
        public virtual List<Category> parentcategories()
        {
            //int cat_id =Convert.ToInt32((from p in dt.Articles
            //              where p.ID == a.ID
            //              select p).FirstOrDefault().cat_id);
            Category c = dt.Categories.Where(p => p.ID == a.cat_id).FirstOrDefault();
            Category parent = null; //dt.Categories.Where(p => p.ID == c.ParentID).FirstOrDefault();
            int pid = 0;
            if (c != null)
            {
                pid = c.ParentID;
            }
            while (pid != 0)
            {
                parent = dt.Categories.Where(p => p.ID == pid).FirstOrDefault();
                try
                {
                    pid = parent.ParentID;
                }
                catch
                {
                    pid = 0;
                }
            }
            List<Category> parents = new List<Category>();
            if (c != null)
                parents.Add(c);
            if (parent != null)
                parents.Add(parent);
            return parents;
        }
        public virtual List<Article> getimportantarticles()
        {
            List<Article> news = (from p in dt.marque_article
                                  join p2 in dt.Articles
                                  on p.article_id equals p2.ID
                                  where p.cat_id == 1
                                  orderby p.rank
                                  select p2).Take(5).ToList();

            return news;
        }

        public void addreader()
        {
            a.Readers++;
            dt.SaveChanges();

        }
        //***********************************memo_mobil*********************************//
        public List<Article> getrelatedarticles_mobil()
        {
            List<article_related> related = a.getallrelatedarticles(3);

            List<Article> articles = new List<Article>();
            foreach (article_related r in related)
            {
                Article art = dt.Articles.Where(p => p.ID == r.related_article_id).FirstOrDefault();
                articles.Add(art);
            }
            return articles.ToList();
        }

    }
}