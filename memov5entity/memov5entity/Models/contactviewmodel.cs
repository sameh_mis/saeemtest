﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using System.Text;
using memov5entity.Models;
namespace memov5entity.Models
{
    public class contactviewmodel
    {
        public contactviewmodel()
        { }
        //SELECT ID, Name FROM ContactCategories where showglobal=1
        // contactDataContext dt = new contactDataContext();
        contact2Entities dt = new contact2Entities();
        string type;
        public contactviewmodel(string type)
        {
            this.type = type;

        }
        public string get_browsable_contacts()
        {

            //if (string.IsNullOrEmpty(language) || language.StartsWith("ar")) language = "";
            // List<ContactCategory>c=dt.ContactCategories.Where(p=>p.ShowGlobal==true).ToList();
            var c = (from p in dt.ContactCategories
                     where p.ShowGlobal == true
                     select new { p.ID, p.Name }).ToList();
            //return c;
            StringBuilder sb = new StringBuilder();

            foreach (var cat in c)
            {
                //            <option value="4">استفسار</option> 
                //<option selected="selected" value="5">أرسل مشاركتك</option> 
                //<option value="6">أعلن معنا</option> 
                string engcatname = cat.Name;

                if (type == "1")
                {

                    if (cat.Name == "أرسل مشاركتك")
                    {


                        sb.Append(string.Format("<option value='{0}' selected='selected'>{1}</option>", cat.ID, engcatname));
                    }
                  
                                      
                }
               
                if (type == "2")
                {

                    if (cat.Name == "أعلن معنا")
                    {


                        sb.Append(string.Format("<option value='{0}' selected='selected'>{1}</option>", cat.ID, engcatname));
                    }
                    else
                    {
                        sb.Append(string.Format("<option value='{0}'>{1}</option>", cat.ID, engcatname));
                    }


                }
                else
                {
                    sb.Append(string.Format("<option value='{0}'>{1}</option>", cat.ID, engcatname));
                }
                
            }
            return sb.ToString();

        }
        public bool insert_contact(string cat_id, string sname, string semail, string country, string title, string body, string cat_ip)
        {
            try
            {
                //CatID, STitle, SName, SEmail, IP, Country, RealCountry, Telephon, Title, Notes, Body, Date, Opened, Repliead
                Contact c = new Contact();
                c.CatID = Convert.ToInt32(cat_id);
                c.STitle = "";
                c.SName = sname;
                c.SEmail = semail;
                c.IP = cat_ip;
                c.Country = country;
                c.Telephon = "";
                c.Title = title;
                c.Notes = "";
                c.Body = body;
                c.Date = DateTime.Now;
                c.Opened = false;
                c.Repliead = false;
                dt.AddToContacts(c);
                dt.SaveChanges();
                //dt.Contacts.InsertOnSubmit(c);
                //dt.SubmitChanges();
                return true;
            }
            catch
            {
                return false;

            }

        }
        public string titlename()
        {
            if (type == "1")
                return "ارسل مشاركتك";
            if(type=="2")
                return "أعلن معنا";
            return "استفسار";
        }
    }
}