﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace memov5entity.Models
{
    public class syria_thawraviewmodel
    {
        test4Entities dt = new test4Entities();
        public List<temp_art> get_syria_akhbar(int arts_no)
        {
            return (from p in dt.Articles
                    join p2 in dt.article_cats
                    on p.ID equals p2.ArtID
                    where p2.CatID == 924 && p.stat == 8
                    orderby p.Date descending
                    select new temp_art() { title = p.Title, apprev = p.Apprev, url = p.url, img = p.image_url,date=p.Date, readers=p.Readers }).Take(arts_no).ToList();
        }
        public List<temp_art> get_syria_tkarer(int arts_no)
        {
            List<temp_art> arts = (from p in dt.Articles
                                   join p2 in dt.article_cats
                                   on p.ID equals p2.ArtID
                                   orderby p.Date descending
                                   where p.cat_id == 924 && p.stat == 8
                                   select new temp_art() { title = p.Title, apprev = p.Apprev, url = p.url, img = p.image_url, date= p.Date,readers=p.Readers }).Except((from p in dt.Articles
                                                                                                                                         join p2 in dt.article_cats
                                                                                                                                         on p.ID equals p2.ArtID
                                                                                                                                         where p.cat_id == 924 && p2.CatID == 621 && p.stat == 8
                                                                                                                                                                           select new temp_art() { title = p.Title, apprev = p.Apprev, url = p.url, img = p.image_url, date = p.Date,readers=p.Readers })).Take(arts_no).ToList();
  

            return arts;
        }
        public List<temp_art> get_important_tkarer(int arts_no)
        {
            List<temp_art> arts = (from p in dt.Articles
                                   join p2 in dt.article_cats
                                   on p.ID equals p2.ArtID
                                   orderby p.Date descending
                                   where p.cat_id == 924 && p.stat == 8 && p.choice==true
                                   select new temp_art() { title = p.Title, apprev = p.Apprev, url = p.url, img = p.image_url, date = p.Date,readers=p.Readers }).Except((from p in dt.Articles
                                                                                                                                                        join p2 in dt.article_cats
                                                                                                                                                        on p.ID equals p2.ArtID
                                                                                                                                                        where p.cat_id == 924 && p2.CatID == 621 && p.stat == 8
                                                                                                                                                        select new temp_art() { title = p.Title, apprev = p.Apprev, url = p.url, img = p.image_url, date = p.Date,readers=p.Readers })).Take(arts_no).ToList();


            return arts;
        }
        public temp_art[] get_top_comments(int arts_no)
        {
            //assetTypes = context.AssetTypes.Where(a => a.AssetClass.Id == 7).Select(a => new { a.Name, a.AssetTypeId, CountOfAssets = a.Asset.Count()).ToList();
           //[dbo].[get_top_comments_forcats] (@arts_no int,@cat int)
            //Title,articles.url,articles.Apprev,articles.image_url,Readers,articles.Date
            ArrayList arr1 = new ArrayList();
            ArrayList arr2 = new ArrayList();
            arr1.Add("@arts_no");
            arr1.Add("@cat");
            arr2.Add(arts_no);
            arr2.Add(924);
            DataTable dtcomments = connection.reader_proc("get_top_comments_forcats", arr1, arr2);
           // string[] arrli = new string[dtcomments.Rows.Count];
            temp_art[] arts = new temp_art[dtcomments.Rows.Count];
            for (int a = 0; a < dtcomments.Rows.Count; a++)
            {
                arts[a] = new temp_art() 
                { 
                    title=dtcomments.Rows[a]["title"].ToString(),
                    apprev = dtcomments.Rows[a]["apprev"].ToString(),
                    url = dtcomments.Rows[a]["title"].ToString(),
                    img = dtcomments.Rows[a]["image_url"].ToString(),
                    readers =Convert.ToInt32( dtcomments.Rows[a]["readers"]),
                     date=Convert.ToDateTime(dtcomments.Rows[a]["date"])
                };
               
            }
            return arts;
        }
    }
}