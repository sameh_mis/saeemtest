﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace memov5entity.Models
{
    public class images_server
    {
        static public string name = "http://38.121.76.242/memoadmin/";
        //http://204.187.100.49:1589/memoadmin/
        //http://208.66.70.165/ismemo/
        //http://38.121.76.242/memoadmin    newserver
        static public string mytitle = "مفكرة الإسلام : ";
        static public string updateimg(string img_name, string dimension)
        {
            string artupdatedimg = "media/noimage.jpg";
            if (string.IsNullOrEmpty(img_name) == false)
            {
                artupdatedimg = img_name;
            }
            if (artupdatedimg.Contains("version4_") && string.IsNullOrEmpty(dimension) == false)
            {
                

                artupdatedimg = artupdatedimg.Split('.')[0] + dimension + "." + artupdatedimg.Split('.')[1];

            }
            return name + artupdatedimg;
        }
    }
}