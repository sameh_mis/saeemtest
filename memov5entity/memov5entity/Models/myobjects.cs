using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.Text.RegularExpressions;



/// <summary>
/// Summary description for myobjects
/// </summary>
/// 


namespace myobjects 
{
    public class CatListItem
    {
        public CatListItem()
        {
            id = "";
            name = "";
            parentId = "";
        }
        public CatListItem(string _id,string _name)
        {
            id = _id;
            name = _name;
            parentId = "";
        }
        public CatListItem(string _id, string _name,string _parentid)
        {
            id = _id;
            name = _name;
            parentId = _parentid;
        }
        public CatListItem(string _id, string _name, string _parentid,bool _door)
        {
            id = _id;
            name = _name;
            parentId = _parentid;
            is_door = _door;
        }

        //fields
        string id;
        string name;
        string parentId;
        bool is_door;

        //properties
        public string Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string ParentId
        {
            get { return parentId; }
            set { parentId = value; }
        }
        public bool Is_door
        {
            get { return is_door; }
            set { is_door = value; }
        }
    }



    public class Article 
    {
        public Article(int artid, System.Web.Caching.Cache cache, HttpApplicationState myapp)
        {
            application = myapp;
            id = artid;

            StringBuilder sb1 = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["newislammemoConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("news_select_article_by_id", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                int mystat = 0;
                int mycat = 0;
                int chainid = 0;
                int.TryParse(dr["stat"].ToString(), out mystat);
                if (mystat != 8)
                {
                    valid = false;
                    title = "���� ����";
                    conn.Close();
                    return;
                }
                int.TryParse(dr["cat_id"].ToString(), out mycat);
                int.TryParse(dr["chain"].ToString(), out chainid);
                title = dr["title"].ToString().Trim();
                apprev = dr["apprev"].ToString().Trim();
                url = dr["url"].ToString().Trim();

                sb1.Append("<div id='article_title' style=\"direction:rtl;margin-top:5px;padding:5px;\" class='main04'>");
                sb1.Append(dr["title"].ToString().Trim());
                sb1.Append("<div style=\"direction:rtl;\" class='date'>");
                sb1.Append(dr["LongDate"].ToString().Trim());
                //sb1.Append("</div>");
                //navigation menu
                sb1.Append("<div class='nolinks' style=\"color:#00517a;padding:2px 2px 2px 2px;direction:rtl;\"><a href='/default.aspx'>������ �������</a>" + memo.design.get_cat_nav_menu(mycat) + "<!--block1-->");
                sb1.Append("</div> </div></div>");

                sb1.Append("<span id='wwww' style='display:none'>" + DateTime.Now.ToString() + "</span>");

                //images


                sb2.Append("<p style=\"text-align:justify;font-family:Simplified Arabic;font-size:12pt;font-weight:bold;\" dir='rtl'>" + "<!--block3-->");
                string mybodyformating = dr["body"].ToString().Replace("����� �������", "<span class='mofakera'>����� �������</span>").Trim();
                //ZetaHtmlTidy.HtmlTidy aa = new ZetaHtmlTidy.HtmlTidy();
                //mybodyformating = aa.CleanHtml(mybodyformating, ZetaHtmlTidy.HtmlTidyOptions.None);
                //mybodyformating = mybodyformating.Substring(mybodyformating.IndexOf("<body>") + 6);
                //mybodyformating = mybodyformating.Substring(0, mybodyformating.LastIndexOf("</body>"));

                sb2.Append(mybodyformating);
                sb2.Append("</p>");

                if (chainid > 0)
                {
                    sb2.Append(memo.design.get_chain_arts(chainid));
                    
                }
                //waleef ads
                sb2.Append("<div align='center' style='width:100%;'><a href='http://www.waleef.net' target='_blank'><img style='margin:20px;' src='http://www.waleef.net/affiliate/images/200�60.gif' width='240' height='60' border='0'/></a><a href='http://mail.islammemo.cc/banneradmin/addclick.aspx?aa=jwalnews_0001' target='_blank'><img src='http://mail.islammemo.cc/temp/r_jaw.gif' style='margin:20px;' border='0'/></a></div>");
                sb2.Append("<table width=\"100%\" dir=rtl><tr><td style=\"width=25%;text-align:center;\" class='save_td'>");
                sb2.Append("<img src=\"/imgs/trans.gif\" style=\"cursor:pointer;width:150px;height:70px;\" onclick=\"savea(" + id.ToString() + ");\" />");
                sb2.Append("</td>");
                sb2.Append("<td style=\"width=25%;text-align:center;\" class='print_td'>");
                sb2.Append("<img src=\"/imgs/trans.gif\" style=\"cursor:pointer;width:150px;height:70px;\" onclick=\"printa(" + id.ToString() + ");\"/>");
                sb2.Append("</td>");
                sb2.Append("<td style=\"width=25%;text-align:center;\" class='send_td'>");
                sb2.Append("<img src=\"/imgs/trans.gif\" style=\"cursor:pointer;width:150px;height:70px;\" onclick=\"senda(" + id.ToString() + ");\"/>");
                sb2.Append("</td>");
                sb2.Append("<td style=\"width=25%;text-align:center;\" class='read_td'>");
                sb2.Append("<div>");
                sb2.Append("<div style=\"position:relative; left: 26px; top: 38px;z-index:100;\"><span class='reads_counter'>");
                //reads
                //sb1.Append("</div></td>");
                //sb1.Append("</tr></table>");
                application.Set("art_reads_" + id.ToString(), dr["readers"]);


                List<string> Myimages = new List<string>();
                List<string> Mydisc = new List<string>();
                List<string> Mynames = new List<string>();
                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        Myimages.Add(dr["path"].ToString());
                        Mydisc.Add(dr["discription"].ToString().Trim());
                        Mynames.Add(dr["name"].ToString().Trim());
                    }
                }
                sb1.Append(memo.design.get_image_slide(Myimages,Mydisc,Mynames));

                

                text1 = sb1.ToString()+sb2.ToString();
                valid = true;

                //COMMENTS
                sb2 = new StringBuilder();
                dr.Close();
                cmd = new SqlCommand("select  title, body, username, cdate from comments where article_id=@id and allow=1", conn);
                cmd.Parameters.AddWithValue("@id", id);

                sb2.Append("<div align=\"center\"><table border=\"0\" cellpadding=\"0\" style=\"border-collapse: collapse\" dir=\"rtl\"><tr><td class=\"manage_top_cat_rc\" style=\"height:22px;width:20px;\"></td><td  class=\"manage_top_cat_c\" ><div  style=\"padding-top:5px;white-space:nowrap;\" class='main'>���������</div></td><td class=\"manage_top_cat_lc\"></td></tr></table></div><div class=\"archive_content_div\" style=\"direction:rtl;\"><div style=\"height:5px;\"></div>");

                sb2.Append("<table width=\"100%\" dir=\"rtl\" cellpadding=\"0\" cellspacing=\"0\">");
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {

                    sb2.Append(string.Format("<tr><td class=\"guestbook01\">{0}</td><td class=\"guestbook01\">{1}</td></tr><tr><td colspan=\"2\" ><div class=\"guestbook02\" ><span class=\"guestbook07\">{2}</span><br />{3}</div></td></tr>", dr["username"].ToString(), dr["cdate"].ToString(), dr["title"].ToString(), dr["body"].ToString()));
                }

                sb2.Append("</table>");

                sb2.Append("</div>");
                text3 = sb2.ToString();

            }
            else
            {
                valid = false;
                title = "���� ����";
            }
            conn.Close();
        }
        


        HttpApplicationState application=null;
        int id;

        bool valid = false;
        public bool Valid
        {
            get { return valid; }
            //set { valid = value; }
        }

        string url;

        public string Url
        {
            get { return url; }
            //set { url = value; }
        }

        string title;
        public string Title 
        {
            get { return title; }
            set { title = value; }
        }

        string apprev;
        public string Apprev 
        {
            get { return apprev; }
            set { apprev = value; }
        }
        string text1;
        string text3;
        int reads;
        public int Reads 
        {
            get { return reads; }
            set { reads = value; }
        }
        //bool valid = false;

        public string read()
        {
            if (!valid)
            {
                return "<div id='article_title' style=\"background-color:#e2f4ff;margin-top:5px;color:#00517a;text-align:right;font-weight:bold;font-size:130%;padding:2px 2px 2px 2px;\">��� ������ ��� ������ </div>";
            }
            SqlConnection conn;
            SqlCommand cmd;

            application.Lock();

            if (application.Get("art_reads_" + id.ToString()) != null)
            {
                int.TryParse(application.Get("art_reads_" + id.ToString()).ToString(), out reads);
            }
            else 
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["newislammemoConnectionString"].ConnectionString);
                cmd = new SqlCommand("select readers from articles where id=@id", conn);
                cmd.Parameters.AddWithValue("@id", id);
                conn.Open();
                int.TryParse(cmd.ExecuteScalar().ToString(),out reads);
                conn.Close();

                reads = 1; 
            }
            reads++;
            application.Set("art_reads_" + id.ToString(),reads);
            application.UnLock();

            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["newislammemoConnectionString"].ConnectionString);
            cmd = new SqlCommand("update articles set readers=readers+1 where id=@id", conn);
            cmd.Parameters.AddWithValue("@id",id);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return text1 + reads.ToString() + "</span></div><img src=\"/imgs/trans.gif\" style='width:150px;height:70px;'/></div></td></tr></table>" + text3;
        }
    }


    public class Cat_Content 
    {
        public Cat_Content(int catid, int pageid, int myyear, int mymonth, int myday) 
        {
            string bigtitle = "";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["newislammemoConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("select id,name,parentid,description,notes,url_name,url_full_path from categories where id=@id", conn);
            cmd.Parameters.AddWithValue("@id", catid);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (!dr.Read())
            {
                valid = false;
                dr.Close();
                conn.Close();
                return;
            }
            else
            {
                valid = true;
                description=dr["description"].ToString();
                keywords = dr["notes"].ToString();
                name = dr["name"].ToString()+": ";
                fullUrl = dr["url_full_path"].ToString();

            }
            dr.Close();

            cat_pager mycat_pager = new cat_pager();

            cmd = new SqlCommand("category_get_arts", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@page_no", pageid);
            cmd.Parameters.AddWithValue("@catid", catid);
            DateTime dt1;
            DateTime dt2;
            if (myyear > 0)
            {
                mycat_pager.Year = myyear.ToString("D4");
                dt1 = new DateTime(myyear, mymonth < 1 ? 1 : mymonth, myday < 1 ? 1 : myday);
                dt2 = dt1.AddYears(1);
                if (mymonth > 0)
                {
                    mycat_pager.Month = mymonth.ToString("D2");
                    dt2 = dt1.AddMonths(1);
                    if (myday > 0)
                    {
                        mycat_pager.Day = myday.ToString("D2");
                        dt2 = dt1.AddDays(1);
                    }
                }
                cmd.Parameters.AddWithValue("@date_min", dt1);
                cmd.Parameters.AddWithValue("@date_max", dt2);
            }

            SqlParameter retValParam = new SqlParameter("@RETURN_VALUE", SqlDbType.Int);
            retValParam.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(retValParam);

            dr = cmd.ExecuteReader();
            

            StringBuilder sb = new StringBuilder();
            sb.Append("<span id='wwww' style='display:none'>"+DateTime.Now.ToString()+"</span>");
            //navigation
            sb.Append("<div class='nolinks' style=\"margin-top:5px;color:#00517a;padding:2px 2px 2px 2px;direction:rtl;\" ><a href='/default.aspx'>������ �������</a>" + memo.design.get_cat_nav_menu(catid) + "</div>");
            //akher eledafat
            sb.Append("<table border='0' dir='rtl' width='100%' cellpadding='0' style='border-collapse: collapse'><tr><td width='117' class='cat_recent_logo'></td><td id='slidetitle' class='cat_recent_title'></td><td class='cat_recent_title_corner'></td></tr></table>");

            StringBuilder sb2 = new StringBuilder();
            while (dr.Read())
            {
                bigtitle += dr["title"].ToString() + ", ";
                sb2.Append("<div class='cat_recent_content' style='text-align:right;'><table width='100%' dir='rtl'><tr><td width='190' rowspan='2'><img src='");
                if (string.IsNullOrEmpty(dr["image_url"].ToString()))
                {
                    sb2.Append(ConfigurationManager.AppSettings["image_server"] + "media/noimage.jpg");
                }
                else
                {
                    sb2.Append(ConfigurationManager.AppSettings["image_server"] + dr["image_url"].ToString());
                }
                sb2.Append("' width='180' height='143' style='border: 1px solid #FFFFFF;' /></td><td class='tab_main01' >");
                sb2.Append(string.Format("<a href='{0}'>{1}</a>", dr["url"], dr["title"]));
                sb2.Append("</td></tr><tr><td class='cat_recent_title' style='background-image:none;'>");
                //sb.Append("<span style='display:none;'>11" + dr["image_url"].ToString() + "</span>");
                sb2.Append(dr["apprev"].ToString());
                sb2.Append("</td></tr></table><br /></div>");
            }
            if (dr.NextResult())
            {
                bigtitle = "";
                while (dr.Read())
                {
                    bigtitle += dr["title"].ToString() + ", ";
                    sb.Append("<div class='cat_recent_content' style='text-align:right;'><table width='100%' dir='rtl'><tr><td width='190' rowspan='2'><img src='");
                    if (string.IsNullOrEmpty(dr["image_url"].ToString()))
                    {
                        sb.Append(ConfigurationManager.AppSettings["image_server"] + "media/noimage.jpg");
                    }
                    else
                    {
                        sb.Append(ConfigurationManager.AppSettings["image_server"] + dr["image_url"].ToString());
                    }
                    sb.Append("' width='180' height='143' style='border: 1px solid #FFFFFF;' /></td><td class='tab_main01' >");
                    sb.Append(string.Format("<a href='{0}'>{1}</a>", dr["url"], dr["title"]));
                    sb.Append("</td></tr><tr><td class='cat_recent_title' style='background-image:none;'>");
                    //sb.Append("<span style='display:none;'>22" + dr["image_url"].ToString() + "</span>");
                    sb.Append(dr["apprev"].ToString());
                    sb.Append("</td></tr></table><br /></div>");
                }
            }
            else
            {
                sb.Append(sb2.ToString());
            }
            dr.Close();
            name += bigtitle;
            if (retValParam.Value!=null)
            {
                mycat_pager.Rowcount = int.Parse(retValParam.Value.ToString());                
            }
            mycat_pager.Pagesize = 10;
            mycat_pager.Currentpage = pageid;
            mycat_pager.Catname = fullUrl;
            
            conn.Close();

            sb.Append("<table border='1' width='100%' cellpadding='0' style='border-collapse: collapse' class='cat_vcr' cellspacing='0'><tr><td align='center' dir='rtl' class='main03'>");
            sb.Append(mycat_pager.getHTML());
            sb.Append("</td></tr></table>");

            html = sb.ToString();
            lastPagesCount = mycat_pager.MaxPagesNo;
        }
        string html;

        public string Html
        {
            get 
            {
                return html; 
            }
        }

        bool valid;

        public bool Valid
        {
            get { return valid; }
            set { valid = value; }
        }
        
        string description;

        public string Description
        {
            get { return description; }
        }

        string keywords;

        public string Keywords
        {
            get { return keywords; }
        }

        string name;

        public string Name
        {
            get { return name; }
        }

        string fullUrl;

        public string FullUrl
        {
            get { return fullUrl; }
        }

        int lastPagesCount;

        public int LastPagesCount
        {
            get { return lastPagesCount; }
        }
    }


    public class cat_pager
    {
        int rowcount = 500;
        int pagesize = 10;
        int currentpage = 0;


        int pagecount;
        int slidersize = 15;
        string QueryStringKey = "start";
        string NextText = " ������ ";
        string LastText = "������";
        string FirstText = "������";
        string BackText = " ������ ";
        // HideOnSinglePage
        int MaxPages = 10;

        public int MaxPagesNo
        {
            get { return MaxPages; }
        }
        string catname = "";
        string year = "";

        public string Year
        {
            get { return year; }
            set { year = value; }
        }
        string month = "";

        public string Month
        {
            get { return month; }
            set { month = value; }
        }
        string day = "";

        public string Day
        {
            get { return day; }
            set { day = value; }
        }

        string link_prefix = "";


        public string getHTML()
        {
            MaxPages = (int)decimal.Ceiling(decimal.Divide((decimal)rowcount, (decimal)pagesize));
            if (currentpage > MaxPages || currentpage == 0)
            {
                currentpage = MaxPages;
            }

            pagecount = MaxPages;


            if (pagecount < 2)
            {
                return "";
            }
            else
            {
                link_prefix = "";
                if (!string.IsNullOrEmpty(catname))
                {
                    link_prefix += catname + "/";
                }
                if (!string.IsNullOrEmpty(year))
                {
                    link_prefix += year + "/";
                    if (!string.IsNullOrEmpty(month))
                    {
                        link_prefix += month + "/";
                        if (!string.IsNullOrEmpty(day))
                        {
                            link_prefix += day + "/";
                        }
                    }
                }
                return BuildLinkNavBack() + BuildPageNumbers() + BuildLinkNavNext();
            }
        }


        string BuildPageNumbers()
        {

            StringBuilder sbPagesNumbers = new StringBuilder();

            int StartPage = 1;

            //End Page
            int EndPage = 0;
            if (pagecount > MaxPages)
            {
                EndPage = MaxPages;
            }
            else
            {
                EndPage = pagecount;
            }

            //Adjust for slider
            if (true)//if (UseSlider)
            {
                int Half = (int)Math.Floor((double)(slidersize - 1) / 2);
                //8: Half = 3
                //7: Half = 3

                int NumAbove = currentpage + Half + ((slidersize - 1) % 2);
                int NumBelow = currentpage - Half;

                if (NumBelow < 1)
                {
                    NumAbove += (1 - NumBelow);
                    NumBelow = 1;
                }

                if (NumAbove > EndPage)
                {
                    NumBelow -= (NumAbove - EndPage);
                    if (NumBelow < 1)
                    {
                        NumBelow = 1;
                    }
                    NumAbove = EndPage;
                }

                StartPage = NumBelow;
                EndPage = NumAbove;
            }

            for (int i = StartPage; i <= EndPage; i++)
            {
                int RecordStart;
                int RecordEnd;


                if (true)
                {
                    //For Runtime.



                    if (i > StartPage)
                    {
                        sbPagesNumbers.Append("-");// PageNumbersSeparator
                    }

                    if (i == currentpage)
                    {
                        sbPagesNumbers.AppendFormat("<span class='tab_main01'><B>{0}</B></span>", (i).ToString());
                    }
                    else
                    {
                        if (i==MaxPages)
                        {
                            sbPagesNumbers.AppendFormat("<a href='{0}'>{1}</a>"
                                , link_prefix 
                                , i);
                        }
                        else
                        {
                            sbPagesNumbers.AppendFormat("<a href='{0}'>{1}</a>"
                                , link_prefix + "page/" + i.ToString()
                                , i);
                        }
                    }
                }
            }
            return sbPagesNumbers.ToString();
        }

        string BuildLinkNavNext()
        {
            StringBuilder LinkNav = new StringBuilder();


            if (currentpage >= MaxPages || currentpage == pagecount)
            {

            }
            else
            {
                if (currentpage + 1 >= MaxPages)
                {
                    LinkNav.AppendFormat("<a href='{0}'>{1}</A>"
                        , link_prefix , NextText);
                }
                else
                {
                    LinkNav.AppendFormat("<a href='{0}'>{1}</A>"
                        , link_prefix + "page/" + (currentpage + 1).ToString()
                        , NextText);
                }
                if (true)//(ShowFirstLast)
                {
                    LinkNav.AppendFormat("&nbsp;<a href='{0}'>{1}</A>"
                        , link_prefix , LastText);
                }
            }

            return LinkNav.ToString();
        }

        string BuildLinkNavBack()
        {
            StringBuilder LinkNav = new StringBuilder();

            //Next Links
            //if (_PagedDataSet.IsFirstPage || _PagedDataSet.DataSourceCount == 0)
            if (currentpage < 2)
            {
                //if (true)
                //{
                //    LinkNav.Append(FirstText + "&nbsp;");
                //}
                //LinkNav.Append(BackText);
            }
            else
            {
                //Set up PostBack link.

                if (true)
                {
                    LinkNav.AppendFormat("<a href='{0}page/1'>{1}</A>&nbsp;"
                        , link_prefix
                        , FirstText);
                }
                LinkNav.AppendFormat("<a href='{0}'>{1}</A>"
                    , link_prefix + "page/" + (currentpage - 1).ToString()
                    , BackText);

            }

            return LinkNav.ToString();
        }


        public int Rowcount
        {
            get { return rowcount; }
            set { rowcount = value; }
        }

        public int Pagesize
        {
            get { return pagesize; }
            set { pagesize = value; }
        }

        public int Currentpage
        {
            get { return currentpage; }
            set { currentpage = value; }
        }

        public string Catname
        {
            get { return catname; }
            set { catname = value; }
        }

    }

    public class ArtImage
    {
        string path;
        string discription;
        string name;
    }

    public class ArticleLite 
    {
        int id;
        string title;
        string image_block;
        string body_block;

    }

}

