﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using memov5entity.Models;
namespace memov5entity.Models
{
    public class edaraviewmodel
    {
        //islammemoDataContext dt = new islammemoDataContext();
        test4Entities dt = new test4Entities();
        public virtual string get_articles(string cat_url)
        {
            StringBuilder sb = new StringBuilder();
            int cat_id = (from p in dt.Categories
                          where p.url_name == cat_url
                          select p.ID).FirstOrDefault();
            var top_4_articles = (from p in dt.Articles
                                  join p2 in dt.article_cats
                                  on p.ID equals p2.ArtID
                                  where p.stat == 8 && p2.CatID == cat_id
                                  orderby p.Date descending
                                  select new { p.Title, p.Apprev, p.image_url, p.url }).Take(4).ToList();

            sb.Append(top_4_articles[0].Title);
            sb.Append("*");
            sb.Append(top_4_articles[0].Apprev);
            sb.Append("*");
            //string img_url = top_4_articles[0].image_url.ToString();
            if (top_4_articles[0].image_url == null)
            {
                sb.Append(images_server.name + "media/noimage.jpg");
            }
            else
            {
                string firstartimageurl = top_4_articles[0].image_url;
                if (firstartimageurl.Contains("_new_"))
                {
                    firstartimageurl = firstartimageurl.Split('.')[0] + "_83_82_" + "." + firstartimageurl.Split('.')[1];
                }
                sb.Append(images_server.name);
                sb.Append(firstartimageurl);
            }
            sb.Append("*");
            sb.Append(top_4_articles[0].url);
            sb.Append("*");
            sb.Append("|");
            foreach (var art in top_4_articles)
            {
                sb.Append(string.Format("<a target='_blank' href='{0}'>{1}</a>", art.url, art.Title));
                sb.Append("*");
                // sb.Append(string.Format("<a href='{0}' target='_blank'>{1}</a>", ad.ad_link, ad.ad));
            }
            return sb.ToString();


        }

        public virtual List<Article> get_articles_v4(string cat_url)
        {
            StringBuilder sb = new StringBuilder();
            int cat_id = (from p in dt.Categories
                          where p.url_name == cat_url
                          select p.ID).FirstOrDefault();
            List<Article> top_4_articles = (from p in dt.Articles
                                            join p2 in dt.article_cats
                                            on p.ID equals p2.ArtID
                                            where p.stat == 8 && p2.CatID == cat_id
                                            orderby p.Date descending
                                            select p).Take(3).ToList();
            return top_4_articles;
            //sb.Append(top_4_articles[0].Title);
            //sb.Append("*");
            //sb.Append(top_4_articles[0].Apprev);
            //sb.Append("*");
            //if (top_4_articles[0].image_url == null)
            //{
            //    sb.Append(images_server.name + "media/noimage.jpg");
            //}
            //else
            //{
            //    string firstartimageurl = top_4_articles[0].image_url;
            //    if (firstartimageurl.Contains("_new_"))
            //    {
            //        firstartimageurl = firstartimageurl.Split('.')[0] + "_83_82_" + "." + firstartimageurl.Split('.')[1];
            //    }
            //    sb.Append(images_server.name);
            //    sb.Append(firstartimageurl);
            //}
            //sb.Append("*");
            //sb.Append(top_4_articles[0].url);
            //sb.Append("*");
            //sb.Append("|");
            //foreach (var art in top_4_articles)
            //{
            //    sb.Append(string.Format("<a target='_blank' href='{0}'>{1}</a>", art.url, art.Title));
            //    sb.Append("|");
            //}
            //return sb.ToString();
        }
        public virtual string getlongdate()
        {
            // SELECT TOP (1) Articles.LongDate FROM article_cats INNER JOIN Articles ON article_cats.ArtID = Articles.ID WHERE (article_cats.CatID = 621) and (stat=8) and (date<getdate()) order by date desc
            return (from p in dt.Articles
                    join p2 in dt.article_cats
                    on p.ID equals p2.ArtID
                    where p2.CatID == 605 &&
                    p.stat == 8 &&
                    p.Date < DateTime.Now
                    orderby p.Date descending
                    select p).FirstOrDefault().LongDate;

        }

        public virtual string explore_important_news()
        {
            StringBuilder sb = new StringBuilder();
            var news = from p in dt.marque_article 
                       join p2 in dt.Articles
                       on p.article_id equals p2.ID
                       where p.cat_id == 1
                       orderby p.rank
                       select new { p2.Title, p2.ID, p2.url };





            foreach (var art in news)
            {
                sb.Append(string.Format("<a target='_blank' href='{0}'>{1}</a>", art.url, art.Title));

                sb.Append("*");
            }

            return sb.ToString();


        }
    }
}