﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using memov5entity.Models;
namespace memov5entity.Models
{
    public class imageviwmodel
    {
        //  islammemoDataContext dt = new islammemoDataContext();
        test4Entities dt = new test4Entities();
        int id;
        string art_url;
        public string Art_url
        {
            get { return art_url; }
            set { art_url = value; }
        }

        public int Id
        {

            get { return id; }
            set { id = value; }
        }
        Article a;

        public Article A
        {
            get { return a; }
            set { a = value; }
        }
        public imageviwmodel() { }
        public imageviwmodel(int? year, int? month, int? day, int? id)
        {

            this.id = Convert.ToInt32(id);
            a = (from p in dt.Articles
                 where p.ID == this.id
                 select p).FirstOrDefault();
        }
        public String rel_images()
        {
            var ims = a.images_cat_images;
            foreach (var im in ims)
            {

            }
            return "";
        }
        public images_cat_images mainimage()
        {
            images_cat_images im = (from p in dt.images_cat_images
                                    where p.article_id == a.ID && p.selected == true
                                    select p).FirstOrDefault();
            if (im == null)
            {
                im = (from p in dt.images_cat_images
                      where p.article_id == a.ID
                      select p).FirstOrDefault();
            }
            return im;

        }
        public virtual List<comment> getcomments()
        {
            List<comment> comments = (from p in dt.comments
                                      where p.article_id == a.ID && p.allow == true
                                      select p).ToList();
            //  dt.comments.Where(p => p.article_id == a.ID).ToList();
            return comments;
        }
        public string explore_important_news()
        {
            StringBuilder sb = new StringBuilder();



            var news = from p in dt.marque_article
                       join p2 in dt.Articles
                       on p.article_id equals p2.ID
                       where p.cat_id == 1
                       orderby p.rank
                       select new { p2.Title, p2.ID, p2.url };





            foreach (var art in news)
            {
                sb.Append(string.Format("<a target='_blank' href='{0}'>{1}</a>", art.url, art.Title));

                sb.Append("*");
            }

            return sb.ToString();


        }
        public bool insertcomment(string Title, string Body, string SName, int article_id, string SEmail)
        {
            try
            {
                comment c2 = new comment();
                c2.username = SName;
                c2.cdate = DateTime.Now;
                c2.body = Body.Replace("\n", "<br>");
                c2.email = SEmail;
                c2.allow = false;
                c2.article_id = article_id;
                c2.title = Title;
                dt.AddTocomments(c2);
                dt.SaveChanges();
                // dt.comments.InsertOnSubmit(c2);
                // dt.SubmitChanges();

                return true;
            }
            catch
            {
                return false;
            }

        }
        //****************************memo_mobil******************************//
        public virtual List<Article> getrelatedarticles_mobil()
        {
            List<article_related> related = a.getallrelatedarticles(3);

            List<Article> articles = new List<Article>();
            int count = 0;
            foreach (article_related r in related)
            {
                count++;
                if (count == 3) break;
                Article art = dt.Articles.Where(p => p.ID == r.related_article_id).FirstOrDefault();
                articles.Add(art);
            }
            return articles;
        }
    }
}