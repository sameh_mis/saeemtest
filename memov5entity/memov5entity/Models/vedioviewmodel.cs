﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using memov5entity.Models;
namespace memov5entity.Models
{
    public class vedioviewmodel
    {

        test4Entities dt = new test4Entities();
        int id;
        string art_url;
        public virtual string Art_url
        {
            get { return art_url; }
            set { art_url = value; }
        }

        public virtual int Id
        {

            get { return id; }
            set { id = value; }
        }
        Article a;

        public virtual Article A
        {
            get { return a; }
            set { a = value; }
        }
        public vedioviewmodel()
        { }
        public vedioviewmodel(int? year, int? month, int? day, int? id)
        {
            this.id = Convert.ToInt32(id);
            a = (from p in dt.Articles
                 where p.ID == this.id
                 select p).First();

        }
        public virtual List<comment> getcomments()
        {
            List<comment> comments = (from p in dt.comments
                                      where p.article_id == a.ID && p.allow == true
                                      select p).ToList();
            //  dt.comments.Where(p => p.article_id == a.ID).ToList();
            return comments;
        }
        public virtual List<Category> parentcategories()
        {
            //int cat_id =Convert.ToInt32((from p in dt.Articles
            //              where p.ID == a.ID
            //              select p).FirstOrDefault().cat_id);
            Category c = dt.Categories.Where(p => p.ID == a.cat_id).FirstOrDefault();
            Category parent = null; //dt.Categories.Where(p => p.ID == c.ParentID).FirstOrDefault();
            int pid = 0;
            if (c != null)
            {
                pid = c.ParentID;
            }
            while (pid != 0)
            {
                parent = dt.Categories.Where(p => p.ID == pid).FirstOrDefault();
                try
                {
                    pid = parent.ParentID;
                }
                catch
                {
                    pid = 0;
                }
            }
            List<Category> parents = new List<Category>();
            if (c != null)
                parents.Add(c);
            if (parent != null)
                parents.Add(parent);
            return parents;
        }
        public virtual List<Article> getrelatedarticles()
        {
            List<article_related> related = a.getallrelatedarticles(9);

            List<Article> articles = new List<Article>();
            int count = 0;
            foreach (article_related r in related)
            {
                count++;
                if (count == 9) break;
                Article art = dt.Articles.Where(p => p.ID == r.related_article_id).FirstOrDefault();
                articles.Add(art);
            }
            return articles;
        }
        public bool insertcomment(string Title, string Body, string SName, int article_id, string SEmail)
        {
            try
            {
                comment c2 = new comment();
                c2.username = SName;
                c2.cdate = DateTime.Now;
                c2.body = Body.Replace("\n", "<br>");
                c2.email = SEmail;
                c2.allow = false;
                c2.article_id = article_id;
                c2.title = Title;
                dt.AddTocomments(c2);
                dt.SaveChanges();
                // dt.comments.InsertOnSubmit(c2);
                // dt.SubmitChanges();

                return true;
            }
            catch
            {
                return false;
            }

        }
        //***************************memo_mobil********************************//
        public virtual List<Article> getrelatedarticles_mobil()
        {
            List<article_related> related = a.getallrelatedarticles(3);

            List<Article> articles = new List<Article>();
            int count = 0;
            foreach (article_related r in related)
            {
                count++;
                if (count == 3) break;
                Article art = dt.Articles.Where(p => p.ID == r.related_article_id).FirstOrDefault();
                articles.Add(art);
            }
            return articles;
        }
    }
}