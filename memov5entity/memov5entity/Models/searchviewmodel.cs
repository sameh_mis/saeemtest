﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Globalization;
using System.Collections;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
namespace memov5entity.Models
{
    public class searchviewmodel
    {
        DataTable ds = new DataTable();
        CultureInfo ci = new CultureInfo("ar-SA");

        public DataTable Ds
        {
            get { return ds; }
            set { ds = value; }
        }
        List<string> mywords = new List<string>();
        bool search_title = false;
        bool search_body = false;
        string safe_cats_id = "";
        bool sortbyscore = false;
        int intmysearchmode = 0;
        searchmode mysearchmode;
        public searchviewmodel()
        {

        }
        public searchviewmodel(string aaa, int? page, string chk_1, int? D1, string chk_2, int? D2, string C12, string hdn_cats)
        {
            if (chk_1 != null)
            {
                if (chk_1.ToLower() == "on")
                {
                    search_title = true;
                }
            }
            if (chk_2 != null)
            {
                if (chk_2.ToLower() == "on")
                {
                    search_body = true;
                }
            }

            if (!search_body && !search_title)
            {
                search_body = true;
            }

            if (D1 != null && D1.ToString() == "1")
            {
                sortbyscore = true;
            }
            int.TryParse(D2.ToString(), out intmysearchmode);
            switch (intmysearchmode)
            {
                case 1:
                    mysearchmode = searchmode.anywords;
                    break;
                case 2:
                    mysearchmode = searchmode.exactphares;
                    break;
                default:
                    mysearchmode = searchmode.allwords;
                    break;
            }
            if (hdn_cats != null)
            {
                string[] mystrcats = hdn_cats.Split(',');

                int mytempint = 0;
                foreach (string ss in mystrcats)
                {
                    int.TryParse(ss, out mytempint);
                    if (mytempint > 0)
                    {
                        safe_cats_id += mytempint.ToString() + ",";
                    }
                }
            }
            StringBuilder sb = new StringBuilder();


            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["test4ConnectionString"].ConnectionString);
            SqlCommand cmd;
            SqlDataAdapter da;
            if (safe_cats_id.Length > 0 && C12 == null)
            {
                cmd = new SqlCommand(article_query_fulltext(aaa.Replace("\"", " "), mysearchmode, search_title, search_body, D1.ToString(), ref mywords, safe_cats_id.Substring(0, safe_cats_id.Length - 1), sortbyscore), conn);
                da = new SqlDataAdapter(article_query_fulltext(aaa.Replace("\"", " "), mysearchmode, search_title, search_body, D1.ToString(), ref mywords, safe_cats_id.Substring(0, safe_cats_id.Length - 1), sortbyscore), conn);
                //mytext.logevent(da.SelectCommand.CommandText);

            }
            else
            {

                cmd = new SqlCommand(article_query_fulltext(aaa.Replace("\"", " "), mysearchmode, search_title, search_body, D1.ToString(), ref mywords, null, sortbyscore), conn);
                da = new SqlDataAdapter(article_query_fulltext(aaa.Replace("\"", " "), mysearchmode, search_title, search_body, D1.ToString(), ref mywords, null, sortbyscore), conn);
                //mytext.logevent(da.SelectCommand.CommandText);
            }


            da.Fill(ds);


        }
        public string tempfunc2(string ss)
        {
            DateTime dt;
            try
            {
                dt = DateTime.Parse(ss);
            }
            catch
            {

                dt = DateTime.Now;
            }

            return dt.ToString("D", ci);
        }

        public string article_query_fulltext(string searchstring, searchmode mode, bool btitle, bool bbody, string strcatid, ref System.Collections.Generic.List<string> outsearchwords, string strcatss, bool score)
        {
            string columnames = "";
            StringBuilder sbwords = new StringBuilder();
            int myintcatid;
            int.TryParse(strcatid, out myintcatid);
            if (btitle)
            {
                columnames = "(title,apprev)";
            }
            if (bbody)
            {
                columnames = "(body,apprev)";
            }
            if (btitle && bbody)
            {
                columnames = "(body,title,apprev)";
            }


            searchstring = searchstring.Trim().Replace('*', ' ').Replace('-', ' ').Replace('/', ' ');
            string searchstring2 = searchstring;
            StringBuilder query = new StringBuilder();
            Queue searchwords = new Queue();
            int wordsqueuelenght = 0;
            int num = 0;//for the while loop of extracting words from the string
            int i = 0;

            while (searchstring.IndexOf(" ", 0) > -1)
            {
                num = searchstring.IndexOf(" ", 0);
                //				MessageBox.Show(str.Substring(0,num+1));
                searchwords.Enqueue(searchstring.Substring(0, num + 1));
                searchstring = searchstring.Substring(num + 1);
                searchstring = searchstring.Trim();
                i++;
                wordsqueuelenght++;
            }
            //			MessageBox.Show(str);
            searchwords.Enqueue(searchstring);
            wordsqueuelenght++;


            if (mode == searchmode.allwords)
            {
                sbwords.Append("' ");
                for (int i2 = 0; i2 < wordsqueuelenght; i2++)
                {
                    string word = searchwords.Dequeue().ToString().Trim();
                    outsearchwords.Add(word);
                    try
                    {
                        if (i2 > 0)
                        {
                            sbwords.Append(" and ");
                        }
                        sbwords.Append("\"");
                        sbwords.Append(word);
                        sbwords.Append("\"");
                        //query.Append("OR body like '%");
                        //query.Append(word);
                        //query.Append("%' )");
                    }
                    catch { }
                }
                sbwords.Append(" '");

            }
            else if (mode == searchmode.anywords)
            {
                sbwords.Append("' ");
                for (int i2 = 0; i2 < wordsqueuelenght; i2++)
                {
                    string word = searchwords.Dequeue().ToString().Trim();
                    outsearchwords.Add(word);
                    try
                    {
                        if (i2 > 0)
                        {
                            sbwords.Append(" or ");
                        }
                        sbwords.Append("\"");
                        sbwords.Append(word);
                        sbwords.Append("\"");
                        //query.Append("OR body like '%");
                        //query.Append(word);
                        //query.Append("%' )");
                    }
                    catch { }
                }
                sbwords.Append(" '");
            }
            else if (mode == searchmode.exactphares)
            {
                sbwords.Append("' \" ");
                sbwords.Append(searchstring2);

                sbwords.Append(" \" '");
            }

            if (myintcatid > 0)
            {
                //query.Append(string.Format("SELECT top 50 FT_TBL.id, FT_TBL.title,FT_TBL.apprev ,KEY_TBL.RANK FROM articles AS FT_TBL INNER JOIN CONTAINSTABLE(articles,{0},{1}) AS KEY_TBL ON FT_TBL.id = KEY_TBL.[KEY] INNER JOIN article_cats ON FT_TBL.ID = article_cats.ArtID where (article_cats.catid={2}) and (stat=8) ORDER BY KEY_TBL.RANK DESC", columnames, sbwords.ToString(), myintcatid.ToString()));
                query.Append(string.Format("SELECT top 250 FT_TBL.id, FT_TBL.title,FT_TBL.apprev,FT_TBL.date ,KEY_TBL.RANK,max(KEY_TBL.RANK) as maxrank,FT_TBL.url FROM articles AS FT_TBL INNER JOIN CONTAINSTABLE(articles,{0},{1}) AS KEY_TBL ON FT_TBL.id = KEY_TBL.[KEY] INNER JOIN article_cats ON FT_TBL.ID = article_cats.ArtID where (article_cats.catid={2}) and (stat=8) group by key_TBL.RANK ,date,apprev,title,id,url ", columnames, sbwords.ToString(), myintcatid.ToString()));
            }
            else if (strcatss != null)
            {
                query.Append(string.Format("SELECT top 250 FT_TBL.id, FT_TBL.title,FT_TBL.apprev,FT_TBL.date ,KEY_TBL.RANK,max(KEY_TBL.RANK) as maxrank,FT_TBL.url FROM articles AS FT_TBL INNER JOIN CONTAINSTABLE(articles,{0},{1}) AS KEY_TBL ON FT_TBL.id = KEY_TBL.[KEY] INNER JOIN article_cats ON FT_TBL.ID = article_cats.ArtID where (article_cats.catid in ({2})) and (stat=8) group by key_TBL.RANK ,date,apprev,title,id,url ", columnames, sbwords.ToString(), strcatss));
            }
            else
            {
                //query.Append(string.Format("SELECT top 50 FT_TBL.id, FT_TBL.title ,FT_TBL.apprev, KEY_TBL.RANK FROM articles AS FT_TBL INNER JOIN CONTAINSTABLE(articles,{0},{1}) AS KEY_TBL ON FT_TBL.id = KEY_TBL.[KEY]  where (stat=8) ORDER BY KEY_TBL.RANK DESC", columnames, sbwords.ToString()));
                query.Append(string.Format("SELECT top 250 FT_TBL.id, FT_TBL.title ,FT_TBL.apprev,FT_TBL.date, KEY_TBL.RANK ,max(KEY_TBL.RANK) as maxrank,FT_TBL.url FROM articles AS FT_TBL INNER JOIN CONTAINSTABLE(articles,{0},{1}) AS KEY_TBL ON FT_TBL.id = KEY_TBL.[KEY]  where (stat=8) group by key_TBL.RANK ,date,apprev,title,id,url ", columnames, sbwords.ToString()));

            }


            if (!score)
            {
                query.Append(" ORDER BY FT_TBL.date DESC");
            }
            else
            {
                query.Append(" ORDER BY KEY_TBL.RANK DESC");
            }
            return query.ToString();
        }

    }

    public enum searchmode
    {
        exactphares = 1,
        allwords = 2,
        anywords = 3
    }
    public enum search_oper
    {
        EQUAL,
        NOTEQUAL,
        CONTAINS,
        NOTCONTAINS,
        GREATER,
        LESS
    }

}