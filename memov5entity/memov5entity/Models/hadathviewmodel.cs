﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace memov5entity.Models
{
    public class hadathviewmodel
    {
        test4Entities dt;
        public hadathviewmodel()
        {

            dt = new test4Entities();
        }
        int getcurrenteventid()
        {


            int maineventid = Convert.ToInt32((from p in dt.settings
                                               where p.varname == "hevent_id"
                                               select p).FirstOrDefault().value);

            return maineventid;
        }




        public string get_main_event_url()
        {
            int event_id = getcurrenteventid();
            string event_url = (from p in dt.Categories
                                where p.ID == event_id
                                select p.url_name + "*" + p.image_url).FirstOrDefault();
            if (string.IsNullOrEmpty(event_url) == false)
            {
                string[] arr = event_url.Split('*');
                string updatedimageurl = arr[1];
                return arr[0] + "*" + updatedimageurl;
            }
            else
            {
                event_url = (from p in dt.Categories
                             where p.ID == event_id
                             select p.url_name + "*" + "media/noimage" + "_691_157_." + "jpg").FirstOrDefault();
                return event_url;
            }

        }
        public string updateimageurl(string imageurl, string dimension)
        {
            if (imageurl.Contains("_new_"))
            {
                imageurl = imageurl.Split('.')[0] + dimension + "." + imageurl.Split('.')[1];
            }
            return imageurl;
        }
        public List<Article> get_main_event_articles()
        {

            int event_id = getcurrenteventid();
            List<Article> art_list = (from p in dt.Articles
                                      join p2 in dt.article_cats
                                      on p.ID equals p2.ArtID
                                      where p2.CatID == event_id && p.stat == 8
                                      orderby p.ID descending
                                      select p).Take(17).ToList();


            return art_list;


        }





        public string get_images_sever_name()
        {
            return images_server.name;
        }
        public string rewrite_url(string art_url, int? cat_id)
        {
            //if (s.StartsWith("/20")) { s = "/hadath-el-saa/" + related[a].cat_id  + s; }
            //if (art_url.StartsWith("/20"))
            //{
            //    art_url = "/hadath-el-saa/" + cat_id + art_url;
            //}
            return art_url;
        }
        public ArrayList gettopevents()
        {
            int event_id = getcurrenteventid();
            //select top 6 id ,name from categories where parentid=652 and id<>834 order by id desc
            var top9events = (from p in dt.Categories
                              where p.ParentID == 652 && p.ID != event_id
                              orderby p.ID descending
                              select new { id = p.ID, name = p.url_name, image_url = p.image_url, title = p.Name }).Take(2).ToList();
            ArrayList arr_id = new ArrayList();
            ArrayList arr_name = new ArrayList();
            ArrayList arr_image = new ArrayList();
            ArrayList titles = new ArrayList();
            // Hashtable h = new Hashtable();
            ArrayList arr = new ArrayList();
            foreach (var t in top9events)
            {
                //h.Add(t.id,t.name);
                //arr2.Add(t.name);
                arr_id.Add(t.id);
                arr_name.Add(t.name);
                titles.Add(t.title);
                if (string.IsNullOrEmpty(t.image_url))
                {
                    arr_image.Add("media/noimage.jpg");
                }
                else
                {
                    string updatedimageurl = t.image_url;
                    if (updatedimageurl.Contains("_new_"))
                    {
                        updatedimageurl = updatedimageurl.Split('.')[0] + "_223_59_." + updatedimageurl.Split('.')[1];
                    }
                    arr_image.Add(updatedimageurl);
                }
            }
            arr.Add(arr_id);
            arr.Add(arr_name);
            arr.Add(arr_image);
            arr.Add(titles);
            return arr;


        }
        public ArrayList gettopevents(int events_no)
        {
            int event_id = getcurrenteventid();
            //select top 6 id ,name from categories where parentid=652 and id<>834 order by id desc
            var topevents = (from p in dt.Categories
                             where p.ParentID == 652 && p.ID != event_id
                             orderby p.ID descending
                             select new { id = p.ID, name = p.url_name }).Take(events_no).ToList();
            ArrayList arr_id = new ArrayList();
            ArrayList arr_name = new ArrayList();
            // Hashtable h = new Hashtable();
            ArrayList arr = new ArrayList();
            foreach (var t in topevents)
            {
                //h.Add(t.id,t.name);
                //arr2.Add(t.name);
                arr_id.Add(t.id);
                arr_name.Add(t.name);
            }
            arr.Add(arr_id);
            arr.Add(arr_name);
            return arr;


        }
        public int getcategoriescount()
        {
            int count = (from p in dt.Categories
                         where p.ParentID == 652
                         orderby p.ID descending
                         select p.ID).Count();

            int pages_count = count / 12;
            if (pages_count % count > 0)
                pages_count++;

            return pages_count;

        }
        public ArrayList gettopevents(int events_no, int page)
        {
            int skippingno = 0;
            int pages_count = getcategoriescount();
            if (page < pages_count && page > 0)
            {
                skippingno = pages_count - page;
            }



            if (page == 1)
            {
                var topevents = (from p in dt.Categories
                                 where p.ParentID == 652
                                 orderby p.ID ascending
                                 select new { id = p.ID, name = p.url_name, image_url = p.image_url, title = p.Name }).Take(events_no).ToList();

                ArrayList arr_id = new ArrayList();
                ArrayList arr_name = new ArrayList();
                ArrayList arr_image = new ArrayList();
                ArrayList titles = new ArrayList();
                // Hashtable h = new Hashtable();
                ArrayList arr = new ArrayList();
                foreach (var t in topevents)
                {
                    //h.Add(t.id,t.name);
                    //arr2.Add(t.name);
                    arr_id.Add(t.id);
                    arr_name.Add(t.name);
                    titles.Add(t.title);
                    if (string.IsNullOrEmpty(t.image_url))
                    {
                        arr_image.Add(images_server.name + "media/noimage.jpg");
                    }
                    else
                    {
                        //foreach (string img in arr_images)
                        //{
                        //    if (img.Contains("_new_"))
                        //    {
                        //        string[] imgarr = img.Split('.');
                        //        img = imgarr[0] + "_223_59_." + imgarr[1];
                        //    }
                        //}
                        string imgupdatedurl = t.image_url;
                        if (imgupdatedurl.Contains("_new_"))
                        {
                            imgupdatedurl = imgupdatedurl.Split('.')[0] + "_223_59_." + imgupdatedurl.Split('.')[1];
                        }
                        arr_image.Add(images_server.name + imgupdatedurl);
                    }
                }
                arr.Add(arr_id);
                arr.Add(arr_name);
                arr.Add(arr_image);
                arr.Add(titles);
                return arr;

            }
            else
            {
                var topevents = (from p in dt.Categories
                                 where p.ParentID == 652
                                 orderby p.ID descending
                                 select new { id = p.ID, name = p.url_name, image_url = p.image_url, title = p.Name }).Skip(skippingno * events_no).Take(events_no).ToList();

                ArrayList arr_id = new ArrayList();
                ArrayList arr_name = new ArrayList();
                ArrayList arr_image = new ArrayList();
                ArrayList titles = new ArrayList();
                // Hashtable h = new Hashtable();
                ArrayList arr = new ArrayList();
                foreach (var t in topevents)
                {
                    //h.Add(t.id,t.name);
                    //arr2.Add(t.name);
                    arr_id.Add(t.id);
                    arr_name.Add(t.name);
                    titles.Add(t.title);
                    if (string.IsNullOrEmpty(t.image_url))
                    {
                        arr_image.Add(images_server.name + "media/noimage.jpg");
                    }
                    else
                    {
                        string imgupdatedurl = t.image_url;
                        if (imgupdatedurl.Contains("_new_"))
                        {
                            imgupdatedurl = imgupdatedurl.Split('.')[0] + "_223_59_." + imgupdatedurl.Split('.')[1];
                        }
                        arr_image.Add(images_server.name + imgupdatedurl);
                    }
                }
                arr.Add(arr_id);
                arr.Add(arr_name);
                arr.Add(arr_image);
                arr.Add(titles);
                return arr;
            }



        }
        public List<Article> othereventsarticles(int event_id)
        {
            List<Article> art = (from p in dt.Articles
                                 where p.cat_id == event_id && p.stat == 8
                                 orderby p.Date descending
                                 select p).Take(4).ToList();
            return art;

        }
        public List<Article> get_top_reading_articles()
        {
            List<Article> art = (from p in dt.Articles
                                 join p2 in dt.Categories
                                 on p.cat_id equals p2.ID
                                 where p2.ParentID == 652 && p.stat == 8
                                 orderby p.Readers descending
                                 select p).Take(6).ToList();
            return art;

        }
        public string getlongdate()
        {
            // SELECT TOP (1) Articles.LongDate FROM article_cats INNER JOIN Articles ON article_cats.ArtID = Articles.ID WHERE (article_cats.CatID = 621) and (stat=8) and (date<getdate()) order by date desc
            return (from p in dt.Articles
                    join p2 in dt.Categories
                    on p.cat_id equals p2.ID
                    where p2.ParentID == 652 &&
                    p.stat == 8 &&
                    p.Date < DateTime.Now
                    orderby p.Date descending
                    select p).FirstOrDefault().LongDate;

        }


        public List<Article> get_tkarer_articles()
        {
            int id = getcurrenteventid();
            List<int> akhbar_articles_ids = (from p in dt.Articles
                                             join p2 in dt.article_cats
                                             on p.ID equals p2.ArtID
                                             where p.cat_id == id && p2.CatID == 621 && p.stat == 8
                                             select p.ID).ToList();

            List<int> all_articles_ids = (from p in dt.Articles
                                          join p2 in dt.article_cats
                                          on p.ID equals p2.ArtID
                                          where p.cat_id == id && p.stat == 8
                                          select p.ID).Distinct().ToList();

            List<int> tkarer_articles_ids = new List<int>();
            List<Article> tkarer_articles = new List<Article>();



            foreach (int a in all_articles_ids)
            {
                bool c = false;
                foreach (int b in akhbar_articles_ids)
                {
                    if (a == b)
                    {
                        c = true;
                        break;
                    }
                }
                if (c == false)
                    tkarer_articles_ids.Add(a);

            }
            foreach (int a in tkarer_articles_ids)
            {
                //Response.Write(a + ".............");
                Article art = dt.Articles.Where(p => p.ID == a).FirstOrDefault();
                tkarer_articles.Add(art);
            }
            tkarer_articles = tkarer_articles.OrderBy(p => p.Date).Reverse().Take(7).ToList();
            return tkarer_articles;

        }
    }
}