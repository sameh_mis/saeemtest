﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Net;
using System.IO;
using System.Xml;
using System.Data;
using System.Text;
using System.Globalization;
using WorldDomination.Net;
using memov5entity.Models;
using System.Data.Objects.SqlClient;
using System.Numerics;
using System.Data.SqlClient;

namespace memov5entity.Models
{
    public class Homeviewmodel
    {
        /*
          public partial class Article
    {
       
        test4Entities dt = new test4Entities();
        public System.Collections.Generic.List<article_related> getallrelatedarticles()
        {
            System.Collections.Generic.List<article_related> related = (from p in dt.article_related
                                             where p.article_id == this.ID
                                             select p).ToList();
            return related;
        }
    }
          
          
         * */
        // Stopwatch stemp = new Stopwatch();
        //  islammemoDataContext dt = new islammemoDataContext();
        test4Entities dt = new test4Entities();

        IEnumerable<Article> important_articles;

        public Homeviewmodel()
        {


            dt.CommandTimeout = 120;
            dtmain = connection.getdatatable("SELECT  Articles.ID, Articles.Title, Articles.Apprev, Articles.image_url ,Articles.url,Articles.Date,article_cats.CatID as cid ,Articles.author_name,Articles.readers", "FROM article_cats INNER JOIN Articles ON article_cats.ArtID = Articles.ID  ", " WHERE  (Articles.stat = 8) AND (Articles.fixed_date > getdate()) AND (Articles.Date < getdate())", "ORDER BY Articles.rank");



            dtlast = connection.getdatatable("SELECT  * ", "FROM last_arts ", "", " order by date desc");
            //return dtmain;
        }
        public virtual ArrayList explorenews2(int catid, int newsno)
        {
            List<Article> news = getmainarticles(catid);
            List<Article> lstnews = getlastarticles(catid, newsno);
            foreach (Article a in lstnews)
            {
                if (news.Contains(a) == false)
                    news.Add(a);
            }
            news = news.Take(newsno).ToList();

            ArrayList arrid = new ArrayList();
            ArrayList arrtitle = new ArrayList();
            ArrayList arrapprev = new ArrayList();
            ArrayList arrurl = new ArrayList();
            ArrayList arrimgurl = new ArrayList();
            ArrayList arrdate = new ArrayList();
            ArrayList arr = new ArrayList();
            if (newsno > news.Count) newsno = news.Count;
            for (int a = 0; a < newsno; a++)
            {
                // if (string.IsNullOrEmpty(news[a].image_url)) news[a].image_url = "media/noimage.jpg";
                arrid.Add(news[a].ID);
                arrtitle.Add(news[a].Title);
                arrapprev.Add(news[a].Apprev);
                arrurl.Add(news[a].url);
                arrimgurl.Add(news[a].image_url);
                arrdate.Add(news[a].Date);
            }
            arr.Add(arrid);
            arr.Add(arrtitle);
            arr.Add(arrapprev);
            arr.Add(arrurl);
            arr.Add(arrimgurl);
            arr.Add(arrdate);
            return arr;
        }
        public virtual List<string> explore_choiced_articles()
        {
            List<string> selected_articles = (from p in dt.Articles
                                              where p.choice == true
                                              orderby p.ID descending
                                              select p.url + "|" + images_server.name + p.image_url + "|" + p.Title).Take(12).ToList();
            return selected_articles;
        }
        public string explore_main_advisement()
        {
            var news = (from p in dt.Articles
                        join p2 in dt.article_cats
                        on p.ID equals p2.ArtID
                        where p2.CatID == 721 &&
                        p.stat == 8 && p.Date < DateTime.Now
                        && p.fixed_date > DateTime.Now
                        orderby p.rank
                        select new { p.Title, p.url }).ToList();

            var last_articles = (from p in dt.Articles
                                 join p2 in dt.article_cats
                                 on p.ID equals p2.ArtID
                                 where p2.CatID == 721 &&
                                 p.stat == 8 && p.Date < DateTime.Now
                                 orderby p.Date descending
                                 select new { p.Title, p.url }).Take(4).ToList();
            // List<Article> news=getmainarticles(721);
            // List<Article> last_articles = getlastarticles(721, 4);

            foreach (var a in last_articles)
            {
                if (news.Contains(a) == false)
                    news.Add(a);
            }
            //int test = news.Count();
            news = news.Take(4).ToList();
            StringBuilder s = new StringBuilder();
            foreach (var a in news)
            {
                s.Append(string.Format("<a href='{0}' target='_blank'>{1}</a>", a.url, a.Title));
                s.Append("*");
            }
            return s.ToString();

        }
        public virtual string explore_main_vedios()
        {
            Article news = (from p in dt.Articles
                            join p2 in dt.article_cats
                            on p.ID equals p2.ArtID
                            where p2.CatID == 846 &&
                            p.stat == 8 && p.Date < DateTime.Now
                            && p.fixed_date > DateTime.Now
                            orderby p.rank
                            select p).FirstOrDefault();
            if (news == null)
            {

                news = (from p in dt.Articles
                        join p2 in dt.article_cats
                        on p.ID equals p2.ArtID
                        where p2.CatID == 846 &&
                        p.stat == 8 && p.Date < DateTime.Now
                        orderby p.Date descending
                        select p).FirstOrDefault();
            }

            if (news.image_url == null) news.image_url = images_server.name + images_server.updateimg("", "_390_310_");
            //return images_server.name + news.image_url + "|" + news.url + "|" + news.Title;
            return news.Body;
        }
        public virtual ArrayList explore_main_akhbaar_aama()
        {
            List<Article> news = (from p in dt.Articles
                                  join p2 in dt.article_cats
                                  on p.ID equals p2.ArtID
                                  where (p2.CatID == 797 || p2.CatID == 723 || p2.CatID == 798) &&
                                 p.stat == 8 && p.Date < DateTime.Now
                                 && p.fixed_date > DateTime.Now
                                  orderby p.rank
                                  select p).ToList();
            List<Article> last_7_articles = (from p in dt.Articles
                                             join p2 in dt.article_cats
                                             on p.ID equals p2.ArtID
                                             where (p2.CatID == 797 || p2.CatID == 723 || p2.CatID == 798) &&
                                             p.stat == 8 && p.Date < DateTime.Now
                                             orderby p.Date descending
                                             select p).Take(7).ToList();
            foreach (Article a in last_7_articles)
            {
                if (news.Contains(a) == false)
                    news.Add(a);
            }
            news = news.Take(7).ToList();
            ArrayList arrid = new ArrayList();
            ArrayList arrtitle = new ArrayList();
            ArrayList arrapprev = new ArrayList();
            ArrayList arrurl = new ArrayList();
            ArrayList arrimgurl = new ArrayList();
            ArrayList arrdate = new ArrayList();
            ArrayList arr = new ArrayList();
            //arr.Add(news.FirstOrDefault().image_url);
            for (int a = 0; a < news.Count; a++)
            {
                if (string.IsNullOrEmpty(news[a].image_url))
                {
                    news[a].image_url = "media/noimage.jpg";
                }
                arrid.Add(news[a].ID);
                arrtitle.Add(news[a].Title);
                arrapprev.Add(news[a].Apprev);
                arrurl.Add(news[a].url);
                arrimgurl.Add(news[a].image_url);
                arrdate.Add(news[a].Date);
                //arr.Add(news[a].url + "|" + news[a].Title);
            }
            arr.Add(arrid);
            arr.Add(arrtitle);
            arr.Add(arrapprev);
            arr.Add(arrurl);
            arr.Add(arrimgurl);
            arr.Add(arrdate);
            return arr;
        }
        public virtual List<Article> getmainarticles(int catigory_id)
        {
            important_articles = from p in dt.Articles
                                 join p2 in dt.article_cats
                                 on p.ID equals p2.ArtID
                                 where p2.CatID == catigory_id &&
                                 p.stat == 8 && p.Date < DateTime.Now
                                 && p.fixed_date > DateTime.Now
                                 orderby p.rank
                                 select p;
            return important_articles.ToList();
        }
        public virtual DataTable get_last_articles(int catigory_id, int articles_count)
        {
            //  ", conn
            DataTable dt = connection.getdatatable("SELECT  TOP (" + articles_count.ToString() + ") Articles.ID, Articles.Title, Articles.image_url, Articles.Apprev ,Articles.url,Articles.Date", "FROM article_cats INNER JOIN Articles ON article_cats.ArtID = Articles.ID ", "WHERE (article_cats.CatID = " + catigory_id + ") AND (Articles.stat = 8) AND (Articles.Date < GETDATE())", "ORDER BY Articles.Date DESC");
            return dt;
        }
        public virtual DataTable get_main_articles(int catigory_id)
        {
            // "
            DataTable dt = connection.getdatatable("SELECT  Articles.ID, Articles.Title, Articles.Apprev, Articles.image_url ,Articles.url,Articles.Date", "FROM article_cats INNER JOIN Articles ON article_cats.ArtID = Articles.ID  ", " WHERE (article_cats.CatID = " + catigory_id + ") AND (Articles.stat = 8) AND (Articles.fixed_date > getdate()) AND (Articles.Date < getdate())", "ORDER BY Articles.rank");
            return dt;
        }
        DataTable dtmain = new DataTable();
        DataTable dtlast = new DataTable();
        // static  DataTable get_main_articles_test()
        //{
        //    // "
        //    dtmain = connection.getdatatable("SELECT  Articles.ID, Articles.Title, Articles.Apprev, Articles.image_url ,Articles.url,Articles.Date", "FROM article_cats INNER JOIN Articles ON article_cats.ArtID = Articles.ID  ", " WHERE  (Articles.stat = 8) AND (Articles.fixed_date > getdate()) AND (Articles.Date < getdate())", "ORDER BY Articles.rank");
        //    return dtmain;
        //}
        //public DataTable explorenews_without_ajax()
        //{

        //}
        public virtual DataTable explorenews3_without_ajax(int catid, int newsno, string type)
        {
            List<int> myides = new List<int>();
            DataRow[] dt = dtmain.Select("cid =" + catid); //get_main_articles_test();// get_main_articles(catid);
            DataRow[] dt2 = dtlast.Select("type ='" + type + "'");
            DataTable dtfinal = new DataTable();
            dtfinal.Columns.Add("id", typeof(int));
            dtfinal.Columns.Add("image_url", typeof(string));
            dtfinal.Columns.Add("Title", typeof(string));
            dtfinal.Columns.Add("Apprev", typeof(string));
            dtfinal.Columns.Add("url", typeof(string));
            dtfinal.Columns.Add("Date", typeof(DateTime));
            dtfinal.Columns.Add("author_name", typeof(string));
            dtfinal.Columns.Add("readers", typeof(int));
            foreach (DataRow myrow in dt)
            {
                dtfinal.ImportRow(myrow);
                myides.Add(int.Parse(myrow["id"].ToString()));
            }

            foreach (DataRow myrow in dt2)
            {
                if (myides.Contains(int.Parse(myrow["id"].ToString())))
                {
                    continue;
                }
                if (dtfinal.Rows.Count >= newsno) break;
                dtfinal.ImportRow(myrow);
            }


            return dtfinal;
        }
        public virtual ArrayList explorenews_without_ajax(int catid, int newsno, string type)
        {
            //stemp.Start();
            List<int> myides = new List<int>();
            DataRow[] dt = dtmain.Select("cid =" + catid); //get_main_articles_test();// get_main_articles(catid);
            DataRow[] dt2 = dtlast.Select("type ='" + type + "'"); //get_last_articles(catid, newsno);

            DataTable dtfinal = new DataTable();
            dtfinal.Columns.Add("id", typeof(int));
            dtfinal.Columns.Add("image_url", typeof(string));
            dtfinal.Columns.Add("Title", typeof(string));
            dtfinal.Columns.Add("Apprev", typeof(string));
            // dtfinal.Columns.Add("mybody", typeof(string));
            dtfinal.Columns.Add("url", typeof(string));
            dtfinal.Columns.Add("Date", typeof(DateTime));
            foreach (DataRow myrow in dt)
            {
                dtfinal.ImportRow(myrow);
                myides.Add(int.Parse(myrow["id"].ToString()));
            }

            foreach (DataRow myrow in dt2)
            {
                if (myides.Contains(int.Parse(myrow["id"].ToString())))
                {
                    continue;
                }
                if (dtfinal.Rows.Count >= newsno) break;
                dtfinal.ImportRow(myrow);
            }



            ArrayList arrid = new ArrayList();
            ArrayList arrtitle = new ArrayList();
            ArrayList arrapprev = new ArrayList();
            ArrayList arrurl = new ArrayList();
            ArrayList arrimgurl = new ArrayList();
            ArrayList arrdate = new ArrayList();
            ArrayList arr = new ArrayList();


            if (newsno > dtfinal.Rows.Count) newsno = dtfinal.Rows.Count;
            DataRowCollection news = dtfinal.Rows;
            for (int a = 0; a < newsno; a++)
            {
                if (string.IsNullOrEmpty(news[a]["image_url"].ToString())) news[a]["image_url"] = "media/noimage.jpg";
                arrid.Add(news[a]["ID"]);
                arrtitle.Add(news[a]["Title"]);
                arrapprev.Add(news[a]["Apprev"]);
                arrurl.Add(news[a]["url"]);
                arrimgurl.Add(news[a]["image_url"]);
                arrdate.Add(news[a]["Date"]);
            }
            arr.Add(arrid);
            arr.Add(arrtitle);
            arr.Add(arrapprev);
            arr.Add(arrurl);
            arr.Add(arrimgurl);
            arr.Add(arrdate);
            // stemp.Stop();

            // long ltemp = stemp.ElapsedMilliseconds;
            return arr;
        }
        public virtual List<Article> explorenews_without_ajax_v5(int catid, int newsno, string type)
        {
            //stemp.Start();
            List<int> myides = new List<int>();
            DataRow[] dt = dtmain.Select("cid =" + catid); //get_main_articles_test();// get_main_articles(catid);
            DataRow[] dt2 = dtlast.Select("type ='" + type + "'"); //get_last_articles(catid, newsno);

            DataTable dtfinal = new DataTable();
            dtfinal.Columns.Add("id", typeof(int));
            dtfinal.Columns.Add("image_url", typeof(string));
            dtfinal.Columns.Add("Title", typeof(string));
            dtfinal.Columns.Add("Apprev", typeof(string));
            // dtfinal.Columns.Add("mybody", typeof(string));
            dtfinal.Columns.Add("url", typeof(string));
            dtfinal.Columns.Add("Date", typeof(DateTime));
            foreach (DataRow myrow in dt)
            {
                dtfinal.ImportRow(myrow);
                myides.Add(int.Parse(myrow["id"].ToString()));
            }

            foreach (DataRow myrow in dt2)
            {
                if (myides.Contains(int.Parse(myrow["id"].ToString())))
                {
                    continue;
                }
                if (dtfinal.Rows.Count >= newsno) break;
                dtfinal.ImportRow(myrow);
            }




            List<Article> arts = new List<Article>();

            if (newsno > dtfinal.Rows.Count) newsno = dtfinal.Rows.Count;
            DataRowCollection news = dtfinal.Rows;
            for (int a = 0; a < newsno; a++)
            {
                Article art = new Article();
                if (string.IsNullOrEmpty(news[a]["image_url"].ToString())) news[a]["image_url"] = "media/noimage.jpg";
                // arrid.Add(news[a]["ID"]);
                art.ID = Convert.ToInt32(news[a]["ID"]);
                //arrtitle.Add(news[a]["Title"]);
                art.Title = news[a]["Title"].ToString();
                // arrapprev.Add(news[a]["Apprev"]);
                art.Apprev = news[a]["Apprev"].ToString();
                // arrurl.Add(news[a]["url"]);
                art.url = news[a]["url"].ToString();
                // arrimgurl.Add(news[a]["image_url"]);
                art.image_url = news[a]["image_url"].ToString();
                // arrdate.Add(news[a]["Date"]);
                art.Date = Convert.ToDateTime(news[a]["Date"]);
                arts.Add(art);
            }

            // stemp.Stop();

            // long ltemp = stemp.ElapsedMilliseconds;
            return arts;
        }
        public virtual ArrayList explorenews(int catid, int newsno)
        {
            //stemp.Start();
            List<int> myides = new List<int>();
            DataRow[] dt = dtmain.Select("cid =" + catid); //get_main_articles_test();// get_main_articles(catid);
            DataTable dt2 = get_last_articles(catid, newsno);

            DataTable dtfinal = new DataTable();
            dtfinal.Columns.Add("id", typeof(int));
            dtfinal.Columns.Add("image_url", typeof(string));
            dtfinal.Columns.Add("Title", typeof(string));
            dtfinal.Columns.Add("Apprev", typeof(string));
            // dtfinal.Columns.Add("mybody", typeof(string));
            dtfinal.Columns.Add("url", typeof(string));
            dtfinal.Columns.Add("Date", typeof(DateTime));
            foreach (DataRow myrow in dt)
            {
                dtfinal.ImportRow(myrow);
                myides.Add(int.Parse(myrow["id"].ToString()));
            }

            foreach (DataRow myrow in dt2.Rows)
            {
                if (myides.Contains(int.Parse(myrow["id"].ToString())))
                {
                    continue;
                }
                if (dtfinal.Rows.Count >= newsno) break;
                dtfinal.ImportRow(myrow);
            }



            ArrayList arrid = new ArrayList();
            ArrayList arrtitle = new ArrayList();
            ArrayList arrapprev = new ArrayList();
            ArrayList arrurl = new ArrayList();
            ArrayList arrimgurl = new ArrayList();
            ArrayList arrdate = new ArrayList();
            ArrayList arr = new ArrayList();


            if (newsno > dtfinal.Rows.Count) newsno = dtfinal.Rows.Count;
            DataRowCollection news = dtfinal.Rows;
            for (int a = 0; a < newsno; a++)
            {
                if (string.IsNullOrEmpty(news[a]["image_url"].ToString())) news[a]["image_url"] = "media/noimage.jpg";
                arrid.Add(news[a]["ID"]);
                arrtitle.Add(news[a]["Title"]);
                arrapprev.Add(news[a]["Apprev"]);
                arrurl.Add(news[a]["url"]);
                arrimgurl.Add(news[a]["image_url"]);
                arrdate.Add(news[a]["Date"]);
            }
            arr.Add(arrid);
            arr.Add(arrtitle);
            arr.Add(arrapprev);
            arr.Add(arrurl);
            arr.Add(arrimgurl);
            arr.Add(arrdate);
            // stemp.Stop();

            // long ltemp = stemp.ElapsedMilliseconds;
            return arr;
        }
        public virtual DataTable explorenews3(int catid, int newsno)
        {
            //stemp.Start();
            List<int> myides = new List<int>();
            DataRow[] dt = dtmain.Select("cid =" + catid); //get_main_articles_test();// get_main_articles(catid);
            DataTable dt2 = get_last_articles(catid, newsno);

            DataTable dtfinal = new DataTable();
            dtfinal.Columns.Add("id", typeof(int));
            dtfinal.Columns.Add("image_url", typeof(string));
            dtfinal.Columns.Add("Title", typeof(string));
            dtfinal.Columns.Add("Apprev", typeof(string));
            // dtfinal.Columns.Add("mybody", typeof(string));
            dtfinal.Columns.Add("url", typeof(string));
            dtfinal.Columns.Add("Date", typeof(DateTime));
            foreach (DataRow myrow in dt)
            {
                dtfinal.ImportRow(myrow);
                myides.Add(int.Parse(myrow["id"].ToString()));
            }

            foreach (DataRow myrow in dt2.Rows)
            {
                if (myides.Contains(int.Parse(myrow["id"].ToString())))
                {
                    continue;
                }
                if (dtfinal.Rows.Count >= newsno) break;
                dtfinal.ImportRow(myrow);
            }


            return dtfinal;
            //ArrayList arrid = new ArrayList();
            //ArrayList arrtitle = new ArrayList();
            //ArrayList arrapprev = new ArrayList();
            //ArrayList arrurl = new ArrayList();
            //ArrayList arrimgurl = new ArrayList();
            //ArrayList arrdate = new ArrayList();
            //ArrayList arr = new ArrayList();


            //if (newsno > dtfinal.Rows.Count) newsno = dtfinal.Rows.Count;
            //DataRowCollection news = dtfinal.Rows;
            //for (int a = 0; a < newsno; a++)
            //{
            //    if (string.IsNullOrEmpty(news[a]["image_url"].ToString())) news[a]["image_url"] = "media/noimage.jpg";
            //    arrid.Add(news[a]["ID"]);
            //    arrtitle.Add(news[a]["Title"]);
            //    arrapprev.Add(news[a]["Apprev"]);
            //    arrurl.Add(news[a]["url"]);
            //    arrimgurl.Add(news[a]["image_url"]);
            //    arrdate.Add(news[a]["Date"]);
            //}
            //arr.Add(arrid);
            //arr.Add(arrtitle);
            //arr.Add(arrapprev);
            //arr.Add(arrurl);
            //arr.Add(arrimgurl);
            //arr.Add(arrdate);
            //// stemp.Stop();

            //// long ltemp = stemp.ElapsedMilliseconds;
            //return arr;
        }
        public virtual List<Article> getlastarticles(int catigory_id, int articles_count)
        {

            List<Article> temp = (from p in dt.Articles
                                  join p2 in dt.article_cats
                                  on p.ID equals p2.ArtID
                                  where p2.CatID == catigory_id &&
                                  p.stat == 8 && p.Date <= DateTime.Now
                                  orderby p.Date descending
                                  select p).Take(articles_count).ToList();
            //stemp.Stop();
            //long ltemp = stemp.ElapsedMilliseconds;
            return temp;
        }
        List<Article> getmainarticles2(int catigory_id)
        {
            IEnumerable<Article> important_articles2 = from p in dt.Articles
                                                       join p2 in dt.article_cats
                                                       on p.ID equals p2.ArtID
                                                       where p2.CatID == catigory_id &&
                                                       p.stat == 8 && p.Date < DateTime.Now
                                                       && p.fixed_date > DateTime.Now
                                                       orderby p.rank
                                                       select p;
            ArrayList arrid = new ArrayList();
            ArrayList arrtitle = new ArrayList();
            ArrayList arrapprev = new ArrayList();
            ArrayList arrurl = new ArrayList();
            ArrayList arrimgurl = new ArrayList();
            return important_articles2.ToList();
        }
        List<Article> getlastarticles2(int catigory_id, int articles_count)
        {
            return (from p in dt.Articles
                    join p2 in dt.article_cats
                    on p.ID equals p2.ArtID
                    where p2.CatID == catigory_id &&
                    p.stat == 8 && p.Date <= DateTime.Now
                    orderby p.Date descending
                    select p).Take(articles_count).ToList();

        }

        int get_hour_event_id()
        {
            List<setting> setting_list = dt.settings.ToList();

            foreach (setting s in setting_list)
            {
                if (s.varname == "hevent_on" && s.value == "1")
                {
                    var id = Convert.ToInt32((from p in dt.settings
                                              where p.varname == "hevent_id"
                                              select p).SingleOrDefault().value);
                    return id;
                }

            }
            return -1;

        }

        public string get_hour_event_name()
        {

            List<setting> setting_list = dt.settings.ToList();
            foreach (setting s in setting_list)
            {
                if (s.varname == "hevent_on" && s.value == "1")
                {
                    var hev_name = (from p in dt.settings
                                    where p.varname == "hevent_name"
                                    select p).SingleOrDefault().value.ToString();
                    return hev_name;
                }
            }
            return "";

        }
        public List<Article> explore_hour_events()
        {
            int hv_id = get_hour_event_id();
            if (hv_id == -1)
                return null;
            else
                return (from p in dt.Articles
                        join p2 in dt.article_cats
                        on p.ID equals p2.ArtID
                        where p2.CatID == hv_id &&
                        p.stat == 8
                        orderby p.Date descending
                        select p).Take(30).ToList();


        }
        public List<Article> explore_hour_events_v5(int artsno)
        {
            int hv_id = get_hour_event_id();
            if (hv_id == -1)
            {
                int catid = (from p in dt.Categories
                             where p.ParentID == 652
                             orderby p.ID descending
                             select p.ID).FirstOrDefault();
                return (from p in dt.Articles
                        join p2 in dt.article_cats
                        on p.ID equals p2.ArtID
                        where p2.CatID == catid &&
                        p.stat == 8
                        orderby p.Date descending
                        select p).Take(artsno).ToList();
            }
            else
                return (from p in dt.Articles
                        join p2 in dt.article_cats
                        on p.ID equals p2.ArtID
                        where p2.CatID == hv_id &&
                        p.stat == 8
                        orderby p.Date descending
                        select p).Take(artsno).ToList();


        }

        public string getlongdate()
        {
            // SELECT TOP (1) Articles.LongDate FROM article_cats INNER JOIN Articles ON article_cats.ArtID = Articles.ID WHERE (article_cats.CatID = 621) and (stat=8) and (date<getdate()) order by date desc
            return (from p in dt.Articles
                    join p2 in dt.article_cats
                    on p.ID equals p2.ArtID
                    where
                    p.stat == 8 &&
                    p.Date < DateTime.Now
                    orderby p.Date descending
                    select p).FirstOrDefault().LongDate;

        }
        public string get_khabar_aagel()
        {
            //aagelclass ag = new aagelclass();
            //return ag.get_khabar_aagle();
            //List<setting> setting_list = dt.settings.ToList();


            string[] hev_name = (from p in dt.settings
                                 where p.varname == "announc_txt" || p.varname == "announce_url"
                                 orderby p.varname
                                 select p.value).ToArray();
            string result = hev_name[0];
            if (!string.IsNullOrWhiteSpace(hev_name[1]))
                result = "<a style='color:white;' href='" + hev_name[1] + "' target='blank'>" + result + "</a>";
            return result;



        }
        public string get_aagel_date()
        {
            CultureInfo ci = new CultureInfo("ar-SA");
            TimeZoneInfo saudidate = TimeZoneInfo.FindSystemTimeZoneById("Arab Standard Time");
            DateTime mydate = TimeZoneInfo.ConvertTime(DateTime.Now, saudidate);
            //Response.Write(mydate.ToString("t"));
            //Response.Write("<br>" + DateTime.UtcNow.ToString("t", ci));
            return mydate.ToString("t") + " مكة المكرمة " + DateTime.UtcNow.ToString("t", ci) + " جرينتش ";
        }

        public virtual string explore_marquee_important_news(string lang)
        {
            StringBuilder sb = new StringBuilder();

            var ads_before_news = from p in dt.marque_textads
                                  where p.active == true
                                  select new { p.ad, p.ad_link };
            string urllng = "";
            //if (string.IsNullOrEmpty(lang) == false) urllng = "?lang=" + lang;
            //translatearabictoenglish tr = new translatearabictoenglish();
            foreach (var ad in ads_before_news)
            {
                sb.Append(" *** ");
                sb.Append(string.Format("<a href='{0}' target='_blank' style='color:black'>{1}</a>", ad.ad_link + urllng, ad.ad));


            }

            var news = from p in dt.marque_article
                       join p2 in dt.Articles
                       on p.article_id equals p2.ID
                       where p.cat_id == 1
                       orderby p.rank
                       select new { p2.Title, p2.ID, p2.url };



            sb.Append("***");

            foreach (var art in news)
            {
                sb.Append(string.Format("<a href='{0}' target='_blank' style='color:black'>{1}</a>", art.url + urllng, art.Title));

                sb.Append(" *** ");
            }

            return sb.ToString();


        }
        public virtual DataTable get_sohof()
        {
            return connection.getdatatable("SELECT  TOP 1  Articles.Apprev ,Articles.url,Articles.Date", "from Articles ", "WHERE articles.cat_id =314 AND Articles.stat = 8 AND Articles.Date < GETDATE()", "ORDER BY Articles.Date DESC");

        }
        public virtual List<string> get_sohof_v4()
        {
            //جولة في صحافة الأربعاء 22 أبريل
            //ملخص صحافة الثلاثاء 21 أبريل

            Article a = (from p in dt.Articles
                         where p.Title.StartsWith("جولة في صحافة") == true || p.Title.StartsWith("ملخص صحافة") == true
                         orderby p.ID descending
                         select p).FirstOrDefault();
            List<string> art_parts = new List<string>();
            if (string.IsNullOrEmpty(a.image_url)) a.image_url = "";
            art_parts.Add(a.ID.ToString());
            art_parts.Add(a.Title);
            art_parts.Add(a.Apprev);
            art_parts.Add(a.url);
            art_parts.Add(a.image_url);

            art_parts.Add(Convert.ToDateTime(a.Date).ToString("dddd dd MMMM yyyy"));
            return art_parts;

        }
        public List<string> ProcessRSSItem(string rssURL)
        {
            WebRequest myRequest = WebRequest.Create(rssURL);
            WebResponse myResponse = myRequest.GetResponse();

            Stream rssStream = myResponse.GetResponseStream();
            XmlDocument rssDoc = new XmlDocument();
            rssDoc.Load(rssStream);

            XmlNodeList rssItems = rssDoc.SelectNodes("rss/channel/item");

            string title = "";
            string link = "";
            // string description = "";
            int count = 6;
            if (count > rssItems.Count) count = rssItems.Count;
            List<string> arts = new List<string>();
            for (int i = 0; i < count; i++)
            {
                XmlNode rssDetail;

                rssDetail = rssItems.Item(i).SelectSingleNode("title");
                if (rssDetail != null)
                {
                    title = rssDetail.InnerText;
                }
                else
                {
                    title = "";
                }
                rssDetail = rssItems.Item(i).SelectSingleNode("link");
                if (rssDetail != null)
                {
                    link = rssDetail.InnerText;
                }
                else
                {
                    link = "";
                }
                //rssDetail = rssItems.Item(i).SelectSingleNode("description"); 
                //if (rssDetail != null) 
                //{ 
                //description = rssDetail.InnerText;
                //} 
                //else 
                //{
                //description = "";
                //} 

                // Response.Write("<p><b><a href='" + link + "' target='new'>" + title + "</a></b><br/>");
                //Response.Write(description + "</p>");
                arts.Add("<a href='" + link + "' target='new'>" + title + "</a>");
            }
            return arts;
        }
        public List<Article> getmemochannels()
        {
            string rssURL = "http://www.islammemo.tv/rss/top";
            WebRequest myRequest = WebRequest.Create(rssURL);
            WebResponse myResponse = myRequest.GetResponse();

            Stream rssStream = myResponse.GetResponseStream();
            XmlDocument rssDoc = new XmlDocument();
            rssDoc.Load(rssStream);

            XmlNodeList rssItems = rssDoc.SelectNodes("rss/channel/item");

            string title = "";
            string link = "";
            string img = "";
            // string description = "";
            int count = 21;
            if (count > rssItems.Count) count = rssItems.Count;
            // List<string> arts = new List<string>();
            List<Article> arts = new List<Article>();
            for (int i = 0; i < count; i++)
            {
                Article art = new Article();
                XmlNode rssDetail;

                rssDetail = rssItems.Item(i).SelectSingleNode("title");
                if (rssDetail != null)
                {
                    //title = rssDetail.InnerText;
                    art.Title = rssDetail.InnerText;
                }
                else
                {
                    //title = "";
                    art.Title = "";
                }
                rssDetail = rssItems.Item(i).SelectSingleNode("link");
                if (rssDetail != null)
                {
                    //link = rssDetail.InnerText;
                    art.url = rssDetail.InnerText;
                }
                else
                {
                    //link = "";
                    art.url = "";
                }
                string imgurl = rssItems.Item(i).SelectSingleNode("enclosure").Attributes["url"].InnerText;
                if (imgurl != null)
                {
                    img = imgurl;
                }
                else
                {
                    img = "";
                }

                art.image_url = img;
                //rssDetail = rssItems.Item(i).SelectSingleNode("description"); 
                //if (rssDetail != null) 
                //{ 
                //description = rssDetail.InnerText;
                //} 
                //else 
                //{
                //description = "";
                //} 

                // Response.Write("<p><b><a href='" + link + "' target='new'>" + title + "</a></b><br/>");
                //Response.Write(description + "</p>");
                //   arts.Add("<a href='" + link + "' target='new'>" + title + "</a>");
                arts.Add(art);
            }
            return arts;
        }
        public Dictionary<string, string> get_curruncy()
        {
            Dictionary<string, string> curruncy_list = new Dictionary<string, string>();
            string provider_url = "http://38.121.76.242/webservices/Currency.aspx";
            string build_url = provider_url;
            XmlDocument doc = new XmlDocument();
            doc.Load(build_url);
            XmlNode data = doc.DocumentElement;
            XmlNodeList x = data.SelectNodes("Currency");
            for (int i = 0; i < x.Count; i++)
            {
                curruncy_list.Add(x[i].SelectSingleNode("name_ar").ChildNodes[0].Value, x[i].SelectSingleNode("Value").ChildNodes[0].Value);
                //.Write(x[i].SelectSingleNode("name_ar").ChildNodes[0].Value + "  " + "  " + x[i].SelectSingleNode("Value").ChildNodes[0].Value + "<br/>");
            }
            return curruncy_list;
        }

        public virtual string topreadingarticle()
        {
            try
            {
                return (from p in dt.Articles
                        where p.Date.Value.Month == DateTime.Now.Month && p.Date.Value.Day == DateTime.Now.Day && p.Date.Value.Year == DateTime.Now.Year && p.stat == 8
                        orderby p.Readers descending
                        select "<a href='" + p.url + "'>" + p.Title + "</a>").FirstOrDefault().ToString();
            }
            catch
            {
                return "";
            }
        }

        public virtual String ImgCatImg()
        {
            //String temp = "";
            //String imgart=(from p in dt.images_cat_images
            //              where p.selected==true && p.Article.stat==8
            //              orderby p.Article.ID descending
            //               select "<img src='" +images_server.name+ p.imgurl.Replace(".","_390_310_.")+ "' onclick='imgpopup(\""+p.Article.url+"\");' Title='" + p.Article.Title + "' width='208px' height='116px'  />"     
            //                   ).FirstOrDefault();
            String imgart = (from p in dt.images_cat_images
                             where p.selected == true && p.Articles.stat == 8
                             orderby p.Articles.ID descending
                             select p.Articles.url + "|" + images_server.name + p.imgurl.Replace(".", "_390_310_.") + "|" + p.Articles.Title + "|" + p.title).FirstOrDefault();




            return imgart;
            //<a href="#" title=""><img src="../../images/dummy/press-photo.gif" onclick="imgpopup();" alt="صورة" /></a>

        }
        public virtual List<string> get_imgs()
        {
            int art_id = Convert.ToInt32(dtlast.Select("type ='img'")[0][0]);

            return (from p in dt.images_cat_images
                    where p.Articles.ID == art_id
                    select images_server.name + p.imgurl + "|" + p.title).ToList();


        }
        public DataTable testfixednews()
        {
            return dtmain;
        }
        public DataTable get_most_searched_words()
        {
            return connection.getdatatable("SELECT top 5 words.id, [words],url ", " FROM [words] join articles on words.artid=articles.id ", " where words.allow=1 and stat=8  ORDER BY words.id DESC ", "");
        }
        public string[] get_weather(string location)
        {
            // string location = "alex,egypt";
            // string[] weather=new string[5];
            string build_url = "http://38.121.76.242/webservices/get_weather.aspx?location=" + location;
            XmlDocument doc = new XmlDocument();
            doc.Load(build_url);
            XmlNode data = doc.DocumentElement;
            string Date = data.SelectSingleNode("Date_Time").ChildNodes[0].Value;
            string temp_c = data.SelectSingleNode("temp_c").ChildNodes[0].Value;
            string description = data.SelectSingleNode("description").ChildNodes[0].Value;
            string wind = data.SelectSingleNode("wind_speed").ChildNodes[0].Value;
            string humidity = data.SelectSingleNode("humidity").ChildNodes[0].Value;
            string img = data.SelectSingleNode("image").ChildNodes[0].Value;
            string[] weather = { location, Date, temp_c, img, humidity, wind };
            return weather;
        }

        public string[] get_country(string location)
        {

            string[] weather_search = new string[6];
            // location = Request.QueryString["location"].ToString();

            string build_url = "http://www.worldweatheronline.com/feed/search.ashx?key=a775198bd5114647131902&query=" + location + "&num_of_results=5&format=xml";
            XmlDocument doc = new XmlDocument();
            doc.Load(build_url);
            XmlNode data = doc.DocumentElement;

            XmlNodeList xl = data.SelectNodes("result");//repeted

            for (int i = 0; i < xl.Count; i++)
            {
                weather_search[i] = xl[i].SelectSingleNode("areaName").ChildNodes[0].Value + "," + xl[i].SelectSingleNode("country").ChildNodes[0].Value;
            }
            return weather_search;

        }
        public int select_tab(string country_ip)
        {
            int select;
            IPAddress ipAddress;
            string iso3166TwoLetterCode = "";
            if (IPAddress.TryParse(country_ip, out ipAddress))
            {

                string country = ipAddress.Country(); // return value: UNITED STATES
                iso3166TwoLetterCode = ipAddress.Iso3166TwoLetterCode(); // return value: US

            }

            if (iso3166TwoLetterCode == null)
            {
                select = 1;
            }
            else
            {
                iso3166TwoLetterCode = iso3166TwoLetterCode.ToLower();
                if (iso3166TwoLetterCode == "eg")
                {
                    select = 2;
                }
                else if (iso3166TwoLetterCode == "sa")
                {
                    select = 1;
                }
                else
                {
                    select = 3;
                }
            }
            return select;

        }

        public int get_country_no(string country_ip)
        {
            //double convert_ip = 0;
            //BigInteger d = new BigInteger();
            //BigInteger w = new BigInteger();
            //BigInteger q = new BigInteger();
            //byte[] addrBytes = System.Net.IPAddress.Parse(country_ip).GetAddressBytes();
            //if (System.BitConverter.IsLittleEndian)
            //{
            //    //little-endian machines store multi-byte integers with the
            //    //least significant byte first. this is a problem, as integer
            //    //values are sent over the network in big-endian mode. reversing
            //    //the order of the bytes is a quick way to get the BitConverter
            //    //methods to convert the byte arrays in big-endian mode.
            //    System.Collections.Generic.List<byte> byteList = new System.Collections.Generic.List<byte>(addrBytes);
            //    byteList.Reverse();
            //    addrBytes = byteList.ToArray();
            //}
            //ulong[] addrWords = new ulong[2];
            //if (addrBytes.Length > 8)
            //{
            //    addrWords[0] = System.BitConverter.ToUInt64(addrBytes, 0);
            //    w = addrWords[0];
            //    addrWords[1] = System.BitConverter.ToUInt64(addrBytes, 8);
            //    q = addrWords[1];
            //    string c = Convert.ToString(w + " " + q);
            //    c = c.Replace(" ", "");
            //    //  convert_ip = Convert.ToInt64(c);
            //    d = BigInteger.Parse(c);




            //}
            //else
            //{
            //    addrWords[0] = System.BitConverter.ToUInt32(addrBytes, 0);
            //    addrWords[1] = 0;
            //    long z = Convert.ToInt64(addrWords[0]);
            //    d = (z / 1000000);

            //}
            //return convert_ip;
            //**************************************************************
            //byte[] addrBytes = System.Net.IPAddress.Parse(country_ip).GetAddressBytes();
            //string ip = "";
            //for (int i = 0; i < addrBytes.Length; i++)
            //{
            //    ip = ip + addrBytes[i].ToString() + ".";
            //}
            //ip.Remove(ip.Length, 1);
            //ip = ip.TrimEnd('.');
            int select;
            string result = "";
            ArrayList param = new ArrayList();
            ArrayList values = new ArrayList();
            param.Add("@ip");
            values.Add(country_ip);
            object country_select = connection.scaler_proc("get_country_by_ip", param, values);
            string convert_country = country_select.ToString();
            string[] country_split = convert_country.Split('"');
            string resultobj = country_split[1];
            if (resultobj == null)
            {
                return 3;
            }
            else
            {
                result = resultobj.ToString();
                if (result.ToLower() == "eg")
                {
                    select = 2;
                }
                else if (result.ToLower() == "sa")
                {
                    select = 1;
                }
                else
                {
                    select = 3;
                }
            }
            return select;

        }
        //  public IPAddress MapToIPv4();
        //bool AreEquivalent(IPAddress ip6addr, IPAddress ip4addr)
        //{
        //    byte[] ip6bytes = ip6addr.GetBytes();
        //    byte[] ip4bytes = ip4addr.
        //    for (int i = 0; i < 10; i++)
        //    {
        //        if (ip6bytes[i] != 0)
        //            return false;
        //    }
        //    for (int i = 0; i < 4; i++)
        //    {
        //        if (ip6bytes[i + 12] != ip4bytes[i])
        //            return false;
        //    }
        //    return true;
        //}
        public string IP4Address { get; set; }


        static public string LongToIP(long longIP)
        {

            string ip = string.Empty;

            for (int i = 0; i < 4; i++)
            {

                int num = (int)(longIP / Math.Pow(256, (3 - i)));

                longIP = longIP - (long)(num * Math.Pow(256, (3 - i)));

                if (i == 0)

                    ip = num.ToString();

                else

                    ip = ip + "." + num.ToString();

            }

            return ip;

        }

        public long IP2Long(string ip)
        {

            string[] ipBytes;

            double num = 0;

            if (!string.IsNullOrEmpty(ip))
            {

                ipBytes = ip.Split('.');

                for (int i = ipBytes.Length - 1; i >= 0; i--)
                {

                    num += ((int.Parse(ipBytes[i]) % 256) * Math.Pow(256, (3 - i)));

                }

            }

            return (long)num;

        }

        public int get_minip_eg()
        {
            int f = 0;
            BigInteger[] minnum_eg = { 2306197189396791296, 2306197258116268032, 2306197842231820288, 2306198151469465600, 2306198460707110912, 2306198735585017856, 2306199010462924800, 2306199216669589504, 3174474787342778368, 3175033614127595520, 3175033923365240832, 3175036500345618432, };
           // BigInteger[] max_eg ={184467440737095516152306197193691758591,184467440737095516152306197262411235327,184467440737095516152306197846526787583,184467440737095516152306198155764432895,184467440737095516152306198465002078207,184467440737095516152306198739879985151,184467440737095516152306199014757892095,184467440737095516152306199216669655039,184467440737095516153174492379528822783,184467440737095516153175033618422562815,184467440737095516153175033927660208127,184467440737095516153175036504640585727};
            BigInteger[] minnum_sa = { 2306131613856038912, 2306131614131290112, 2306131614396317696, 2306131614476009472, 2306147092898250752, 2306149360640983040, 3027022340958453760, 3027475030511452160, 3026442451654017024, 3026442967050092544, 3026443791683813376, 3026446403023929344, 3026446540462882816, 3026453000093696000, 3026453171892387840, 3026511308569706496, 3026549241720864768, 3026550341232492544, 3026688879697592320, 3026991279754969088, 3026992551065288704, 3026996055758602240, 3026996674233892864, 3026997223989706752, 3026999216854532096, 3027006432399589376};
         // BigInteger[] max_sa ={184467440737095516152306131613856104447,184467440737095516152306131614131421183,184467440737095516152306131614396383231,184467440737095516152306131614476075007,184467440737095516152306147092898381823,184467440737095516152306149364935950335,184467440737095516153027022345253421055,184467440737095516153027475034806419455,184467440737095516153026442455948984319,184467440737095516153026442971345059839,184467440737095516153026443795978780671,184467440737095516153026446407318896639,184467440737095516153026446544757850111,184467440737095516153026453004388663295,184467440737095516153026453176187355135,184467440737095516153026511312864673791,184467440737095516153026549246015832063,184467440737095516153026550345527459839,184467440737095516153026688883992559615,184467440737095516153026991284049936383,184467440737095516153026992555360255999,184467440737095516153026996060053569535,184467440737095516153026996678528860159,184467440737095516153026997228284674047,184467440737095516153026999221149499391,184467440737095516153027006436694556671};



            return f;
        }
    }
}