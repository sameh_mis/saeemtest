﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Data;
using System.Data;
using System.Configuration;

using System.Web.Mail;


namespace memov5entity.Models
{
    public class mailing
    {
        string constr = "";
        SqlConnection cn;
        SqlCommand cmd;
        public string add_email(string SEmail)
        {
            constr = ConfigurationManager.ConnectionStrings["mailinglist"].ConnectionString;
            cn = new SqlConnection(constr);
            cmd = cn.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "add_email";
            SqlParameter p1 = new SqlParameter("@email", SEmail);
            cmd.Parameters.Add(p1);
            try
            {
                cn.Open();
                if (SEmail.Length > 0)
                    cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception ex) { return ("نعتذر عن حدوث خطا  رجاء تكرار المحاولة فيما بعد"); }
            finally { cn.Close(); }
            //webdev2009@islammemo.cc
            //Response.Write("تم تسجيل الاشتراك بنجاح");
            return ("تم تسجيل الاشتراك بنجاح");
        }


        //////////////////remove email//////////////////////////////////////////////////
        public string Remove_email_share(string SEmail)
        {
            constr = ConfigurationManager.ConnectionStrings["mailinglist"].ConnectionString;
            cn = new SqlConnection(constr);
            cmd = cn.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "getguid";
            SqlParameter p1 = new SqlParameter("@email", SEmail);
            cmd.Parameters.Add(p1);
            string guid = "";
            cn.Open();
            if (SEmail.Length > 0)
                guid = cmd.ExecuteScalar().ToString();
            cn.Close();
            if (string.IsNullOrEmpty(guid) == false)
            {
                MailMessage objEmail = new MailMessage();
                objEmail.To = SEmail;
                objEmail.From = "sendto@islammemo.cc";
                //objEmail.Cc       = txtCc.Text;
                objEmail.Subject = "message from islammemo";
                objEmail.Body = "لاستكمال الغاء اشتراكك في القائمة البريدية الخاصة بموقع مفكرة الاسلام اضغط على هذا الرابط" + "<br>" + "http://www.saeem.com/mailing_list_remove_email.aspx?id=" + guid;
                objEmail.Priority = MailPriority.High;
                objEmail.BodyFormat = MailFormat.Html;
                SmtpMail.SmtpServer = "smtp.islammemo.cc";
                objEmail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
                objEmail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "sendto"); //set your username here
                objEmail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", ConfigurationManager.AppSettings["password_email"]);	//set your password here

                try
                {
                    SmtpMail.Send(objEmail);


                }
                catch (Exception ex)
                {

                    return ("نعتذر عن حدوث خطا  رجاء تكرار المحاولة فيما بعد");
                }
            }
            //webdev2009@islammemo.cc
            //Response.Write("تم استقبال الطلب وسوف يتم ارسال رسالة في البريد الالكترونى الخاص بك لاستكمال خطوات الغاء اشتراكك في المفكرة");
            return ("تم استقبال الطلب وسوف يتم ارسال رسالة في البريد الالكترونى الخاص بك لاستكمال خطوات الغاء اشتراكك في المفكرة");
        }


    }

}
