﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Data;
using System.Data;
using System.Configuration;
using System.Text;

namespace memov5entity.Models
{

    public class shreetlogin
    {

        string constr = "";
        SqlConnection cn;
        SqlCommand cmd;
        bool flag = false;
        test4Entities dt = new test4Entities();
        public string shreet_log(string logname, string logpass)
        {
            string name = "";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["test4ConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("SELECT id FROM bar WHERE (uname = @uname) AND (password = @password)", conn);
            cmd.Parameters.AddWithValue("@uname", logname.Trim().ToLower());
            cmd.Parameters.AddWithValue("@password", logpass.Trim().ToLower());
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                flag = true;
                name = dr[0].ToString();

            }

            conn.Close();


            return name;
        }

        public string shreet_register(string name, string pass, string email, string site)
        {
            string reg_name = "";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["test4ConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("INSERT INTO bar (uname, password, email, site) VALUES (@uname, @password, @email, @site)", conn);
            cmd.Parameters.AddWithValue("@uname", name.Trim().ToLower());
            cmd.Parameters.AddWithValue("@password", pass.Trim().ToLower());
            cmd.Parameters.AddWithValue("@email", email.Trim().ToLower());
            cmd.Parameters.AddWithValue("@site", site.Trim().ToLower());
            conn.Open();
            try
            {
                cmd.ExecuteNonQuery();

            }
            catch (SqlException ex)
            {
                if (ex.Message.Contains("UNIQUE"))
                {
                    // lblerror.Text = Resources.memo.repeatedname;
                    // lblerror.Visible = true;
                }
                conn.Close();
                // return;
            }
            cmd = new SqlCommand("select id from bar where uname=@uname", conn);
            cmd.Parameters.AddWithValue("@uname", name.Trim().ToLower());
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {

                reg_name = dr[0].ToString();


            }
            dr.Close();
            conn.Close();

            return reg_name;

        }

        public string explore_marquee_important_news()
        {
            StringBuilder sb = new StringBuilder();
            var news = from p in dt.marque_article
                       join p2 in dt.Articles
                       on p.article_id equals p2.ID
                       where p.cat_id == 1
                       orderby p.rank
                       select new { p2.Title, p2.ID, p2.url };
            sb.Append("***");

            foreach (var art in news)
            {
                sb.Append(string.Format("<a href='{0}'style='font-size:12px; font-weight:bold; text-decoration:none;color:Black;' target='_blank'>{1}</a>", art.url, art.Title));

                sb.Append(" *** ");
            }

            return sb.ToString();


        }
        public string explore_marquee_important_news_up()
        {
            StringBuilder sb = new StringBuilder();
            var news = from p in dt.marque_article
                       join p2 in dt.Articles
                       on p.article_id equals p2.ID
                       where p.cat_id == 1
                       orderby p.rank
                       select new { p2.Title, p2.ID, p2.url };
            // sb.Append("***");
            foreach (var art in news)
            {
                sb.Append(string.Format("<a href='{0}' style='color:black;font-size:13px; font-weight:bold;text-decoration:none;' target='_blank'>{1}</a><br/><br/>", art.url, art.Title));

                // sb.Append(" *** ");
            }
            return sb.ToString();
        }

    }
}