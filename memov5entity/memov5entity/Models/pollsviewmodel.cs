﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using memov5entity.Models;
namespace memov5entity.Models
{
    public class pollsviewmodel
    {
        //  pollsDataContext dt = new pollsDataContext();
        //  islammemoDataContext dt2 = new islammemoDataContext();
        weekly_pollSQLEntities dt = new weekly_pollSQLEntities();
        test4Entities dt2 = new test4Entities();
        int page, count;
        public virtual int getpageno
        {
            get { return page; }
            set { page = value; }
        }
        public pollsviewmodel()
        { }
        public pollsviewmodel(int page)
        {
            this.page = page;
            this.count = (from p in dt.tblPolls
                          orderby p.id_no descending
                          select p.id_no).Count();

        }
        public virtual string getlongdate()
        {
            // SELECT TOP (1) Articles.LongDate FROM article_cats INNER JOIN Articles ON article_cats.ArtID = Articles.ID WHERE (article_cats.CatID = 621) and (stat=8) and (date<getdate()) order by date desc
            return (from p in dt2.Articles
                    join p2 in dt2.article_cats
                    on p.ID equals p2.ArtID
                    where p2.CatID == 621 &&
                    p.stat == 8 &&
                    p.Date < DateTime.Now
                    orderby p.Date descending
                    select p).FirstOrDefault().LongDate;

        }
        public virtual string explore_important_news()
        {
            StringBuilder sb = new StringBuilder();



            var news = from p in dt2.marque_article
                       join p2 in dt2.Articles
                       on p.article_id equals p2.ID
                       where p.cat_id == 1
                       orderby p.rank
                       select new { p2.Title, p2.ID, p2.url };





            foreach (var art in news)
            {
                sb.Append(string.Format("<a href='{0}' target='_blank'>{1}</a>", art.url, art.Title));

                sb.Append("*");
            }

            return sb.ToString();


        }
        public virtual int get_pages_count()
        {


            //  if (pages_count < 33) return 1;
            int pages_count = count / 9;
            if (pages_count % count > 0 && pages_count > 0)
                pages_count++;

            return pages_count;

        }
        public virtual string get_37_polls()
        {
            int skippingno = 0;
            int cat_count = get_pages_count();
            int true_page = cat_count + 1 - page;
            if (page <= cat_count && page > 0)
                skippingno = cat_count - true_page;
            StringBuilder s = new StringBuilder();
            if (page == 1)
            {
                var polls_list = (from p in dt.tblPolls
                                  orderby p.id_no descending
                                  select new { p.id_no, p.Question, p.Date }).Take(9).ToList();
                foreach (var p in polls_list)
                {
                    s.Append(p.id_no + "*" + p.Question + "*" + p.Date);
                    s.Append("|");


                }
                return s.ToString();
            }
            else
            {
                var polls_list = (from p in dt.tblPolls
                                  orderby p.id_no descending
                                  select new { p.id_no, p.Question, p.Date }).Skip(skippingno * 9).Take(9).ToList();
                foreach (var p in polls_list)
                {
                    s.Append(p.id_no + "*" + p.Question + "*" + p.Date);
                    s.Append("|");


                }
                return s.ToString();
            }


        }
        //public  tblPoll getpollbyid(int poll_id)
        //{
        //    tblPoll poll = dt.tblPolls.Where(p => p.id_no == poll_id);
        //    return poll;

        //}
    }


    public class ajaxpollviewmodel
    {
        // pollsDataContext dt = new pollsDataContext();
        weekly_pollSQLEntities dt = new weekly_pollSQLEntities();
        string bab;

        public string Bab
        {
            get { return bab; }
            set { bab = value; }
        }
        int groupid;
        public ajaxpollviewmodel(string bab)
        {
            this.bab = bab;
            groupid = Convert.ToInt32(dt.babs.Where(p => p.babname == bab).FirstOrDefault().groupnom);


        }
        public tblPoll getpoll()
        {
            tblPoll poll = (from p in dt.tblPolls
                            where p.active == true && p.@group == groupid
                            orderby p.id_no descending
                            select p).FirstOrDefault();
            return poll;


        }
        public int add_voting(string choice)
        {
        
            if (choice != null )
            {
                if (choice.Length <= 10)
                {
                    int poll_id = (from p in dt.tblPolls
                                   where p.active == true && p.@group == groupid
                                   orderby p.id_no descending
                                   select p.id_no).FirstOrDefault();
                    connection.update(string.Format("update dbo.tblPolls set {0} = {1} +1 where id_no= {2}", choice, choice, poll_id));
                    // dt.ExecuteStoreCommand("update dbo.tblPolls set {0} = {1} +1 where id_no= {2}",choice,choice,poll_id);
                    return poll_id;
                }
            }
            return 0;
        }
        public int get_poll_id()
        {
            int poll_id = (from p in dt.tblPolls
                           where p.active == true && p.@group == groupid
                           orderby p.id_no descending
                           select p.id_no).FirstOrDefault();
            return poll_id;

        }
    }

}