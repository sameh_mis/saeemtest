using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


namespace memo
{
    public class design
    {
        public static string gettabs(Dictionary<string, string> cats) 
        {
            StringBuilder sb = new StringBuilder();

            int counter=0;
            foreach (KeyValuePair<string,string> dr in cats)
            {
                
                if (counter < 1)
                {
                    //<td class="atab" style="cursor:pointer;" onclick="default_tab_click(this.parentNode,this,1);" >
                    //    <div class="tab-open-c" >
                    //        <span>�� ����</span>
                    //         <img src="imgs/trans.gif" style="height: 20px; width: 2px;" />
                    //    </div>
                    //</td>
                    sb.Append("<td catid=\"" + dr.Key.Trim() + "\" class=\"atab\" style=\"cursor:pointer;\" onclick=\"default_tab_click(this.parentNode,this," + dr.Key.Trim() + ","+counter.ToString()+");\" >");
                    sb.Append("<div class=\"tab-open-c\" >");
                    sb.Append("<span>" + dr.Value.Trim() + "</span>");
                    sb.Append("<img src=\"imgs/trans.gif\" style=\"height: 20px; width: 2px;\" />");
                    sb.Append("</div>");
                    sb.Append("</td>");
                }
                else 
                {
                    sb.Append("<td catid=\"" + dr.Key.Trim() + "\" class=\"tab\" style=\"cursor:pointer;\" onclick=\"default_tab_click(this.parentNode,this," + dr.Key.Trim() + "," + counter.ToString() + ");\" >");
                    sb.Append("<div class=\"tab-open-c\" >");
                    sb.Append("<span>" + dr.Value.Trim() + "</span>");
                    sb.Append("<img src=\"imgs/trans.gif\" style=\"height: 20px; width: 2px;\" />");
                    sb.Append("</div>");
                    sb.Append("</td>");
                
                }
                counter++;


            }

            
            return sb.ToString();
        }


        public static string getMainOneNews(IDataReader dr, string catid)
        {
            StringBuilder sb = new StringBuilder();

            // <div style="width:100%;font-size:15px;" >

            //<img align="right" src="image2.jpg" style="padding: 10px 10px 10px 10px; height: 145px;width: 180px;" />
            //<div style="color:Red;padding-top:5px;font-size:17px;">
            //    ������ ������� ����� ��������� ��� ������� ���� �����
            //</div>
            //<span>������ �������� ���� ����� ���� ��� ��� ����� �� �����ϡ ���� ���� ����� ��� �����
            //    �������� �����. ����� ���� ������� ������� �� ������ ��� ���� ���� ���� ����� ���
            //    ����� ����� ��� ������� �� ������. ����� ������ '�������' �� ���� ����� �����: '��
            //    ������� ���� ��� ����� �������� ������ɡ ��� ��� ���� �� ������ ������� ��� �� ���������
            //    ��� ���� ������� ������� ��� ���� ����� ��� ���� ������'. ����� ������� [���]: ��
            //    ������ ������ �������� ���� ��� ��� ����� ��� ����� �� ������ ���� ������ ���ɡ
            //    ��� ����� '����� �������' �� ����� ������ ��� ���� ����� ��� ����� �� ����� �� ���
            //    ��������� ��� ��� ����� ������ ������� ��� ������ ������ ����� ���� �� ������ �������
            //    ����� ������ ������� ��� �������� ��� ����� ��� ������� �����. ���� ������� �� �������
            //    �� ���� ����� ���� �� ������� �� ������ ����: �� ���������� ���� ���� �� ������
            //    ����� �� ��� ����� ��� ���� ��� ����. </span>

            //  </div>




            if (dr.Read())
            {
                sb.Append("<table style=\"width:100%;font-size:100%;\" class=\"mainonenews\"><tr><td colspan='2'>");
                sb.Append("<img align=\"right\" src=\"" + dr["image_url"].ToString() + "\" style=\"padding: 10px 10px 10px 10px;height:145px;width:180px;\" />");
                sb.Append("<div style=\"color:Red;padding-top:5px;font-size:120%;\" class='mainonenews'>");
                sb.Append(string.Format("<a href='article1.aspx?id={0}'>", dr["id"].ToString()));
                sb.Append(dr["title"].ToString());
                sb.Append("</a>");
                sb.Append("</div>");
                sb.Append("<span >");
                sb.Append(dr["mybody"].ToString());
                sb.Append(string.Format("</span>{0}", string.Format("...<a href='article1.aspx?id={0}'>������</a>", dr["id"].ToString())));
                sb.Append("</td></tr>");
                for (int i = 0; i < 2; i++)
                {
                    dr.Read();
                    sb.Append("<tr>");
                    sb.Append("<td valign='top' style=\"border-top:dotted 1px black;width:50%;\">");
                    sb.Append("<table width='100%'><tr><td><div>");
                    sb.Append(string.Format("<a style=\"color:#024771;font-size:110%;font-weight:bold;\" href='article1.aspx?id={0}'>", dr["id"].ToString()));
                    sb.Append(dr["title"].ToString());
                    sb.Append("</a>");
                    sb.Append("</div></td></tr></table><table width='100%'><tr><td><div style=\"font-size:100%\">");
                    sb.Append(dr["apprev"].ToString());
                    sb.Append("</div></td>");
                    if (!string.IsNullOrEmpty(dr["image_url"].ToString()))
                    {
                        sb.Append("<td align='left'>");
                        sb.Append("<img src='" + mytext.getthumbname(dr["image_url"].ToString().Trim()) + "' width='95' height='85' />");
                        sb.Append("</td>");
                    }
                    sb.Append("</tr></table>");
                    sb.Append("</td>");
                    dr.Read();
                    sb.Append("<td valign='top' style=\"border-top:dotted 1px black;width:50%;\">");
                    sb.Append("<table width='100%'><tr><td><div >");
                    sb.Append(string.Format("<a style=\"color:#024771;font-size:110%;font-weight:bold;\" href='article1.aspx?id={0}'>", dr["id"].ToString()));
                    sb.Append(dr["title"].ToString());
                    sb.Append("</a>");
                    sb.Append("</div></td></tr></table><table width='100%'><tr><td><div style=\"font-size:100%\">");
                    sb.Append(dr["apprev"].ToString());
                    sb.Append("</div></td>");
                    if (!string.IsNullOrEmpty(dr["image_url"].ToString()))
                    {
                        sb.Append("<td align='left'>");
                        sb.Append("<img src='" + mytext.getthumbname(dr["image_url"].ToString().Trim()) + "' width='95' height='85' />");
                        sb.Append("</td>");
                    }
                    sb.Append("</tr></table>");
                    sb.Append("</td>");

                    sb.Append("</tr>");
                }
                sb.Append("</table>");

            }




            //            <table id="DataList1" class="toparts" cellspacing="0" border="0" width="100%">
            //    <tr>
            //        <td>
            //            <table>
            //                <tr>
            //                    <td align="right" width="1px">
            //                        <img src="cryst/imgs/ar2.jpg" />
            //                    </td>
            //                    <td align="right">
            //                        بعد أن أوثقت يديه وقدميه.. الاحتلال يعدم طفلاً
            //                        سنيًا بالرمادي ة
            //                    </td>
            //                </tr>
            //            </table>
            //        </td>
            //        <td>
            //            <table>
            //                <tr>
            //                    <td align="right" width="1px">
            //                        <img src="cryst/imgs/ar2.jpg" />
            //                    </td>
            //                    <td align="right">
            //                        ثلاث عمليات استشهادية تستهدف مواقع للاحتلال
            //                        في حديثة
            //                    </td>
            //                </tr>
            //            </table>
            //        </td>
            //    </tr>
            //</table>
            //StringBuilder sblist1 = new StringBuilder("<ul style='list-style-image:url(cryst/imgs/ar2.jpg);'>");
            //StringBuilder sblist2 = new StringBuilder("<ul style='list-style-image:url(cryst/imgs/ar2.jpg);'>");
            //sb.Append("<table class=\"toparts\" cellspacing=\"0\" border=\"0\" width=\"100%\"><tr><td style='width:50%' align='right'>");
            sb.Append("<table class='toparts' cellspacing='0' border='0' width='100%'>");
            while (dr.Read())
            {
                sb.Append("<tr>");
                //sblist1.Append(string.Format("<li class='stablinks'><a href='article1.aspx?id={0}'>{1}</a></li>", dr["id"].ToString(), dr["title"].ToString()));
                sb.Append(string.Format("<td style='width:15px;'><img src='cryst/imgs/ar2.jpg' /></td><td><a href='article1.aspx?id={0}'>{1}</a></td>", dr["id"].ToString(), dr["title"].ToString()));
                if (dr.Read())
                {
                    //sblist2.Append(string.Format("<li class='stablinks'><a href='article1.aspx?id={0}'>{1}</a></li>", dr["id"].ToString(), dr["title"].ToString()));
                    sb.Append(string.Format("<td style='width:15px;'><img src='cryst/imgs/ar2.jpg' /></td><td><a href='article1.aspx?id={0}'>{1}</a></td>", dr["id"].ToString(), dr["title"].ToString()));
                }
                else
                {
                    sb.Append("<td colspan='2'></td>");
                }
                sb.Append("</tr>");
            }
            //sblist1.Append("</ul>");
            //sblist2.Append("</ul>");
            //sb.Append(sblist1.ToString() + "</td><td style='width:50%' align='right'>" + sblist2.ToString());


            //sb.Append(string.Format("</td></tr><tr><td colspan='2' class='nolinks' align='left'><a href='cat1.aspx?id={0}'>������</a></td></tr>",catid));
            sb.Append(string.Format("<tr><td colspan='4' class='nolinks' align='left'><a href='cat1.aspx?id={0}'>������</a></td></tr>", catid));
            sb.Append("</table>");

            dr.Close();
            return sb.ToString();
        }
        public static string getMainOneNews2(IDataReader dr, string catid)
        {
            StringBuilder sb = new StringBuilder();

            if (dr.Read())
            {
                sb.Append("<table style=\"width:100%;font-size:100%;\" class=\"mainonenews\"><tr><td colspan='2'>");
                sb.Append("<img align=\"right\" src=\"" + dr["image_url"].ToString() + "\" style=\"padding: 10px 10px 10px 10px;height:145px;width:180px;\" />");
                sb.Append("<div style=\"color:Red;padding-top:5px;font-size:120%;\" class='mainonenews'>");
                sb.Append(string.Format("<a href='article1.aspx?id={0}'>", dr["id"].ToString()));
                sb.Append(dr["title"].ToString());
                sb.Append("</a>");
                sb.Append("</div>");
                sb.Append("<span >");
                sb.Append(dr["mybody"].ToString());
                sb.Append(string.Format("</span>{0}", string.Format("...<a href='article1.aspx?id={0}'>������</a>", dr["id"].ToString())));
                sb.Append("</td></tr>");
                for (int i = 0; i < 3; i++)
                {
                    dr.Read();
                    sb.Append("<tr>");
                    sb.Append("<td valign='top' style=\"border-top:dotted 1px black;width:50%;\">");
                    sb.Append("<table width='100%'><tr><td><div>");
                    sb.Append(string.Format("<a style=\"color:#024771;font-size:110%;font-weight:bold;\" href='article1.aspx?id={0}'>", dr["id"].ToString()));
                    sb.Append(dr["title"].ToString());
                    sb.Append("</a>");
                    sb.Append("</div></td></tr></table><table width='100%'><tr><td><div style=\"font-size:100%\">");
                    sb.Append(dr["apprev"].ToString());
                    sb.Append("</div></td>");
                    if (!string.IsNullOrEmpty(dr["image_url"].ToString()))
                    {
                        sb.Append("<td align='left'>");
                        sb.Append("<img style='border:solid 2px gray;' src='" + mytext.getthumbname(dr["image_url"].ToString().Trim()) + "' width='95' height='85' />");
                        sb.Append("</td>");
                    }
                    sb.Append("</tr></table>");
                    sb.Append("</td>");
                    dr.Read();
                    sb.Append("<td valign='top' style=\"border-top:dotted 1px black;width:50%;\">");
                    sb.Append("<table width='100%'><tr><td><div >");
                    sb.Append(string.Format("<a style=\"color:#024771;font-size:110%;font-weight:bold;\" href='article1.aspx?id={0}'>", dr["id"].ToString()));
                    sb.Append(dr["title"].ToString());
                    sb.Append("</a>");
                    sb.Append("</div></td></tr></table><table width='100%'><tr><td><div style=\"font-size:100%\">");
                    sb.Append(dr["apprev"].ToString());
                    sb.Append("</div></td>");
                    if (!string.IsNullOrEmpty(dr["image_url"].ToString()))
                    {
                        sb.Append("<td align='left'>");
                        sb.Append("<img style='border:solid 2px gray;' src='" + mytext.getthumbname(dr["image_url"].ToString().Trim()) + "' width='95' height='85' />");
                        sb.Append("</td>");
                    }
                    sb.Append("</tr></table>");
                    sb.Append("</td>");

                    sb.Append("</tr>");
                }
                sb.Append("</table>");

            }



            sb.Append("<table class='toparts' cellspacing='0' border='0' width='100%'>");
            while (dr.Read())
            {
                sb.Append("<tr>");
                //sblist1.Append(string.Format("<li class='stablinks'><a href='article1.aspx?id={0}'>{1}</a></li>", dr["id"].ToString(), dr["title"].ToString()));
                sb.Append(string.Format("<td style='width:15px;'><img src='cryst/imgs/ar2.jpg'  style='margin-top:8px;' /></td><td style='padding-top:5px;padding-bottom:5px;'><a href='article1.aspx?id={0}'>{1}</a></td>", dr["id"].ToString(), dr["title"].ToString()));
                if (dr.Read())
                {
                    //sblist2.Append(string.Format("<li class='stablinks'><a href='article1.aspx?id={0}'>{1}</a></li>", dr["id"].ToString(), dr["title"].ToString()));
                    sb.Append(string.Format("<td style='width:15px;'><img src='cryst/imgs/ar2.jpg' style='margin-top:8px;' /></td><td style='padding-top:5px;padding-bottom:5px;'><a href='article1.aspx?id={0}'>{1}</a></td>", dr["id"].ToString(), dr["title"].ToString()));
                }
                else
                {
                    sb.Append("<td colspan='2'></td>");
                }
                sb.Append("</tr>");
            }
            //sblist1.Append("</ul>");
            //sblist2.Append("</ul>");
            //sb.Append(sblist1.ToString() + "</td><td style='width:50%' align='right'>" + sblist2.ToString());


            //sb.Append(string.Format("</td></tr><tr><td colspan='2' class='nolinks' align='left'><a href='cat1.aspx?id={0}'>������</a></td></tr>",catid));
            sb.Append(string.Format("<tr><td colspan='4' class='nolinks' align='left'><a href='cat1.aspx?id={0}'>������</a></td></tr>", catid));
            sb.Append("</table>");

            dr.Close();
            return sb.ToString();
        }
        public static string getMainNews(IDataReader dr, string catid)
        {
            StringBuilder sb = new StringBuilder();


            sb.Append("<table style=\"width:100%;font-size:100%;\" class=\"mainonenews\">");

            for (int i = 0; i < 3; i++)
            {
                dr.Read();
                sb.Append("<tr>");
                sb.Append("<td valign='top' style=\"border-top:dotted 1px black;width:50%;\">");
                sb.Append("<table width='100%'><tr><td><div>");
                sb.Append(string.Format("<a style=\"color:#024771;font-size:110%;font-weight:bold;\" href='article1.aspx?id={0}'>", dr["id"].ToString()));
                sb.Append(dr["title"].ToString());
                sb.Append("</a>");
                sb.Append("</div></td></tr></table><table width='100%'><tr><td><div style=\"font-size:100%\">");
                sb.Append(dr["apprev"].ToString());
                sb.Append("</div></td>");
                if (!string.IsNullOrEmpty(dr["image_url"].ToString()))
                {
                    sb.Append("<td align='left'>");
                    sb.Append("<img src='" + mytext.getthumbname(dr["image_url"].ToString().Trim()) + "' width='95' height='85' />");
                    sb.Append("</td>");
                }
                sb.Append("</tr></table>");
                sb.Append("</td>");
                dr.Read();
                sb.Append("<td valign='top' style=\"border-top:dotted 1px black;width:50%;\">");
                sb.Append("<table width='100%'><tr><td><div >");
                sb.Append(string.Format("<a style=\"color:#024771;font-size:110%;font-weight:bold;\" href='article1.aspx?id={0}'>", dr["id"].ToString()));
                sb.Append(dr["title"].ToString());
                sb.Append("</a>");
                sb.Append("</div></td></tr></table><table width='100%'><tr><td><div style=\"font-size:100%\">");
                sb.Append(dr["apprev"].ToString());
                sb.Append("</div></td>");
                if (!string.IsNullOrEmpty(dr["image_url"].ToString()))
                {
                    sb.Append("<td align='left'>");
                    sb.Append("<img src='" + mytext.getthumbname(dr["image_url"].ToString().Trim()) + "' width='95' height='85' />");
                    sb.Append("</td>");
                }
                sb.Append("</tr></table>");
                sb.Append("</td>");

                sb.Append("</tr>");
            }
            sb.Append("</table>");






            //            <table id="DataList1" class="toparts" cellspacing="0" border="0" width="100%">
            //    <tr>
            //        <td>
            //            <table>
            //                <tr>
            //                    <td align="right" width="1px">
            //                        <img src="cryst/imgs/ar2.jpg" />
            //                    </td>
            //                    <td align="right">
            //                        بعد أن أوثقت يديه وقدميه.. الاحتلال يعدم طفلاً
            //                        سنيًا بالرمادي ة
            //                    </td>
            //                </tr>
            //            </table>
            //        </td>
            //        <td>
            //            <table>
            //                <tr>
            //                    <td align="right" width="1px">
            //                        <img src="cryst/imgs/ar2.jpg" />
            //                    </td>
            //                    <td align="right">
            //                        ثلاث عمليات استشهادية تستهدف مواقع للاحتلال
            //                        في حديثة
            //                    </td>
            //                </tr>
            //            </table>
            //        </td>
            //    </tr>
            //</table>
            //StringBuilder sblist1 = new StringBuilder("<ul style='list-style-image:url(cryst/imgs/ar2.jpg);'>");
            //StringBuilder sblist2 = new StringBuilder("<ul style='list-style-image:url(cryst/imgs/ar2.jpg);'>");
            //sb.Append("<table class=\"toparts\" cellspacing=\"0\" border=\"0\" width=\"100%\"><tr><td style='width:50%' align='right'>");
            sb.Append("<table class='toparts' cellspacing='0' border='0' width='100%'>");
            while (dr.Read())
            {
                sb.Append("<tr>");
                //sblist1.Append(string.Format("<li class='stablinks'><a href='article1.aspx?id={0}'>{1}</a></li>", dr["id"].ToString(), dr["title"].ToString()));
                sb.Append(string.Format("<td style='width:15px;'><img src='cryst/imgs/ar2.jpg' /></td><td><a href='article1.aspx?id={0}'>{1}</a><br /><br /></td>", dr["id"].ToString(), dr["title"].ToString()));
                if (dr.Read())
                {
                    //sblist2.Append(string.Format("<li class='stablinks'><a href='article1.aspx?id={0}'>{1}</a></li>", dr["id"].ToString(), dr["title"].ToString()));
                    sb.Append(string.Format("<td style='width:15px;'><img src='cryst/imgs/ar2.jpg' /></td><td><a href='article1.aspx?id={0}'>{1}</a><br /><br /></td>", dr["id"].ToString(), dr["title"].ToString()));
                }
                else
                {
                    sb.Append("<td colspan='2'></td>");
                }
                sb.Append("</tr>");
            }
            //sblist1.Append("</ul>");
            //sblist2.Append("</ul>");
            //sb.Append(sblist1.ToString() + "</td><td style='width:50%' align='right'>" + sblist2.ToString());


            //sb.Append(string.Format("</td></tr><tr><td colspan='2' class='nolinks' align='left'><a href='cat1.aspx?id={0}'>������</a></td></tr>",catid));
            sb.Append(string.Format("<tr><td colspan='4' class='nolinks' align='left'><a href='cat1.aspx?id={0}'>������</a></td></tr>", catid));
            sb.Append("</table>");

            dr.Close();
            return sb.ToString();
        }

        public static string getCatSlideShow(SqlDataReader dr,HttpServerUtility myserver) 
        {
        
//        <div style="background-image:url('cryst/imgs/slidshow_bg.gif'); width:100%; margin:5px 0px 5px 0px;border: 2px solid #6e8ca8;">

//<div style="width:100%;text-align:center;color:Yellow;font-size:17px;">
//<img  src="cryst/imgs/news1.jpg" style="margin-top:5px;" /></div>

//<table width="100%" dir=rtl>
//<tr>

//<td>
//<div id="slidetitle" style="font-size:20px;font-weight:bold;color:#024771;">

//���� ����� ���� ���� ����� ������ ��������</div>
//<div style="font-size:17px;" id="slidesubj">
 
//����� ������� [���]: ������ ���� ����� ������� ����� ������ �������� �������� �������� ��� ������ ����� �� ���� ������� ���� ��� ����� ������� ������� ��� ����� �� ������� �������� �����.
//���� ����� '����� �������' - ����� �� ���� ���� �� ����� ����� ����������� �� ���� ������� - �� ���� ����� ���� ��� 

//</div>
//</td>
//<td width="1">
//<img id="img1" width="200" height="170" id="slideimg" style="filter:progid:DXImageTransform.Microsoft.Fade(Duration=2)"  src="http://www.islammemo.cc/news/newsimages/al-jazerra/Iraqi-soldiers-patrol.jpg"/>
//</td>
//</tr>
//</table>
//<div style="width:100%;text-align:center;color:Yellow;font-size:17px;">
//<%--<a href="javascript:bprev()">--%>
//<img src="cryst/imgs/prev.jpg" border=0 onclick="bprev();" style="cursor:hand;"/>
//<%--</a>--%>
//<%--<a href="javascript:bnext()">--%>
//<img src="cryst/imgs/next.jpg" border=0 onclick="bnext();" style="cursor:hand;"/>
//<%--</a>--%>
//</div>
//</div>

//<script type="text/javascript">slidestart();</script>

            if (!dr.Read())
            {
                return "";
            }
            StringBuilder sb = new StringBuilder();

            //start of new 
            sb.Append("<style>.isl01{font-weight:800;color:#f6db99;}.isl02 {font-weight:800;color:#FFFFFF;}.isl03{font-weight:800;color:#000000;padding-right:5px;}.myactive{text-decoration:underline;}</style><div style='width:500px;'></div>");

            sb.Append("<table dir='rtl' border='0' width='100%' cellpadding='0' style='border-collapse: collapse' height='100'>");
            sb.Append("<tr>");
            sb.Append("<td bgcolor='#0090DF' bordercolor='#000000' style='border: 1px solid #000000; padding: 5px' valign='top'>");
            sb.Append("<table border='0' width='100%' cellpadding='0' style='border-collapse: collapse'>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("<table border='0' width='100%' cellpadding='0' style='border-collapse: collapse'>");
            sb.Append("<tr>");
            sb.Append("<td class='isl03' bgcolor='#3BAAE6'><span >��� ��������</span></td>");
            sb.Append("<td bgcolor='#3BAAE6'>");
            sb.Append("<img border='0' src='cryst/imgs/lastest01.jpg' width='21' height='29'></td>");//change image src
            sb.Append("<td id='slidetitle' bgcolor='#3BAAE6' class='isl01'>");
            sb.Append(string.Format("<a style='color:#f6db99;' href='article1.aspx?id={0}'>", dr["id"].ToString()));//style of the link
            sb.Append(myserver.HtmlEncode(dr[1].ToString()));
            sb.Append("</a>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("<table border='0' width='100%' cellpadding='0' style='border-collapse: collapse'>");
            sb.Append("<tr>");
            sb.Append("<td id='slidesubj' class='isl02' style='padding: 5px'>");
            sb.Append(myserver.HtmlEncode(dr[2].ToString()));
            sb.Append("</td>");

            if (!string.IsNullOrEmpty(dr[3].ToString()))
            {

                sb.Append("<td align='left' style='padding: 5px'>");
                sb.Append("<div style='height:244px;'><img border='0' id=\"img1\" style=\"filter:progid:DXImageTransform.Microsoft.Fade(Duration=2)\"  src='" + dr[3].ToString() + "' width='300' height='234' style='border: 1px solid #FFFFFF'></div></td>");//chang src of image
            }
            else
            {
                sb.Append("<td align='left' style='padding: 5px'>");
                sb.Append("<div style='height:244px;'><img border='0' id=\"1mg1\" style=\"display:none;filter:progid:DXImageTransform.Microsoft.Fade(Duration=2)\"  src='" + dr[3].ToString() + "' width='300' height='234' style='border: 1px solid #FFFFFF'></div></td>");//chang src of image
            }
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style='padding-right: 10px'>");
            sb.Append("<table border='0' width='300' cellpadding='0' style='border-collapse: collapse'>");
            sb.Append("<tr>");
            sb.Append("<td width='5'>");
            sb.Append("<img border='0' src='cryst/imgs/lastest04.jpg' width='16' height='27'></td>");
            sb.Append("<td background='cryst/imgs/lastest08.jpg' width='100%'>");
            sb.Append("<table border='0' width='100%' cellpadding='0' style='border-collapse: collapse'>");
            sb.Append("<tr>");
            sb.Append("<td align='center'><span id='plc1' style='cursor:pointer;'>1</span></td>");
            sb.Append("<td align='center'><span id='plc2' style='cursor:pointer;'>2</span></td>");
            sb.Append("<td align='center'><span id='plc3' style='cursor:pointer;'>3</span></td>");
            sb.Append("<td width='20' align='center'>");
            sb.Append("<img border='0' src='cryst/imgs/lastest06.jpg' style='cursor:pointer;' width='14' height='27' onclick=\"bprev();\"></td>");
            sb.Append("<td width='20' align='center'>");
            sb.Append("<img border='0' src='cryst/imgs/lastest07.jpg' style='cursor:pointer;' width='7' height='27' onclick=\"window.clearTimeout(timer);\"></td>");
            sb.Append("<td width='20' align='center'>");
            sb.Append("<img border='0' src='cryst/imgs/lastest05.jpg' style='cursor:pointer;' width='14' height='27' onclick=\"bnext();\"></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</td>");
            sb.Append("<td width='5'>");
            sb.Append("<img border='0' src='cryst/imgs/lastest03.jpg' width='16' height='27'></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");




            /////////////////////
            sb.Append("<script>");
            sb.Append("var titles=new Array();var imgs=new Array();var sbj=new Array();");
            sb.Append("var timer;var hhh=0;pic1= new Image(100,25);");
            //function next previos
            sb.Append("function next(){if(hhh<titles.length-1){hhh++;}else{hhh=0;}update();}function prev(){if(hhh>0){hhh--;}else{hhh=titles.length-1;}update();}");
            //function update
            sb.Append("function update(){try{document.getElementById('img1').filters[0].apply();document.getElementById('img1').filters[0].play();}catch(e){} if (imgs[hhh].length>1){document.getElementById('img1').style.display='';}else{document.getElementById('img1').style.display='none';} document.getElementById('img1').src=imgs[hhh];document.getElementById('slidesubj').innerHTML=sbj[hhh];document.getElementById('slidetitle').innerHTML=titles[hhh];document.getElementById('plc1').className='';document.getElementById('plc2').className='';document.getElementById('plc3').className='';document.getElementById('plc'+(hhh+1)).className='myactive';}");
            //function slidestart bnext bprev
            sb.Append("function slidestart(){timer=window.setTimeout( \"slidestart()\", 5000);if(document.getElementById('img1').complete){next();}}function bnext(){next();clearTimeout(timer);timer=window.setTimeout( \"slidestart()\", 8000);}function bprev(){prev();clearTimeout(timer);timer=window.setTimeout( \"slidestart()\", 8000);}");



            sb.Append(string.Format("titles[0]=\"<a style='color:#f6db99;' href='article1.aspx?id={0}'>{1}</a>\";", dr["id"].ToString(), myserver.HtmlEncode(dr["title"].ToString().Replace(Environment.NewLine, "<br/>"))));
            sb.Append("imgs[0]=\"" + dr[3].ToString() + "\";");
            sb.Append("pic1.src=\"" + dr[3].ToString() + "\";");
            sb.Append("sbj[0]=\"" + myserver.HtmlEncode(dr[2].ToString().Trim().Replace(Environment.NewLine, "<br/>")) + "\";");


            int counter = 0;
            while (dr.Read())
            {
                counter++;

                sb.Append(string.Format("titles[{0}]=\"<a style='color:#f6db99;' href='article1.aspx?id={1}'>{2}</a>\";", counter.ToString(), dr["id"].ToString(), myserver.HtmlEncode(dr["title"].ToString().Replace(Environment.NewLine, "<br/>"))));
                sb.Append("imgs[" + counter.ToString() + "]=\"" + dr[3].ToString() + "\";");
                sb.Append("pic1.src=\"" + dr[3].ToString() + "\";");
                sb.Append("sbj[" + counter.ToString() + "]=\"" + myserver.HtmlEncode(dr[2].ToString().Trim().Replace(Environment.NewLine, "<br/>")) + "\";");
                if (counter > 1) { break; }
            }

            sb.Append("</script>");


            sb.Append("<script type=\"text/javascript\">slidestart();</script>");



            //end of new
           




//            <script>
//var titles=new Array();
//titles[0]="���������: ������ ���� ����� ������ ������� ������� ��� �����";
//titles[1]="����� ����� ������� �� ������ ���� ����� ���� �������� ";
//titles[2]="���� ���� ����� �� ������ �������� ���� ����� ";

//var imgs=new Array();
//imgs[0]="http://www.islammemo.cc/news/newsimages/Lebanon/Israeli_aggression9.jpg";
//pic1= new Image(100,25); 
//pic1.src="http://www.islammemo.cc/news/newsimages/Lebanon/Israeli_aggression9.jpg"; 

//imgs[1]="http://www.islammemo.cc/news/newsimages/iraq2/burned_truck_attacked.jpg";
//pic1.src="http://www.islammemo.cc/news/newsimages/iraq2/burned_truck_attacked.jpg"; 

//imgs[2]="http://www.islammemo.cc/news/newsimages/Fallujah/Armed_Iraqi_insurgents.jpg";
//pic1.src="http://www.islammemo.cc/news/newsimages/Fallujah/Armed_Iraqi_insurgents.jpg"; 




//var sbj=new Array();
//sbj[0]="����� �������: ���� ����� '���������' �������� �� ������� ��������� ���� �� ���� ������� ��� ������� ������� ��� ����� ��� ����� �� �������� ������� ����";
//sbj[1]="����� ������� [���]: ������ ���� ����� ������� ����� �� �������� ������� ������� ������� ��������� ���";
//sbj[2]="����� ������� [���]: ��� ������ �� ����� ���� ������ �������� ������� ������ ��� ������� ����� �� ���";

//var timer;var hhh=0;
//function next()
//{
//    if(hhh<titles.length-1)
//    {
//        hhh++;
//    }else
//    {
//        hhh=0;
//    }
//    update();
//}
//function prev()
//{
//    if(hhh>0)
//    {
//    hhh--;
//    }else
//    {
//        hhh=titles.length-1;
//    }
//    update();
//}
//function update()
//{
    
////    document.getElementById('img1').style.filters="blendTrans(duration=1);";
    
//    try{
//    document.getElementById('img1').filters[0].apply();

//    document.getElementById('img1').filters[0].play();
//    }catch(e){}
            //if (imgs[hhh].length>1){document.getElementById('img1').style.display='';}else{document.getElementById('img1').style.display='none';}
//    document.getElementById('img1').src=imgs[hhh];
//    document.getElementById('slidesubj').innerHTML=sbj[hhh];
//    document.getElementById('slidetitle').innerHTML=titles[hhh];
//}

//function slidestart()
//{
//    next();
//    timer=window.setTimeout( "slidestart()", 5000);
//}
//function bnext()
//{
//    next();
//    clearTimeout(timer);
//    timer=window.setTimeout( "slidestart()", 8000);
//}
//function bprev()
//{
//    prev();
//    clearTimeout(timer);
//    timer=window.setTimeout( "slidestart()", 8000);
//}


//</script>


           


    //        <table dir=rtl width=100% cellpadding="0" cellspacing="0" >
    //    <tr >
    //        <td style="border-top:dotted 1px black;">
            
    //            <table>
    //                <tr>
    //                    <td>
    //                    <div style="color:#024771;font-size:16px;font-weight:bold;">
    //                    &#1592;�&#1592;�&#1591;�&#1591;� &#1591;�&#1591;�&#1591;�&#1592;�&#1592;� &#1592;&#1657;&#1592;�&#1591;&#1726;&#1592;� &#1591;�&#1592;�&#1591;�&#1591;� &#1591;�&#1592;�&#1591;�&#1592;&#1657;&#1592;�&#1592;&#1657;&#1591;� &#1592;&#1662;&#1592;� &#1591;�&#1592;�&#1592;�&#1592;�&#1591;�&#1592;�    
    //                    </div>
    //                    <div style="font-size:16px;">
    //                    &#1592;�&#1592;&#1662;&#1592;�&#1591;�&#1591;� &#1591;�&#1592;�&#1591;�&#1591;�&#1592;�&#1591;�&#1592;� [&#1591;�&#1591;�&#1591;�]: &#1592;�&#1591;�&#1592;&#1662;&#1591;&#1726; &#1591;�&#1592;�&#1592;�&#1592;�&#1591;�&#1592;�&#1592;�&#1591;� &#1591;�&#1592;�&#1591;�&#1591;�&#1591;�&#1592;�&#1592;&#1657;&#1591;� &#1592;�&#1592;�&#1591;�&#1592;&#1657;&#1592;� &#1592;�&#1592;�&#1592;�&#1591;�&#1591;&#1726; &#1592;�&#1591;&#1563;&#1591;�&#1592;�&#1592;&#1657;&#1591;� &#1591;�&#1592;�&#1591;�&#1591;�&#1591;�&#1592;�&#1592;&#1657;&#1591;� &#1591;�&#1592;�&#1592;�&#1592;�&#1591;�&#1592;�&#1592;&#1657;&#1591;� &#1592;�&#1592;�&#1591;�&#1591;�&#1591;&#1726;&#1592;�&#1591;�&#1592;� &#1591;�&#1592;�&#1591;�&#1592;�&#1591;�&#1592;&#1657;&#1592;�&#1592;&#1657; &#1592;&#1662;&#1592;&#1657; ... 
    //                    </div>
    //                    </td>
    //                    <td>
    //                    <img src='http://www.islammemo.cc/news/newsimages/Iraq/explosions_s.jpg' width='95' height='85' />
    //                    </td>
    //                </tr>
    //            </table>
            
    //        </td>
    //        <td style="border-top:dotted 1px black;">
            
    //            <table>
    //                <tr>
    //                    <td>
    //                    <div style="color:#024771;font-size:16px;font-weight:bold;">
    //                    &#1592;�&#1592;�&#1591;�&#1591;� &#1591;�&#1591;�&#1591;�&#1592;�&#1592;� &#1592;&#1657;&#1592;�&#1591;&#1726;&#1592;� &#1591;�&#1592;�&#1591;�&#1591;� &#1591;�&#1592;�&#1591;�&#1592;&#1657;&#1592;�&#1592;&#1657;&#1591;� &#1592;&#1662;&#1592;� &#1591;�&#1592;�&#1592;�&#1592;�&#1591;�&#1592;�    
    //                    </div>
    //                    <div style="font-size:16px;">
    //                    &#1592;�&#1592;&#1662;&#1592;�&#1591;�&#1591;� &#1591;�&#1592;�&#1591;�&#1591;�&#1592;�&#1591;�&#1592;� [&#1591;�&#1591;�&#1591;�]: &#1592;�&#1591;�&#1592;&#1662;&#1591;&#1726; &#1591;�&#1592;�&#1592;�&#1592;�&#1591;�&#1592;�&#1592;�&#1591;� &#1591;�&#1592;�&#1591;�&#1591;�&#1591;�&#1592;�&#1592;&#1657;&#1591;� &#1592;�&#1592;�&#1591;�&#1592;&#1657;&#1592;� &#1592;�&#1592;�&#1592;�&#1591;�&#1591;&#1726; &#1592;�&#1591;&#1563;&#1591;�&#1592;�&#1592;&#1657;&#1591;� &#1591;�&#1592;�&#1591;�&#1591;�&#1591;�&#1592;�&#1592;&#1657;&#1591;� &#1591;�&#1592;�&#1592;�&#1592;�&#1591;�&#1592;�&#1592;&#1657;&#1591;� &#1592;�&#1592;�&#1591;�&#1591;�&#1591;&#1726;&#1592;�&#1591;�&#1592;� &#1591;�&#1592;�&#1591;�&#1592;�&#1591;�&#1592;&#1657;&#1592;�&#1592;&#1657; &#1592;&#1662;&#1592;&#1657; ... 
    //                    </div>
    //                    </td>
    //                    <td>
    //                    <img src='http://www.islammemo.cc/news/newsimages/Iraq/explosions_s.jpg' width='95' height='85' />
    //                    </td>
    //                </tr>
    //            </table>
    //        </td>
    //    </tr>
    //</table>

            sb.Append("<table dir=rtl width=100% cellpadding=\"0\" cellspacing=\"0\" class='catslideshow'>");
            counter = 0;
            while (dr.Read())
            {
                counter++;
                
                sb.Append("<tr>");

                sb.Append("<td valign='top' style=\"border-bottom:dotted 1px black;width:50%;\">");
                sb.Append("<table width='100%'><tr><td><div style=\"color:#024771;font-size:110%;font-weight:bold;\">");
                sb.Append(string.Format("<a href='article1.aspx?id={0}'>",dr["id"].ToString()));
                sb.Append(dr[1].ToString());
                sb.Append("</a>");
                sb.Append("</div></td></tr></table><table width='100%'><tr><td><div style=\"font-size:100%\">");
                sb.Append(dr[2].ToString());
                sb.Append("</div></td>");
                if (!string.IsNullOrEmpty(dr[3].ToString()))
                {
                    sb.Append("<td align='left'>");
                    sb.Append("<img src='" + mytext.getthumbname(dr[3].ToString().Trim()) + "' width='95' height='85' />");
                    sb.Append("</td>"); 
                }
                sb.Append("</tr></table>");
                sb.Append("</td>");
                if (dr.Read()) 
                {
                    sb.Append("<td valign='top' style=\"border-bottom:dotted 1px black;width:50%;\">");
                    sb.Append("<table width='100%'><tr><td><div style=\"color:#024771;font-size:110%;font-weight:bold;\">");
                    sb.Append(string.Format("<a href='article1.aspx?id={0}'>", dr["id"].ToString()));                    
                    sb.Append(dr[1].ToString());
                    sb.Append("</a>");
                    sb.Append("</div></td></tr></table><table width='100%'><tr><td><div style=\"font-size:100%;\">");
                    sb.Append(dr[2].ToString());
                    sb.Append("</div></td>");
                    if (!string.IsNullOrEmpty(dr[3].ToString()))
                    {
                        sb.Append("<td align='left'>");
                        sb.Append("<img src='" +mytext.getthumbname(dr[3].ToString().Trim()) + "' width='95' height='85' />");
                        sb.Append("</td>");
                    }
                    sb.Append("</tr></table>");
                    sb.Append("</td>");
                }
                else
                {
                    sb.Append("<td style=\"border-top:dotted 1px black;\"></td>");
                }

                sb.Append("</tr>");

                if (counter > 1) { break; }
            }
            sb.Append("</table>");





            return sb.ToString();
        }

        public static string getMainSlideShow(IDataReader dr, HttpServerUtility myserver)
        {

            if (!dr.Read())
            {
                return "";
            }
            StringBuilder sb = new StringBuilder();

            //start of new 
            sb.Append("<div style=\"width:100%;background-color:#e2f4ff;border:solid 1px #0090df;padding:0px;margin:0px;\"><table width=\"100%\"  cellpadding=\"0\"  cellspacing=\"0\"><tr><td style=\"width:150px;\"><img src=\"imgs/main/lastadd.jpg\" style=\"text-align:right;margin:0px;padding:0px;\" /></td><td id=\"slidetitle\" style=\"color:#9f0a0e;font-weight:bold;\" class=\"lastaddtitle\">");

            sb.Append(string.Format("<a  href='article1.aspx?id={0}'>{1}</a>", dr["id"].ToString(), myserver.HtmlEncode(dr[2].ToString())));//style of the link

            sb.Append("</td></tr></table></div>");
            sb.Append("<div style=\"width:100%;background-color:#e2f4ff;border:solid 1px #0090df; border-top-width:0px; padding:0px;margin:0px;\"><table width=\"100%\"  cellpadding=\"0\"  cellspacing=\"0\"><tr><td valign=\"top\" style=\"padding-top:5px;padding-left:5px;padding-right:5px;\"><div id='slidesubj' style=\"width:100%;margin:0px;\">");
            sb.Append(myserver.HtmlEncode(dr["apprev"].ToString()));
            sb.Append(string.Format("</div></td><td rowspan=\"2\" style=\"width:150px;\"><img src=\"{0}\" id=\"img1\" style=\"{1}filter: progid:DXImageTransform.Microsoft.Fade(Duration=2);text-align:left;margin:0px;padding:0px;width:260px;height:180px;border-right:solid 10px #155e8b;\"/></td>", dr[1].ToString(), string.IsNullOrEmpty(dr[1].ToString()) ? "display:none;" : ""));
            sb.Append("</tr><tr><td valign=\"bottom\" align=\"left\" style=\"padding:0px;margin:0px;\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" ><tr><td><img src=\"imgs/main/vcr_r_corn.jpg\" /></td><td style=\"background-image:url('imgs/main/vcr_bg.jpg'); background-repeat:repeat-x; width:20px;text-align:center;\"><img src=\"imgs/main/vcr_prev.jpg\" onclick=\"bprev();\" /></td><td style=\"background-image:url('imgs/main/vcr_bg.jpg'); background-repeat:repeat-x; width:20px;text-align:center;\"><img src=\"imgs/main/vcr_pause.jpg\" onclick=\"window.clearTimeout(timer);\" /></td><td style=\"background-image:url('imgs/main/vcr_bg.jpg'); background-repeat:repeat-x; width:20px;text-align:center;\"><img src=\"imgs/main/vcr_next.jpg\" onclick=\"bnext();\"/></td><td style=\"background-image:url('imgs/main/vcr_bg.jpg'); background-repeat:repeat-x; width:20px;text-align:center;\" id='plc1' class='mynotactive'><a href='javascript://void(0);' onclick='gootoo(0);'>1</a></td><td style=\"background-image:url('imgs/main/vcr_bg.jpg'); background-repeat:repeat-x; width:20px;text-align:center;\" id='plc2' class='mynotactive'><a href='javascript://void(0);' onclick='gootoo(1);'>2</a></td><td style=\"background-image:url('imgs/main/vcr_bg.jpg'); background-repeat:repeat-x; width:20px;text-align:center;\" id='plc3' class='mynotactive'><a href='javascript://void(0);' onclick='gootoo(2);'>3</a></td><td><img src=\"imgs/main/vcr_l_corn.jpg\" /></td><td style=\"width:50px;\"></td></tr></table></td></tr></table></div>");




            /////////////////////
            sb.Append("<script>");
            sb.Append("var titles=new Array();var imgs=new Array();var sbj=new Array();");
            sb.Append("var timer;var hhh=0;pic1= new Image(100,25);");
            //function next previos
            sb.Append("function next(){if(hhh<titles.length-1){hhh++;}else{hhh=0;}update();}function prev(){if(hhh>0){hhh--;}else{hhh=titles.length-1;}update();}");
            //function update
            sb.Append("function gootoo(aa){hhh=aa;update();clearTimeout(timer);} function update(){try{document.getElementById('img1').filters[0].apply();document.getElementById('img1').filters[0].play();}catch(e){} if (imgs[hhh].length>1){document.getElementById('img1').style.display='';}else{document.getElementById('img1').style.display='none';} document.getElementById('img1').src=imgs[hhh];document.getElementById('slidesubj').innerHTML=sbj[hhh];document.getElementById('slidetitle').innerHTML=titles[hhh];document.getElementById('plc1').className='mynotactive';document.getElementById('plc2').className='mynotactive';document.getElementById('plc3').className='mynotactive';document.getElementById('plc'+(hhh+1)).className='myactive';}");
            //function slidestart bnext bprev
            sb.Append("function slidestart(){timer=window.setTimeout( \"slidestart()\", 5000);if(document.getElementById('img1').complete){next();}}function bnext(){next();clearTimeout(timer);timer=window.setTimeout( \"slidestart()\", 8000);}function bprev(){prev();clearTimeout(timer);timer=window.setTimeout( \"slidestart()\", 8000);}");



            sb.Append(string.Format("titles[0]=\"<a  href='article1.aspx?id={0}'>{1}</a>\";", dr["id"].ToString(), myserver.HtmlEncode(dr["title"].ToString().Replace(Environment.NewLine, "<br/>"))));
            sb.Append("imgs[0]=\"" + dr[1].ToString() + "\";");
            sb.Append("pic1.src=\"" + dr[1].ToString() + "\";");
            sb.Append("sbj[0]=\"" + myserver.HtmlEncode(dr["apprev"].ToString().Trim().Replace(Environment.NewLine, "<br/>")) + "\";");


            int counter = 0;
            while (dr.Read())
            {
                counter++;

                sb.Append(string.Format("titles[{0}]=\"<a  href='article1.aspx?id={1}'>{2}</a>\";", counter.ToString(), dr["id"].ToString(), myserver.HtmlEncode(dr["title"].ToString().Replace(Environment.NewLine, "<br/>"))));
                sb.Append("imgs[" + counter.ToString() + "]=\"" + dr[1].ToString() + "\";");
                sb.Append("pic1.src=\"" + dr[1].ToString() + "\";");
                sb.Append("sbj[" + counter.ToString() + "]=\"" + myserver.HtmlEncode(dr["apprev"].ToString().Trim().Replace(Environment.NewLine, "<br/>")) + "\";");
                if (counter > 1) { break; }
            }

            sb.Append("</script>");


            sb.Append("<script type=\"text/javascript\">slidestart();</script>");










            return sb.ToString();
        }

        public static string getCatmorenews(SqlDataReader dr,int catid,string articlepage)
        {

            StringBuilder sb = new StringBuilder();

            sb.Append("<table width=\"100%\" dir=\"rtl\" cellpadding=\"0\" cellspacing=\"0\" style=\"height:30px;;background-image:url('cryst/imgs/m_p_header_bg.jpg');margin-top:5px;\" ><tr><td></td></tr></table>");


            //<table width="100%" cellpadding="0" cellspacing="0" dir="rtl">
            //<tr>
            //    <td align=center style="color:#ffa92f;">
            //        10:10
            //    </td>
            //    <td style="background-color:#f7f7f7;font-size:16px;padding-right:5px;color:#024771;">
            //        عريقات متى يتحرك صناع القرار 
            //        العربى
            //    </td>
            //    <td align=center style="color:#ffa92f;">
            //        10:10
            //    </td>
            //    <td style="background-color:#f7f7f7;font-size:16px;padding-right:5px;color:#024771;">
            //        عريقات متى يتحرك صناع القرار 
            //        العربى
            //    </td>
            //</tr>
            //</table>
            int counter = 0;
            int counter25 = 0;
            int counterarch = 0;

            sb.Append("<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" dir=\"rtl\">");
            while (dr.Read())
            {
                counter++;
                counter25++;
                counterarch++;

                if (counter25 < 14)
                {
                    sb.Append("<tr>");

                    sb.Append("<td align=center style=\"color:#ffa92f;\">");
                    if (!dr.IsDBNull(4))
                    {
                        sb.Append(dr.GetDateTime(4).Hour.ToString() + ":" + dr.GetDateTime(4).Minute.ToString());
                    }
                    sb.Append("</td><td class='artboxs' width='47%' style=\"background-color:#f7f7f7;padding-right:5px;color:#024771;\">");
                    sb.Append("<a href='" + articlepage + "?id=" + dr["id"].ToString() + "'>");
                    sb.Append(dr[1].ToString());
                    sb.Append("</a>");
                    sb.Append("</td>");

                    if (dr.Read())
                    {
                        sb.Append("<td align=center style=\"color:#ffa92f;\">");
                        if (!dr.IsDBNull(4))
                        {
                            sb.Append(dr.GetDateTime(4).Hour.ToString() + ":" + dr.GetDateTime(4).Minute.ToString());
                        }

                        sb.Append("</td><td class='artboxs' width='47%'  style=\"background-color:#f7f7f7;font-size:16px;padding-right:5px;color:#024771;\">");
                        sb.Append("<a href='" + articlepage + "?id=" + dr["id"].ToString() + "'>");
                        sb.Append(dr[1].ToString());
                        sb.Append("</a>");
                        sb.Append("</td>");
                    }
                    else
                    {
                        sb.Append("<td></td><td></td>");
                    }

                    sb.Append("</tr>");
                }
                else
                {
                    sb.Append("<tr><td style='height:20px;'></td></tr>");
                    counter25 = 0;
                }

                if (counterarch==49)
                {
                    break;
                }
            }

            sb.Append("</table>");


            

            //  <div style="text-align:left;width:100%;margin-top:5px;">

            //    <img src="cryst/imgs/archive.jpg" />
            //</div>

            if (dr.Read())
            {
                sb.Append("<div style=\"text-align:left;width:100%;margin-top:5px;\">");
                sb.Append(string.Format("<a href=\"arch1.aspx?id={0}\">", catid));
                sb.Append("<img src=\"cryst/imgs/archive.jpg\" border=0/>");
                sb.Append("</a>");
                sb.Append("</div>"); 
            }


            if (counter > 0)
            {
                return sb.ToString();
            }
            else { return ""; }
        }

        public static string getManageTabs()
        {
        //    <table style="direction: ltr; width: 100%;" cellpadding="0" cellspacing="0">
        //    <tr>
        //        <td>
        //            <ul id="managtabs">
        //                <li class="atab2" onclick="javascript:managtab_click(this,1);"><span>وظائف اداريه</span></li>
        //                <li class="tab2" onclick="javascript:managtab_click(this,2);"><span>اقسام الاداره</span></li>
        //                <li class="tab2" onclick="javascript:managtab_click(this,3);"><span>القياده</span></li>
        //                <li class="tab2" onclick="javascript:managtab_click(this,4);"><span>إدارة استراتيجية</span></li>
        //                <li class="tab2" onclick="javascript:managtab_click(this,5);"><span>سلوك إداري</span></li>
        //                <li class="tab2" onclick="javascript:managtab_click(this,6);"><span>اقتصاد إداري
        //                </span></li>
        //                <li class="tab2" onclick="javascript:managtab_click(this,7);"><span>مهارات إدارية
        //                </span></li>
        //                <li class="tab2" onclick="javascript:managtab_click(this,8);"><span>فكر إداري
        //                </span></li>
        //                <li class="tab2" onclick="javascript:managtab_click(this,9);"><span>إدارة دعوية
        //                </span></li>
        //                <li class="tab2" onclick="javascript:managtab_click(this,10);"><span>إدارة الذات
        //                </span></li>
        //            </ul>
        //        </td>
        //    </tr>
        //    <tr>
        //        <td style="padding: 0px 2px 2px 2px;">
        //            <div style="width: 100%; height: 29px; background-image: url('cryst/imgs/mang_bar.jpg');
        //                background-repeat: repeat-x; border-top: solid 1px #b9b9b9; border-left: solid 1px #b9b9b9;
        //                border-right: solid 1px #b9b9b9; text-align: center; color: White; font-size: 16px;
        //                font-weight: bold;" class="subtab">
        //                <div style="padding-top: 5px; text-align: center; width: 100%; direction: rtl;" id="sublinks">
        //                </div>
        //            </div>
        //            <div style="width: 100%; height: 300px; border: solid 1px #b9b9b9; background-color: #f7f7f7;
        //                direction: rtl; font-size: 16px; font-weight: bold;">
        //                <div style="padding: 10px;" id="mainarts">
        //                </div>
        //            </div>
        //        </td>
        //    </tr>
            //</table>
            
            StringBuilder sb = new StringBuilder();
            sb.Append("<table style=\"direction: ltr; width: 100%;\" cellpadding=\"0\" cellspacing=\"0\">");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("<ul id=\"managtabs\">");
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["newislammemoConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("select top 6 id, name from categories",conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                sb.Append("<li class=\"atab2\" onclick=\"javascript:managtab_click(this," + dr[0].ToString() + ");\"><span>" + dr[1].ToString().Trim() + "</span></li>");
            }
            while (dr.Read())
            {
                sb.Append("<li class=\"tab2\" onclick=\"javascript:managtab_click(this," + dr[0].ToString() + ");\"><span>" + dr[1].ToString().Trim() + "</span></li>");

            }
            dr.Close();
            conn.Close();
            sb.Append("</ul>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style=\"padding: 0px 2px 2px 2px;\">");
            sb.Append("<div style=\"width: 100%; height: 29px; background-image: url('cryst/imgs/mang_bar.jpg');background-repeat: repeat-x; border-top: solid 1px #b9b9b9; border-left: solid 1px #b9b9b9;border-right: solid 1px #b9b9b9; text-align: center; color: White; font-size: 16px;font-weight: bold;\" class=\"subtab\">");
            sb.Append("<div style=\"padding-top: 5px; text-align: center; width: 100%; direction: rtl;\" id=\"sublinks\"></div>");
            sb.Append("</div>");
            sb.Append("<div style=\"width: 100%; height: 300px; border: solid 1px #b9b9b9; background-color: #f7f7f7;direction: rtl; font-size: 16px; font-weight: bold;\">");
            sb.Append("<div style=\"padding: 10px;\" id=\"mainarts\"></div>");
            sb.Append("</div>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            

            return sb.ToString();
        }

        public static string getManageTabs2()
        {


             //<table style="direction:rtl;width:100%;" cellpadding="0" cellspacing="0">
             //               <tr>
             //                   <td style="padding-top:5px;">
             //                       <table cellpadding=0 cellspacing=0 style="height:22px;">
             //                           <tr>
             //                          <td style="background-image:url('cryst/imgs/mange_tab_rc.jpg');background-repeat:no-repeat;width:7px;"> </td>
             //                          <td style="background-image:url('cryst/imgs/mange_tab_cc.jpg');background-repeat:repeat-x;color:White;"><div style="padding-top:5px;font-size:115%;">  ����� ������ </div>   </td>
             //                          <td style="background-image:url('cryst/imgs/mange_tab_lc.jpg');background-repeat:no-repeat;width:7px;"></td>
             //                           </tr>
             //                       </table>
             //                   </td>
             //               </tr>
             //               <tr >
             //                   <td align="center" style="background-image: url('cryst/imgs/mang_bar.jpg');height: 29px;width: 100%;white-space:nowrap;">
             //                   <div style="padding-top:6px;">
             //                       <span onclick="javascript:hi(this,0);" style="margin:5px;padding:5px;cursor:pointer;" class="subtabaselected">�������</span>
             //                       <span onclick="javascript:hi(this,1);" style="margin:5px;padding:5px;cursor:pointer;" class="subtaba">�������</span>
             //                       <span onclick="javascript:hi(this,2);" style="margin:5px;padding:5px;cursor:pointer;" class="subtaba">�������</span>
             //                       <span onclick="javascript:hi(this,3);" style="margin:5px;padding:5px;cursor:pointer;" class="subtaba">�������</span>
             //                   </div>
             //                   </td>
             //               </tr>
             //               <tr>
             //                   <td id="mytd" style="background-color: #f7f7f7;border: solid 1px #b9b9b9;">
                                
             //                   <div style="padding:10px;">
                                   
                                    
             //                       <table class="aaaa">
             //                           <tr>
             //                               <td >
             //                                ��� ���� ������� ����߿ [����� �� �������]  692  

             //                               </td>
             //                               <td>
             //                                ��� ���� ����� ����� � [9] ������ �������  1991  
  
             //                               </td>
             //                           </tr>
             //                           <tr>
             //                               <td>
             //                             �� ��� ����� ���� ������  2378  
 
             //                               </td>
             //                               <td>
             //                               ��� �������� ������� [3] ������ ���������[2]  1055  
             //                               </td>
             //                           </tr>
             //                           <tr>
             //                               <td>
             //                               �� ��� ����� ���� ������  2378 
             //                               </td>
             //                               <td>
             //                               ��� �������� ������� [3] ������ ���������[2]  1055  
             //                               </td>
             //                           </tr>
             //                           <tr>
             //                               <td>
             //                                ��� ���� ������� ����߿ [����� �� �������]  692  
             //                               </td>
             //                               <td>
             //                               ��� ���� ������� ����߿ [����� �� �������]  692  
             //                               </td>
             //                           </tr>
             //                       </table>
                                    
             //                      </div>
                                   
             //                      <div style="padding:10px;display:none;">
                                   
             //                       <table class="aaaa">
             //                           <tr>
             //                               <td >
             //                               ����� �������� ������� ������� ����� ������� ���'����' 
             //                               </td>
             //                               <td>
             //                               ����� �������� ����� �� ����� ����� ����� ������� �������� 

             //                               </td>
             //                           </tr>
             //                           <tr>
             //                               <td>
             //                              ����� ����� ������ ���� ��� '���' ������� ��� ����� 

             //                               </td>
             //                               <td>
             //                               ����� �������� ����� �� ����� ����� ����� ������� �������� 

             //                               </td>
             //                           </tr>
             //                           <tr>
             //                               <td>
             //                              ����� ����� ������ ���� ��� '���' ������� ��� ����� 

             //                               </td>
             //                               <td>
             //                              ������ ������ ������ ��� '��� ������ ������' �������� 
             //                               </td>
             //                           </tr>
             //                           <tr>
             //                               <td>
             //                               ����� ����� ������ ���� ��� '���' ������� ��� ����� 

             //                               </td>
             //                               <td>
             //                               ������ ������ ������ ��� '��� ������ ������' �������� 
             //                               </td>
             //                           </tr>
             //                       </table>
             //                      </div>
             //                   </td>
             //               </tr>
             //              </table>
             //              <div style="padding:3px;width:100%;"></div>


            
            

            //�� ������� id
            int manageid = 605;

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["newislammemoConnectionString"].ConnectionString);
            DataTable dt = new DataTable();
            // TODO:change query and door=1
            SqlDataAdapter da = new SqlDataAdapter("select id,name from categories where parentid=@catid",conn);
            da.SelectCommand.Parameters.AddWithValue("@catid",manageid);
            da.Fill(dt);



            StringBuilder sb = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();


            foreach (DataRow dr in dt.Rows)
            {

               // <table style="direction:rtl;width:100%;" cellpadding="0" cellspacing="0">
             //               <tr>
             //                   <td style="padding-top:5px;">
             //                       <table cellpadding=0 cellspacing=0 style="height:22px;">
             //                           <tr>
             //                          <td style="background-image:url('cryst/imgs/mange_tab_rc.jpg');background-repeat:no-repeat;width:7px;"> </td>
             //                          <td style="background-image:url('cryst/imgs/mange_tab_cc.jpg');background-repeat:repeat-x;color:White;"><div style="padding-top:5px;font-size:115%;">  ����� ������ </div>   </td>
             //                          <td style="background-image:url('cryst/imgs/mange_tab_lc.jpg');background-repeat:no-repeat;width:7px;"></td>
             //                           </tr>
             //                       </table>
             //                   </td>
             //               </tr>
                sb.Append("<table style=\"direction:rtl;width:100%;\" cellpadding=\"0\" cellspacing=\"0\">");
                sb.Append("<tr>");
                sb.Append("<td style=\"padding-top:5px;\">");
                sb.Append("<table cellpadding=0 cellspacing=0 style=\"height:22px;\">");
                sb.Append("<tr>");
                sb.Append("<td style=\"background-image:url('cryst/imgs/mange_tab_rc.jpg');background-repeat:no-repeat;width:7px;\"> </td>");
                sb.Append(string.Format("<td style=\"background-image:url('cryst/imgs/mange_tab_cc.jpg');background-repeat:repeat-x;color:White;\"><div style=\"padding-top:5px;font-size:115%;white-space:nowrap;\">{0}</div>   </td>", dr["name"].ToString().Trim()));
                sb.Append("<td style=\"background-image:url('cryst/imgs/mange_tab_lc.jpg');background-repeat:no-repeat;width:7px;\"></td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("</td>");
                sb.Append("</tr>");

                

                if (conn.State != ConnectionState.Open) { conn.Open(); }
                // TODO:change query and door=1
                SqlCommand cmd = new SqlCommand("select id,name from categories where parentid=@catid",conn);
                cmd.Parameters.AddWithValue("@catid",dr["id"].ToString());
                SqlDataReader sdr = cmd.ExecuteReader();

               // <tr >
             //                   <td align="center" style="background-image: url('cryst/imgs/mang_bar.jpg');height: 29px;width: 100%;white-space:nowrap;">
             //                  <div style="padding-top:6px;">
                sb.Append("<tr>");
                sb.Append("<td align=\"center\" style=\"background-image: url('cryst/imgs/mang_bar.jpg');height: 29px;width: 100%;white-space:nowrap;font-size:115%;\">");
                sb.Append("<div style=\"padding-top:6px;\">");
                if (sdr.Read())
                {

                    sb.Append("<span onclick=\"javascript:hi(this,0);\" style=\"margin:5px;padding:5px;cursor:pointer;\" class=\"subtabaselected\">" + sdr["name"].ToString().Trim() + "</span>");
                    //fill the sb2
                    sb2.Append("<div style=\"padding:10px;\"><table class=\"manage_arts_table\">");
                    sb2.Append(getManageTabs2(sdr[0].ToString()));
                    sb2.Append("</table></div>");




                    int counter = 0;
                    while (sdr.Read()) 
                    {
                        counter++;
                        sb.Append("<span onclick=\"javascript:hi(this," + counter + ");\" style=\"margin:5px;padding:5px;cursor:pointer;\" class=\"subtaba\">" + sdr["name"].ToString().Trim() + "</span>");

                        sb2.Append("<div style=\"padding:10px;display:none;\"><table class=\"manage_arts_table\">");
                        sb2.Append(getManageTabs2(sdr[0].ToString()));
                        sb2.Append("</table></div>");
                    }

                    //get the data from the child myid

                }
                else
                {
                    sb2.Append("<div style=\"padding:10px;\">");
                    sb2.Append("<table class=\"manage_arts_table\">");

                    sb2.Append(getManageTabs2(dr["id"].ToString()));
                    sb2.Append("</table>");
                    sb2.Append("</div>");


                    //get the data from the parent dr["id"]
                }
                sdr.Close();

               //  </div>
             //                   </td>
             //               </tr>
                sb.Append("</div>");
                sb.Append("</td>");
                sb.Append("</tr>");

//start of div article

 //<tr>
             //                   <td id="mytd" style="background-color: #f7f7f7;border: solid 1px #b9b9b9;">
                                

                sb.Append("<tr>");
                sb.Append("<td style=\"background-color: #f7f7f7;border: solid 1px #b9b9b9;\">");

                sb.Append(sb2.ToString());
                sb2 = new StringBuilder();

                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("</table>");//spacer
                sb.Append("<div style=\"padding:3px;width:100%;\"></div>");//spacer
                    
            }




            return sb.ToString();

             
            return "";      
        }


        static string getManageTabs2(string catid) 
        {

            //<tr>
             //                               <td >
             //                               ����� �������� ������� ������� ����� ������� ���'����' 
             //                               </td>
             //                               <td>
             //                               ����� �������� ����� �� ����� ����� ����� ������� �������� 

             //                               </td>
             //                           </tr>
            int counter = 0;
            StringBuilder sb = new StringBuilder();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["newislammemoConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("SELECT top 10 Articles.ID, Articles.Title FROM article_cats INNER JOIN Articles ON article_cats.ArtID = Articles.ID WHERE (Articles.stat = 8) AND (article_cats.CatID = @catid) order by date desc", conn);
            cmd.Parameters.AddWithValue("@catid",catid);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while(dr.Read())
            {
                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append("<a href=\"article1.aspx?id=" + dr[0].ToString() + "\">");
                sb.Append(dr[1].ToString().Trim());
                sb.Append("</a>");
                sb.Append("</td>");
                counter++;
                if (dr.Read())
                {
                    sb.Append("<td>");
                    sb.Append("<a href=\"article1.aspx?id=" + dr[0].ToString() + "\">");
                    sb.Append(dr[1].ToString().Trim());
                    sb.Append("</a>");
                    sb.Append("</td>");
                }
                else { sb.Append("<td style=\"background-image:url('');\"></td>"); }

                sb.Append("</tr>");
                if (counter>3)
                {
                    break;
                }
            }
            if (dr.Read())
            {
                sb.Append("<td colspan='2' align='left' style=\"background-image:url('');\"><a href='cat1.aspx?id="+catid+"' style='padding:5px;'>������</a></td>");
            }

            conn.Close();


            return sb.ToString();
        }

        public static string admin_news_default_tr(IDataReader dr,List<string> myroles,string req_cat,string req_cas,string myuname) 
        {
            StringBuilder sb = new StringBuilder();
            DateTime dt = DateTime.Now.AddYears(-1);
            try
            {
                dt = (DateTime)dr["fixed_date"];
            }
            catch (Exception ex) { }


            //<td><%# Eval("id")%></td>
            //<td><%# Eval("username")%></td>
            //<td><%# geturgenticon(Eval("urgent").ToString())%>
            //<a href="<%# getpopupstring(Eval("id").ToString())%>" title='<%# Eval("Apprev")%>'>
            //<%# iffixed(Eval("fixed_date").ToString()) + 
            //Eval("Title").ToString()%></a></td><td nowrap>
            //<%# mytext.getdatestring(Eval("date").ToString())%></td>
            //<td><%# getcasestring(Eval("stat").ToString())%></td><td></td></tr><tr><td valign=top>�������</td><td colspan="4"><%# Eval("notes").ToString() %></td></tr>
            //<tr><td colspan=5><%# getactionpane(Eval("id").ToString(), Eval("stat").ToString(), Eval("username").ToString())%><hr /></td>
            sb.Append(string.Format("<td>{0}</td><td>{1}</td>", dr["id"], dr["username"]));
            sb.Append(string.Format("<td id='td1{0}'>",dr["id"]));
            if (dr["urgent"].ToString().ToLower() == "true")
            {
                sb.Append("<img src='icons/urg.jpg' border=0 >");
            }
            if (dt > DateTime.Now)
            {
                sb.Append(string.Format("<a href=\"javascript:myf('{0}');\" title=\"{1}\"> {2}</a>", dr["id"], dr["Apprev"].ToString().Replace('"', ' '), "<img src='icons/fixed.gif' alt='����' border='0'/>" + dr["title"].ToString().Replace('"', ' ')));
            }
            else
            {
                sb.Append(string.Format("<a href=\"javascript:myf('{0}');\" title=\"{1}\">{2}</a>", dr["id"], dr["Apprev"].ToString().Replace('"', ' '), dr["title"].ToString().Replace('"', ' ')));
            }
            sb.Append("</td>");
            sb.Append(string.Format("<td nowrap id='td2{1}'>{0}</td>", dr["date"], dr["id"]));
            sb.Append(string.Format("<td id='td3{1}'>{0}</td><td></td></tr>", getcasestring(dr["stat"].ToString()),dr["id"]));
            sb.Append(string.Format("<tr><td valign=top>�������</td><td colspan='4' id='td4{1}'>{0}</td></tr>", dr["notes"], dr["id"]));
            sb.Append(string.Format("<tr><td colspan=5 id='td5{1}'>{0}<hr /></td>", mytext.getactionpane(dr["id"].ToString(), dr["stat"].ToString(), dr["username"].ToString(), myroles, req_cat, req_cas, myuname),dr["id"]));
            return sb.ToString();
        }


        public static string getcasestring(string strcase)
        {
            int temp;
            int.TryParse(strcase, out temp);
            switch (temp)
            {
                case 1:
                    strcase = "��� ����";
                    break;
                case 2:
                    strcase = "��� ����� ��� �������";
                    break;
                case 3:
                    strcase = "��� ����� �������";
                    break;
                case 4:
                    strcase = "����� ������� ������ �������";
                    break;
                case 5:
                    strcase = "������� ������ ����";
                    break;
                case 6:
                    strcase = "������� ������ ����";
                    break;
                case 7:
                    strcase = "����� ������";
                    break;
                case 8:
                    strcase = "��� �� ";
                    break;
                default:
                    strcase = "0";
                    break;
            }
            return strcase;
        }

        public static string get_news_letter(int letter_id) 
        {

            
            StringBuilder sb = new StringBuilder();
            sb.Append("<style>.newsletter a:link ,.newsletter  a:visited{color:#004e89;font-weight:bold;}.newsletter a:hover{text-decoration:underline;}</style>");
            sb.Append("<table width='100%' dir='rtl' style='font-size:16px;'><tr><td align='right'><img src='http://islammemo.cc/imgs/maillist/01.jpg' alt='������ �������� ������ �������' /></td></tr><tr><td style='background-color:#cce9ff;'><img src='http://islammemo.cc/imgs/maillist/02.jpg' alt='��� �������' /></td></tr><tr><td>");
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["newislammemoConnectionString"].ConnectionString);
            SqlCommand cmd;
            SqlDataReader dr;

            conn.Open();

            sb.Append("<table dir='rtl' width='100%' class='newsletter'>");

            cmd = new SqlCommand("SELECT letters_arts.art_id, Articles.ID, Articles.Title,Articles.Apprev, Articles.image_url FROM letters_arts INNER JOIN Articles ON letters_arts.art_id = Articles.ID WHERE (letters_arts.letter_id = @letter_id) AND (letters_arts.layout = @layout)", conn);
            cmd.Parameters.AddWithValue("@letter_id",letter_id);
            cmd.Parameters.AddWithValue("@layout",1);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append(string.Format("<table><tr><td><a href='http://www.islammemo.cc/article1.aspx?id={0}'>{1}</a></td><td rowspan='2'><img height='80' width='80' src='{2}' /></td></tr><tr><td>{3}</td></tr></table>", dr["id"], dr["title"].ToString().Trim(), ConfigurationManager.AppSettings["image_server"] + dr["image_url"].ToString().Trim(), dr["Apprev"].ToString().Trim()));
                sb.Append("</td>");
                if (dr.Read())
                {
                    sb.Append("<td>");
                    sb.Append(string.Format("<table><tr><td><a href='http://www.islammemo.cc/article1.aspx?id={0}'>{1}</a></td><td rowspan='2'><img height='80' width='80' src='{2}' /></td></tr><tr><td>{3}</td></tr></table>", dr["id"], dr["title"].ToString().Trim(), ConfigurationManager.AppSettings["image_server"] + dr["image_url"].ToString().Trim(), dr["Apprev"].ToString().Trim()));
                    sb.Append("</td>");
                }
                else
                {
                    sb.Append("<td>");
                    sb.Append("</td>");
                }
                sb.Append("</tr>");
            } dr.Close();

            sb.Append("</table>");
            //jawal e3lan
            sb.Append("<hr /><div align='center'><a href='http://www.mutawef.com/' target='_blank'> <img style='margin:20px;' src='http://islammemo.cc/images/flash/mutawef.gif'  width='240' height='60' border='0'/>  </a><a href='http://www.almosaly.com/' target='_blank'><img src='http://islammemo.cc/images/flash/musally.gif'  style='margin:20px;' border='0'/> </a></div><hr />");
            sb.Append("<table dir='rtl' width='100%' class='newsletter'>");

            cmd = new SqlCommand("SELECT letters_arts.art_id, Articles.ID, Articles.Title  FROM letters_arts INNER JOIN Articles ON letters_arts.art_id = Articles.ID WHERE (letters_arts.letter_id = @letter_id) AND (letters_arts.layout = @layout)", conn);
            cmd.Parameters.AddWithValue("@letter_id", letter_id);
            cmd.Parameters.AddWithValue("@layout", 2);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {

                sb.Append("<tr>");

                sb.Append("<td>");
                sb.Append(string.Format("<a href='http://www.islammemo.cc/article1.aspx?id={0}'>{1}</a>", dr["id"], dr["title"].ToString().Trim()));
                sb.Append("</td>");
                if (dr.Read())
                {
                    sb.Append("<td>");
                    sb.Append(string.Format("<a href='http://www.islammemo.cc/article1.aspx?id={0}'>{1}</a>", dr["id"], dr["title"].ToString().Trim()));
                    sb.Append("</td>");
                }
                else
                {
                    sb.Append("<td>");
                    sb.Append("</td>");
                }
                sb.Append("</tr>");

            } dr.Close();

            sb.Append("</table>");
            sb.Append("</td></tr><tr><td style='background-color:#cce9ff;'><img src='http://islammemo.cc/imgs/maillist/03.jpg' alt='��������'/></td></tr><tr><td>");

            sb.Append("<div class='newsletter' style='margin-top:10px;margin-bottom:10px;'>");
            cmd = new SqlCommand("SELECT letters_arts.art_id, Articles.ID, Articles.Title FROM letters_arts INNER JOIN Articles ON letters_arts.art_id = Articles.ID WHERE (letters_arts.letter_id = @letter_id) AND (letters_arts.layout = @layout)", conn);
            cmd.Parameters.AddWithValue("@letter_id", letter_id);
            cmd.Parameters.AddWithValue("@layout", 3);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                sb.Append(string.Format("<div style='width:100%;text-align:right;'><a href='http://www.islammemo.cc/article1.aspx?id={0}'>{1}</a></div>", dr["id"], dr["title"].ToString().Trim()));

            } dr.Close();
            sb.Append("</div>");

            sb.Append("</td></tr><tr><td style='background-color:#cce9ff;'><img src='http://islammemo.cc/imgs/maillist/04.jpg' alt='������� �������' /></td></tr><tr><td>");

            sb.Append("<div class='newsletter' style='margin-top:10px;margin-bottom:10px;'>");
            cmd = new SqlCommand("SELECT letters_arts.art_id, Articles.ID, Articles.Title FROM letters_arts INNER JOIN Articles ON letters_arts.art_id = Articles.ID WHERE (letters_arts.letter_id = @letter_id) AND (letters_arts.layout = @layout)", conn);
            cmd.Parameters.AddWithValue("@letter_id", letter_id);
            cmd.Parameters.AddWithValue("@layout", 4);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                sb.Append(string.Format("<div style='width:100%;text-align:right;'><a href='http://www.islammemo.cc/article1.aspx?id={0}'>{1}</a></div>",dr["id"],dr["title"].ToString().Trim()));

            } dr.Close();
            sb.Append("</div>");

            sb.Append("</td></tr><tr><td>");

            sb.Append("</td></tr></table>");



            conn.Close();

           

            return sb.ToString();
        }

        public static string get_article(int artid,System.Web.Caching.Cache cache) 
        {
            string part1 = cache.Get(string.Format("art_{0}_1",artid)).ToString();
            string part2 = cache.Get(string.Format("art_{0}_2",artid)).ToString();
            if (string.IsNullOrEmpty(part1) || string.IsNullOrEmpty(part2))
            {

            }
            return "";
        }
        static string get_article_live(int artid, System.Web.Caching.Cache cache) 
        {
            StringBuilder sb1 = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            string myreads = "";
            SqlConnection conn = new SqlConnection();
            SqlCommand cmd = new SqlCommand("news_select_article_by_id", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", artid);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                int mystat = 0;
                int.TryParse(dr["stat"].ToString(), out mystat);
                if (mystat != 8)
                {
                    sb1.Append("<div id='article_title' style=\"background-color:#e2f4ff;margin-top:5px;color:#00517a;text-align:right;font-weight:bold;font-size:130%;padding:2px 2px 2px 2px;\">");
                    sb2.Append("��� ������ ��� ������ </div>");
                }
                else 
                {
                
                }
            
            }
            conn.Close();

            return sb1.ToString() + myreads + sb2.ToString();
        
        }

        public static string get_image_slide(List<string> myimages, List<string> mydisc, List<string> mynames) 
        {
            if (myimages == null||myimages.Count<1)
            {
                return "";
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='100%' dir='rtl'><tr><td align='left' rowspan='2' width='50%'>");
            if (myimages.Count>1)
            {
                sb.Append("<img src='/cryst/imgs/arr2.jpg' style='cursor:pointer;' onclick='javascript:changeThumb(-1,false);' />");
            }
            //todo:change image id
            sb.Append(string.Format("</td><td align='center'><img id='multiimg_img' src='{0}' title='{1}' alt='{2}' /></td><td align='right' rowspan='2'  width='50%'>", ConfigurationManager.AppSettings["image_server"] + myimages[0], mynames[0], mynames[0]));
            if (myimages.Count > 1)
            {
                sb.Append("<img src='/cryst/imgs/arr.jpg' style='cursor:pointer;' onclick='javascript:changeThumb(1,false)' />"); 
            }
            //todo:change lable id
            sb.Append("</td></tr><tr><td align='center'><span  id='multiimg_lbl' style='font-weight: bold;color: gray;text-align: center;white-space:nowrap;direction:rtl;'>"+mydisc[0]+"</span></td></tr></table>");

            if (myimages.Count<2)
            {
                return sb.ToString();
            }
            //javascript
            sb.Append("<script language='javascript' type='text/javascript'>");
            sb.Append("var thumbs = new Array(");
            for (int i = 0; i < myimages.Count; i++)
            {
                sb.Append("'/" + myimages[i].Trim() + "'");
                if (i + 1 != myimages.Count) { sb.Append(" ,"); }
            }
            sb.Append(");");
            sb.Append("var discs = new Array(");
            for (int i = 0; i < myimages.Count; i++)
            {
                sb.Append("'" + mydisc[i].Trim() + "'");
                if (i + 1 != myimages.Count) { sb.Append(" ,"); }
            }
            sb.Append(");");
            sb.Append("var img_names = new Array(");
            for (int i = 0; i < myimages.Count; i++)
            {
                sb.Append("'" + mynames[i].Trim() + "'");
                if (i + 1 != myimages.Count) { sb.Append(" ,"); }
            }
            sb.Append(");");
            sb.Append("thisThumb = 1;");
            sb.Append("thumbCount = thumbs.length;");
            sb.Append("function changeThumb(direction,flage1) {");
            //sb.Append("if (true) {");
            sb.Append("thisThumb = thisThumb + direction;");
            sb.Append("if (thisThumb > thumbCount) {thisThumb = 1;}");
            sb.Append("if (thisThumb <= 0) {thisThumb = thumbCount;}");
            sb.Append("document.getElementById('" + "multiimg_img" + "').src = thumbs[thisThumb - 1];");//but the src of the img
            sb.Append("document.getElementById('" + "multiimg_img" + "').title = img_names[thisThumb - 1];");//but the src of the img
            sb.Append("document.getElementById('" + "multiimg_img" + "').alt = img_names[thisThumb - 1];");//but the src of the img
            sb.Append("document.getElementById('" + "multiimg_lbl" + "').innerHTML = discs[thisThumb - 1];");//but the src of the img
            //sb.Append("document.thumbindex.src = '../../../images/game-imagenav-number-' + thisThumb + '.gif';"); //but the number of the current img as picture
            sb.Append("window.clearTimeout(myslidtimer);if(flage1){myslidtimer=window.setTimeout('changeThumb(1,true)',7000);}}");
            //sb.Append("}");
            sb.Append(@"var myslidtimer= window.setTimeout('changeThumb(1,true)',7000);</script>");
            return sb.ToString();
        }

        public static string get_nav_menu(int catid) 
        {
            int mycatid = catid;
            StringBuilder sb1 = new StringBuilder();
            sb1.Append("<div class='nolinks' style=\"color:#00517a;padding:2px 2px 2px 2px;direction:rtl;\"><a href='/default.aspx'>������ �������</a>");
            string tempstr = "";
            string tempstr2 = "";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["newislammemoConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("select id,name,parentid,url,door from categories where id=@id ", conn);
            cmd.Parameters.AddWithValue("@id", mycatid);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                int.TryParse(dr["parentid"].ToString(), out mycatid);
                if (!dr.IsDBNull(4) && dr.GetBoolean(4))
                {
                    if (string.IsNullOrEmpty(dr["url"].ToString()))
                    {
                        tempstr = string.Format(" &gt; <a href='cat1.aspx?id={0}'>{1}</a>", dr["id"], dr["name"].ToString().Trim());
                    }
                    else
                    {
                        tempstr = string.Format(" &gt; <a href='{0}'>{1}</a>", dr["url"], dr["name"].ToString().Trim());
                    }
                }

            }
            dr.Close();
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@id", mycatid);
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                int.TryParse(dr["parentid"].ToString(), out mycatid);
                if (!dr.IsDBNull(4) && dr.GetBoolean(4))
                {
                    if (string.IsNullOrEmpty(dr["url"].ToString()))
                    {
                        tempstr2=string.Format(" &gt; <a href='cat1.aspx?id={0}'>{1}</a>", dr["id"], dr["name"].ToString().Trim());
                    }
                    else
                    {
                        tempstr2=string.Format(" &gt; <a href='{0}'>{1}</a>", dr["url"], dr["name"].ToString().Trim());
                    }
                }

            } dr.Close();
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@id", mycatid);
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                int.TryParse(dr["parentid"].ToString(), out mycatid);
                if (!dr.IsDBNull(4) && dr.GetBoolean(4))
                {
                    if (string.IsNullOrEmpty(dr["url"].ToString()))
                    {
                        sb1.Append(string.Format(" &gt; <a href='cat1.aspx?id={0}'>{1}</a>", dr["id"], dr["name"].ToString().Trim()));
                    }
                    else
                    {
                        sb1.Append(string.Format(" &gt; <a href='{0}'>{1}</a>", dr["url"], dr["name"].ToString().Trim()));
                    }
                }

            } dr.Close();
            conn.Close();
            sb1.Append(tempstr2);
            sb1.Append(tempstr);
            sb1.Append("</div>");

            return sb1.ToString();
        }

        public static string get_cat_nav_menu(int catid)
        {
            string navigation = "";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["newislammemoConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand("select id,name,parentid,url,door,url_full_path from categories where id=@id ", conn);
            conn.Open();
            get_cat_parent(cmd, catid, ref navigation);
            conn.Close();
            return navigation;
        }

        static void get_cat_parent(SqlCommand cmd, int catid, ref string navigation)
        {
            int temp_parentid;
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@id", catid);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                int.TryParse(dr["parentid"].ToString(), out temp_parentid);
                string tempstring = "";
                if (!dr.IsDBNull(4) && dr.GetBoolean(4))
                {
                    if (string.IsNullOrEmpty(dr["url"].ToString()))
                    {
                        tempstring = string.Format("-&gt; <a href='{0}'>{1}</a>", dr["url_full_path"], dr["name"]);
                        //cattree += dr["url_name"].ToString();
                    }
                    else
                    {
                        tempstring = string.Format("-&gt; <a href='/{0}'>{1}</a>", dr["url"], dr["name"]);
                    }
                }
                dr.Close();
                get_cat_parent(cmd, temp_parentid, ref navigation);
                navigation += tempstring;
            }
            //dr.Close();
        }

        public static string get_chain_arts(int chainid)
        {
            StringBuilder sb2=new StringBuilder();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["newislammemoConnectionString"].ConnectionString);


            sb2.Append("<div align=right class=\"tab_main01\">");
            SqlCommand cmd = new SqlCommand("SELECT ID, Title,url FROM Articles WHERE (chain = @ch_id) and (stat=8) order by id", conn);
            cmd.Parameters.AddWithValue("@ch_id", chainid);
            conn.Open();
            sb2.Append("<ul>");
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                sb2.Append(string.Format("<li><a href='{0}'>{1}</a><br/></li>", dr["url"].ToString(), dr["title"].ToString()));
            }
            conn.Close();
            sb2.Append("</ul>");
            sb2.Append("</div>");

            return sb2.ToString();
        }
    } 
}
